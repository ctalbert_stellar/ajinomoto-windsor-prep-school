<body class="<?=$pageName?>">
<!--[if lte IE 8]>
<div class="chromeframe">
	<p>You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
</div>
<![endif]-->
<noscript>
<div class="noscript">
	<p>Your browser has javascript disabled. This website will still display it's contents, but for the best experience, please enable javascript.</p>
</div>
</noscript>
<div id="site-wrapper">
<header id="site-header" class='<?=$pageName?>'>
    <h1 id="heading">
        <a href="http://ajinomotowindsorprepschool.com"><img src="images/logo.png" alt="Ajinomoto Windsor Prep School Logo"></a>
    </h1>
    <span id="vanity">
        Hello, <?=$user->person->firstName . " " . $user->person->lastName?>.
        <a href="login.php?<?=http_build_query(array('action' => 'logout'))?>">Logout</a>
        <?php if($user->isAdmin):?>
                <br><a href="wonadmin/">Admin Control Panel</a>
        <?php endif;?>
    </span>
    <!--<span id='hamburger-wrapper'>
        <span id="hamburger"><img src="images/hamburger.png" alt=''></span>
    </span>-->
</header>