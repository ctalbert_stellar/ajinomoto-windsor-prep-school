<?php
if(!isset($user)){
    header("Location: login.php");
    exit();
}elseif(!$user->isApproved){  
    $_SESSION['Message'] = "Sorry, this account hasn't been approved yet.";
    header("Location: login.php");
    exit();
}