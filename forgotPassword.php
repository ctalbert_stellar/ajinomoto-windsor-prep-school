<?php
require_once 'init.php';
require_once 'includes/phpmailer/PHPMailerAutoload.php';

$title = 'Forgot Password';
require_once 'head.php';
$message = "";

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    $users = $dbContext['Users']->find(array('email'=>$_POST['email']));
    
    if(count($users) > 0){
        $userGUID = array_values($users)[0];
        $hash = hash("sha256", time());
        $query = "INSERT INTO `passwordResets` VALUES(:email, :token, :timestamp)";
        
        $statement = $db->prepare($query);
        $statement->execute(array('email' => $userGUID->email, 'token' => $hash, 'timestamp' => (microtime() + (24 * 60 * 60 * 60))));
        $mail = new PHPMailer;

        $mail->isSMTP();
        $mail->Host = 'mail.ajinomotowindsorprepschool.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'info@ajinomotowindsorprepschool.com';
        $mail->Password = 'letM3know';
        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;

        $mail->From = 'info@ajinomotowindsorprepschool.com';
        $mail->FromName = 'Ajinomoto Windsor Prep School';
        $admins = $dbContext['Users']->find(array('isAdmin' => true));

        $mail->addAddress($userGUID->email);

        $mail->Subject = "Ajinomoto Windsor Prep School Password Reset Link";
        $mail->Body = "Here is the password reset link you requested from Ajinomoto Windsor.\n\n" . 'http://ajinomotowindsorprepschool.com/resetPassword.php?token='
                . urlencode($hash) . "\n\nIf you requested this reset in error, you can simply ignore this email and the reset token will expire in 24 hours.  If you did not request this email"
                . ' please contact your site administrator.';

        $mail->send();
        $message = "Password reset request successful.  You should receive an email with instructions on how to continue shortly.";
    }else{
        $message = "User with the given email does not exist. Please verify that you have entered the correct email address for your account.";
    }
}
?>
<section id="main">
    <div id='login'>
        <div id='login-image-wrapper'>
            <img src='images/logo.png' alt='Ajinomoto Windsor Prep School Logo'/>
        </div>
        <?php if(isset($message)):?>
        <div class="message"><?=$message?></div>
        <?php endif;?>
        <p>Forgot your password? Enter your email below to receive a link to reset it.</p>
        <form method='post' class='group'>
            <input type='text' name='email' class='textbox-style-1' placeholder='Email'>
            <input type='submit' value='Submit' class='submit blue'>
        </form>
        <div id='registration-link' class='link-style-1'>
            <p><a href="login.php">Return to Login</a></p>
        </div>
    </div>
</section>
<?php include_once 'footer.php'?>

