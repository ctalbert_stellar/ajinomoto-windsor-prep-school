<!DOCTYPE html>
<!--[if lt IE 8]>       <html class="no-js oldie"> <![endif]-->
<!--[if IE 8]>          <html class="no-js ie8"> <![endif]-->
<!--[if IE 9]>          <html class="no-js ie9"> <![endif]-->
<!--[if gt IE 9]><!-->  <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!-- TODO: Set page title -->
    <title><?=isset($title)?$title:"Set your page title";?></title>

    <!-- TODO: Set page description. -->
    <meta name="description" content="">

    <!-- TODO: Change Favicon. -->
    <link rel="icon" type="image/png" href="<?=Config::$siteRoot?>/favicon.png">

    <!-- Webfonts -->
    <link href='http://fonts.googleapis.com/css?family=Neuton:200,400,700' rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link rel="stylesheet" href="<?=Config::$siteRoot?>/styles/normalize.css">
    <link rel="stylesheet" href="<?=Config::$siteRoot?>/styles/main.css">
    <?php if(basename($_SERVER["PHP_SELF"]) === 'pitchBuilder.php'): ?>
    <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
    <!--[if lte IE 9]><link rel="stylesheet" href="<?=Config::$siteRoot?>/styles/pitch-builder_ie9.css"><![endif]-->
    <!--[if !IE]><!--><link rel="stylesheet" href="<?=Config::$siteRoot?>/styles/pitch-builder.css"><!--<![endif]-->
    <?php endif; ?>

    <!-- Besides html5shiv and async analytics, all other scripts go before </body> -->
    <script src="<?=Config::$siteRoot?>/scripts/lib/html5shiv.js"></script>

    <?php if(!empty(Config::$googleAnalyticsId) && !IS_DEVELOPER): ?>
    <!-- Google Analytics -->
    <script>
    var _gaq=[['_setAccount','<?=Config::$googleAnalyticsId?>'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
    </script>
    <?php endif; ?>
</head>