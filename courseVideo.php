<?php
$courseGUID = filter_input(INPUT_GET, 'course', FILTER_SANITIZE_STRING);

if(!isset($courseGUID) || empty($courseGUID) || $courseGUID === FALSE){
    header("Location: courseOverview.php");
    exit();
}

//Start the system
include_once('init.php');

include 'loginCheck.php';

$title = "Course Overview";
$pageName = 'courseOverview';
$pageTitle = 'Sales Coach';
$pageIcon = 'images/sales-coach.png';
$course = array_values($dbContext['Courses']->find($courseGUID))[0];
$_SESSION['course'] = $course;
$_SESSION['questions'] = $course->getQuestions();
$_SESSION['questionProgress'] = 1;

include_once('head.php');
?>
<section id="main">
    <div id="site-wrapper">
        <?php
        include_once('header.php');
        ?>
        <div id="nav-dropdown">
            <?php include_once('nav.php');?>
        </div>
        
        <div class="youtube-wrapper">
            <?php preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $course->videos->location, $matches); ?>
            <div id="player"></div>
        </div>
            <a id="skip-video" class="blue small button right" href="course.php?<?=http_build_query(array('course' => $courseGUID))?>">Skip Video</a>
    </div>
</section>
<script src="http://www.youtube.com/player_api"></script>

<script type="text/javascript">

    // create youtube player
    var player;
    window.onYouTubeIframeAPIReady = function() {
        player = new YT.Player('player', {
          height: '585',
          width: '960',
          videoId: '<?=$matches[0]?>',
          showinfo: 0,
          events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
        });
    }

    // autoplay video
    function onPlayerReady(event) {
        event.target.playVideo();
    }

    // when video ends
    function onPlayerStateChange(event) {        
        if(event.data === 0) {          
            window.location.href = 'http://ajinomotowindsorprepschool.com/course.php?<?=http_build_query(array('course' => $courseGUID))?>';
        }
    }
</script>
<script type="text/javascript">
    function IE(v) {
        return RegExp('msie' + (!isNaN(v)?('\\s'+v):''), 'i').test(navigator.userAgent);
    }
    if(IE()) {
        window.onload = function () {
            if (!window.location.hash) {
                window.location = window.location + '#loaded';
                window.location.reload();
            }
        }
    }
</script>
<?php include 'footer.php';?>
