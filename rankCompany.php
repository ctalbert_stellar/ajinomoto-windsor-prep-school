<?php
class CompanyRanking{
    public $name;
    public $average;
    
    public function __construct($name, $average) {
        $this->name = $name;
        $this->average = $average;
    }
}

function compareRanking($company1, $company2){
    if($company1->average === $company2->average){
        return 0;
    }
    
    return ($company1->average > $company2->average)?-1:1;
}
include 'includes/Config.php';
include 'includes/dbContext.php';
$db = new PDO('mysql:dbname='.Config::$dbSchema.';host='.Config::$dbHost, Config::$dbUser, Config::$dbPass);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$db->query("SET time_zone = '+5:00'");
$dbContext = new dbContext($db);

$rankedCompanies = array();

$companies = $dbContext['Companies']->getAll();
$courses = $dbContext['Courses']->getAllApproved();
$courseGUIDs= array();

foreach($courses as $course){
    $courseGUIDs[] = $course->guid;
}



foreach($companies as $company){
    $userAverages = array();
    foreach($dbContext['Persons']->find(array('company' => $company->guid)) as $user){
        $courseHistories = array();
        foreach($user->courseHistory as $ch){
            if(in_array($ch->courseGUID, $courseGUIDs)){
                if(!isset($courseHistories[$ch->courseGUID])){
                    $courseHistories[$ch->courseGUID] = $ch;
                }else{
                    if($ch->timestamp > $courseHistories[$ch->courseGUID]->timestamp){
                        $courseHistories[$ch->courseGUID] = $ch;
                    }
                }
            }
        }
        //At this point $courseHistories is all of a users most current attempts on active courses
        
        $sum = 0;
        $count = 0;
        foreach($courseHistories as $ch){
            //We only want to count active courses
            $course = array_values($dbContext['Courses']->find($ch->courseGUID))[0];
            if($course->active){
                $sum += $ch->grade;
                $count++;
            }
        }
        
        //And we want to assign a score of 0 for each uncompleted course
        $courses = $dbContext['Courses']->getAll();
        
        foreach($courses as $course){
            if($course->active && !isset($courseHistories[$course->guid])){
                $count++;
            }
        }
        
        if($count > 0 && $sum > 0){
            $userAverages[] = ($sum/$count);
        }else{
            $userAverages[] = 0;
        }
    }
    $rankedCompanies[] = new CompanyRanking($company->name, (array_sum($userAverages)/count($userAverages)));
}

usort($rankedCompanies, 'compareRanking');

file_put_contents('/opt/www/windsorwonsource.com/public/cache/companyRankings.json', json_encode($rankedCompanies));