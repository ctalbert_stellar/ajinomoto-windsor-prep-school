<?php
//Start the system
include_once('init.php');

include 'loginCheck.php';

$title = "Course Overview";
$pageName = 'courseOverview';
$pageTitle = 'Sales Coach';
$pageIcon = 'images/sales-coach.png';

//Check for login Session

$person = $user->person;

$courses = $dbContext['Courses']->getAllApproved();

$categories = array();
foreach($courses as $course){
    $cats = $dbContext['Categories']->findByCourseGUID($course->guid);
    foreach($cats as $c){
        if(!isset($categories[$c->guid])){
            $categories[$c->guid] = $c;
        }
    }
}
$courseHistories = $dbContext['CourseHistories']->findByPersonGUID($person->guid);

$passedCourses = 0;
foreach($person->courseHistory as $ch){
    if($ch->grade > 70){
        $passedCourses++;
    }
}

if(isset($_SESSION['course'])){
    unset($_SESSION['course']);
}
if(isset($_SESSION['questions'])){
    unset($_SESSION['questions']);
}
if(isset($_SESSION['questionProgress'])){
    unset($_SESSION['questionProgress']);
}
if(isset($_SESSION['questionHistories'])){
    unset($_SESSION['questionHistories']);
}

//Include HTML head
include_once('head.php');
?>
<section id="main" class="courseOverview">
    <div id="site-wrapper">
        <?php 
            //Include page header
            include_once('header.php');
        ?>
        <div id="nav-dropdown">
            <?php include_once('nav.php');?>
        </div>
        <div id="filter">
            Filter
            <select>
                <option value ="all">All</option>
                <?php foreach($categories as $category):?>
                <option value="<?=$category->guid?>"><?=$category->name?></option>
                <?php endforeach;?>
            </select>
        </div>
        <div id="courses">
            <?php 
            $i = 2;
                foreach($courses as $course):
                    $courseCategories = array();
                    foreach($course->categories as $category):
                        $courseCategories[] = $category->guid;
                    endforeach;
                    $relatedCourseHistory = $dbContext['CourseHistories']->find(array('course' => $course->guid, 'person' => $person->guid));
                    $passed = FALSE;
                    $grade = 0;
                    if(is_array($relatedCourseHistory)):
                        foreach($relatedCourseHistory as $ch):
                            if($ch->grade > 70 && !$passed):
                                $passed = TRUE;
                                $grade = $ch->grade;
                            elseif($ch->grade > $grade):
                                $grade = $ch->grade;
                            endif;
                        endforeach;
                    endif;
                    preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $course->videos->location, $matches);
?>
            <div class="box<?=$i%3 === 0?' margin':''?>" data-category='<?=implode(',',$courseCategories)?>' style="background-image:url('http://i.ytimg.com/vi/<?=$matches[0]?>/maxresdefault.jpg');">
                <a href="courseVideo.php?<?=http_build_query(array('course'=> $course->guid))?>" class="box-contents">
                    <h2><?=$course->title?></h2>
                
                <div class ='box-footer'>
                    <span class='big-blue'>
                    <?=($passed?"Retake Course":"Take Course")?>
                    </span>
                    <p><?=($passed)?'Current Score ' . $grade:''?></p>
                </div>
                </a>
            </div>
            <?php 
            $i++;
            endforeach;?>
        </div>
    </div>
</section>
<?php include_once 'footer.php';
