<?php

class PersonRanking{
    public $guid;
    public $name;
    public $average;
    
    public function __construct($guid, $name, $average) {
        $this->guid = $guid;
        $this->name = $name;
        $this->average = $average;
    }
}

function compareRanking($person1, $person2){
    if($person1->average === $person2->average)
        return 0;
    
    return ($person1->average > $person2->average)?-1:1;
}

include 'includes/Config.php';
include 'includes/dbContext.php';

$db = new PDO('mysql:dbname='.Config::$dbSchema.';host='.Config::$dbHost, Config::$dbUser, Config::$dbPass);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$db->query("SET time_zone = '+5:00'");
$dbContext = new dbContext($db);

$rankedPersons = array();
$persons = $dbContext['Persons']->getAll();
$courses = $dbContext['Courses']->getAllApproved();

foreach($courses as $course){
    $courseGUIDs[] = $course->guid;
}

foreach($persons as $person){
    $user = new PersonRanking($person->guid, "{$person->firstName} {$person->lastName}", 0);
    $courseHistories = array();
    foreach($person->courseHistory as $ch){
        if(in_array($ch->courseGUID, $courseGUIDs)){
            if(!isset($courseHistories[$ch->courseGUID])){
                $courseHistories[$ch->courseGUID] = $ch;
            }else{
                if($ch->timestamp > $courseHistories[$ch->courseGUID]->timestamp){
                    $courseHistories[$ch->courseGUID] = $ch;
                }
            }
        }
    }
    
    //At this point $courseHistories is all of a users most recent attempts on active courses
    
    $sum = 0;
    $count = 0;
    foreach($courseHistories as $ch){
        //We only want to count active courses
        $course = array_values($dbContext['Courses']->find($ch->courseGUID))[0];
        if($course->active){
            $sum += $ch->grade;
            $count++;
        }
    }
    
    //And we want to assign a score of 0 for each uncompleted course
    $courses = $dbContext['Courses']->getAll();
    
    foreach($courses as $course){
        if($course->active && !isset($courseHistories[$course->guid])){
            $count++;
        }
    }
    
    if($count > 0 && $sum > 0){
        $user->average = $sum/$count;
    }else{
        $user->average = 0;
    }
    
    $rankedPersons[] = $user;
}

usort($rankedPersons, 'compareRanking');

file_put_contents('/opt/www/windsorwonsource.com/public/cache/personRankings.json', json_encode($rankedPersons));