<?php

include '../init.php';

include 'loginCheck.php';

$course = new Course();
if(isset($_GET['questionPool'])){
    $course->questionPool = array_values($dbContext['QuestionPools']->find($_GET['questionPool']))[0];
}
$dbcategories = $dbContext['Categories']->getAll();
$questionPools = $dbContext['QuestionPools']->getAll();

$errors = array();

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    function validateAttr($attr){
        return (isset($attr) && !empty($attr) && $attr !== FALSE);
    }
    $course->title = filter_input(INPUT_POST, 'courseTitle', FILTER_SANITIZE_STRING);
    if(!validateAttr($course->title)){
        $errors[] = "You must enter a valid course title";
    }
    
    $videoURL = filter_input(INPUT_POST, 'video', FILTER_SANITIZE_URL);
    $course->videos = new Video(null, $videoURL);
    if(!validateAttr($videoURL)){
        $errors[] = "You must enter a valid link to the course video on YouTube";
    }
    
    $course->description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
    if(!validateAttr($course->description)){
        $errors[] = "You must enter a valid course description";
    }
    
    if(isset($_POST['categories'])){
        $categories = $_POST['categories'];
        foreach($categories as $category){
            if(isset($category['checked'])){
                if($category['checked'] == 'newCategory'){
                    if(isset($category['name'])){
                        $course->categories[] = new Category(null, $category['name']);
                    }
                }else{
                    $course->categories[] = array_values($dbContext['Categories']->find($category['checked']))[0];
                }
            }
        }
    }else{
        $errors[] = "You must select at least one category";
    }
    
    
    $questionPool = $dbContext['QuestionPools']->find(filter_input(INPUT_POST, 'questionPool', FILTER_SANITIZE_STRING));
    $course->questionPool = array_values($questionPool)[0];
    if(count($questionPool) <= 0){
        $errors[] = "You must select a question pool";
    }
    
    $course->numberOfQuestions = filter_input(INPUT_POST, 'numberOfQuestions', FILTER_SANITIZE_NUMBER_INT);
    if(!validateAttr($course->numberOfQuestions)){
        $errors[] = "You must enter the number of questions you want to use for this question pool";
    }
    
    if(count($errors) <= 0){
        $course->active = false;
        $dbContext['Courses']->addOrEdit($course);
        $dbContext['Courses']->save();
        header('Location: courses.php');
    }
}
    $title = "Ajinomoto Windsor Prep School Admin";
    $pageName = "createcourse";
    include 'header.php';
?>
<section id="main">
    <h2>Create New Course</h2>
    <?php if(count($errors) > 0):?>
    <div class='error'>
        <h2>Errors</h2>
        <ul>
            <?php foreach($errors as $error):?>
            <li><?=$error?></li>
            <?php endforeach;?>
        </ul>
    </div>
    <?php endif;?>
    <?php if(count($questionPools) === 0):?>
    <p class="warning">No question pools have been created yet.  You must <a href='createQuestionPool.php'>Create at least one question pool</a> before you can create a course.</p>
    <?php else:?>
    <form method='post' style="margin-bottom: 1.5em;">
        
        <p>
            <label for='courseTitle'>Title</label><br>
            <input type='text' name='courseTitle' id='courseTitle' value='<?=(isset($course->title)?$course->title:"")?>'>        
        </p>
        <p>
            <label for='video'>Video Link</label><br>
            <input type='text' name='video' id='video' value='<?=(isset($course->videos[0]->location)?$course->videos[0]->location:"")?>'>
        </p>
        <p>
            <label for="description">Description</label>
            <textarea name='description' id='description'><?=isset($course->description)?$course->description:""?></textarea>
        </p>
        <div class="clearfix">
            <div class='column left'>
                <h3>Categories</h3>
                <table class='datatable' id='categories'>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Category</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if(count($dbcategories) > 0):?>
                        <?php foreach($dbcategories as $category):?>
                            <tr>
                                <td><input type='checkbox' name='categories[<?=$category->guid?>][checked]' value='<?=$category->guid?>' <?=(isset($course->categories)?(in_array($category, $course->categories)?"checked":""):"")?>></td>
                                <td><?=$category->name?></td>
                            </tr>
                        <?php endforeach;?>
                    <?php else:?>
                            <tr>
                                <td><input type='checkbox' name='categories[0][checked]' value='newCategory'></td>
                                <td><input type='textbox' name='categories[0][name]' placeholder='Category Name'></td>
                            </tr>
                    <?php endif;?>
                    </tbody>
                </table>
                <button class='addCategory'><img src='../images/add.png'> Create New Category</button>
            </div>
            <div class='column right'>
                <h3>Question Pools</h3>
                <?php if(count($questionPools) > 0):?>
                <table class='datatable' id='questionPools'>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Question Pool</th>
                            <th>Question Count</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($questionPools as $questionPool):?>
                        <tr>
                            <td><input type='radio' name='questionPool' value='<?=$questionPool->guid?>' <?=$questionPool->guid === $course->questionPool->guid?"checked":""?> required></td>
                            <td><?=$questionPool->name?></td>
                            <td><?=count($questionPool->questions)?></td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
                <?php else:?>
                <p>No question pools have been created yet.  You must <a href='createQuestionPool.php'>Create at least one question pool</a> before you can create a course.</p>
                <?php endif;?>
                <p>
                    <label for='numOfQuestions'>Number of Questions from Pool</label>
                    <input type='text' name='numberOfQuestions' id='numOfQuestions' value='<?=isset($course->numberOfQuestions)?$course->numberOfQuestions:""?>'>
                </p>
            </div>
        </div>
        <input class="right" type='submit' value='Save Course'>
    </form>
    <?php endif;?>
</section>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?=Config::$siteRoot?>\/scripts\/lib\/jquery.min.js"><\/script>')</script>
<!-- Scripts at the bottom for speed -->
<script src="<?=Config::$siteRoot?>/scripts/lib/jquery-ui-1.10.3.custom.min"></script>
<script src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
<script src="scripts/main.js"></script>
</body>
</html>

