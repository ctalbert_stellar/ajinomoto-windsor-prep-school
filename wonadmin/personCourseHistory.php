<?php
include '../init.php';

include 'loginCheck.php';
$title = "Ajinomoto Windsor Prep School Admin";
$pageName = 'wonadmin';
include 'header.php';

$person = array_values($dbContext['Persons']->find(urldecode($_GET['person'])))[0];
?>
<section id="main">
    <?php if(count($person->courseHistory) > 0):?>
    <table class="datatable">
        <thead>
            <tr>
                <th>Date</th>
                <th>Course</th>
                <th>Score</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($person->courseHistory as $courseHistory):?>
            <tr>
                <td><?=$courseHistory->timestamp?></td>
                <td><?=$courseHistory->title?></td>
                <td><?=$courseHistory->grade?></td>
                <td><a href="courseHistoryDetails.php?<?=http_build_query(array('courseHistory' => $courseHistory->guid))?>">View Details</a></td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
    <?php else:?>
    <p>This person has not completed any courses as of this time.</p>
    <?php endif;?>
</section>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?=Config::$siteRoot?>\/scripts\/lib\/jquery.min.js"><\/script>')</script>
<!-- Scripts at the bottom for speed -->
<script src="<?=Config::$siteRoot?>/scripts/lib/jquery-ui-1.10.4.min.js"></script>
<script src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
<script src="scripts/main.js"></script>

</body>
</html>
