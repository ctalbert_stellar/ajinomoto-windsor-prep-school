<?php
if(!isset($user)){
    header("Location: ../login.php");
    exit();
}elseif(!$user->isApproved){  
    $_SESSION['Message'] = "Sorry, this account hasn't been approved yet.";
    header("Location: ../login.php");
    exit();
}elseif(!$user->isAdmin){
    header('HTTP/1.0 403 Forbidden');
    echo "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">
<html><head>
<title>403 Forbidden</title>
</head><body>
<h1>Forbidden</h1>
<p>You don't have permission to access this resource.</p>
<p>Additionally, a 403 Forbidden
error was encountered while trying to use an ErrorDocument to handle the request.</p>
</body></html>";
    die();
}

