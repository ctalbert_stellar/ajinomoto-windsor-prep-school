//jQuery won't conflict with other libraries that might use $
jQuery.noConflict();

jQuery(function($){
	//jQuery Document Ready
	$('html').removeClass('no-js');
});

//Stub Console Methods if they don't exist.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

jQuery(function($){
    $('.datatable').dataTable();
    
    $('#addAddress').click(function(e){
        e.preventDefault();
        $.get('addNewAddress.php',
        function(response){
            $('.datatable').dataTable().fnDestroy();
            $('.datatable tr:last').after('<tr>' +
                '<td><input type="text" name="address[' + response + '][lineOne]" placeholder="Line One"></td>' +
                '<td><input type="text" name="address[' + response + '][lineTwo]" placeholder="Line Two"></td>' +
                '<td><input type="text" name="address[' + response + '][city]" placeholder="City"></td>' +
                '<td><input type="text" name="address[' + response + '][state]" placeholder="State"></td>' +
                '<td><input type="text" name="address[' + response + '][zip]" placeholder="Zip"></td>' +
            '</tr>');
            $('.datatable').dataTable();
        });
        return false;
    });
    
    $('#companySelect').change(function(){
        if(this.value === 'create'){
            $('#newCompany').toggleClass('hidden', false);
        }else{
            $('#newCompany').toggleClass('hidden', true);
        }
    });
    
    $('.addNewAnswer').click(function(e){
        e.preventDefault();
        var guid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
        var question = $(this).data('question');
        $('#question-' + question).dataTable().fnDestroy();
        $('#question-' + question +' tr:last').after('<tr>' +
                    '<td><input type="checkbox" name="questions['
                    + question + '][answers][' + guid + '][correct]"></td>' +
                    '<td><input type="text" name="questions['
                    + question + '][answers][' + guid + '][text]"></td>' +
                '</tr>'
                );
        $('#question-' + question).dataTable();
        $('.removeLastAnswer[data-question="' + question + '"]').removeAttr('disabled');
        return false;   
    });
    
    $('.removeLastAnswer').click(function(e){
        e.preventDefault();
        var question = $(this).data('question');
        $('#question-' + question).dataTable().fnDestroy();
        $('#question-' + question + ' tr:last').remove();
        $('#question-' + question).dataTable();
        if($('#question-' + question + ' tbody>tr').length === 1){
            $(this).attr('disabled', 'disabled');
        }
        return false;
    });
    
    $('.addNewQuestion').click(function(e){
        e.preventDefault();
        var questionCounter = $(this).attr('data-questionCounter');
        $('.question:last').after('<div class="question">' +
            '<p><label for="question-' + questionCounter + '-question">Question Text</label><br>'+
            '<input class="question-text" type="text" name="questions[' + questionCounter + '][text]" id="question-'+questionCounter+'-question" required></p>'+
            '<table class="datatable question-table" id="question-' + questionCounter + '">'+
                '<thead>' +
                    '<tr>' +
                        '<th>Correct Answer</th>' +
                        '<th class="wide">Answer Text</th>' +
                    '</tr>' +
                '</thead>' +
                '<tbody>' + 
                    '<tr>' +
                        '<td class="center"><input type="checkbox" name="questions[' + questionCounter + '][answers][0][correct]"></td>' +
                        '<td><input type="text" name="questions[' + questionCounter + '][answers][0][text]" required></td>'+
                    '</tr>' +
                    '<tr>' +
                        '<td class="center"><input type="checkbox" name="questions[' + questionCounter + '][answers][1][correct]"></td>' +
                        '<td><input type="text" name="questions[' + questionCounter + '][answers][1][text]" required></td>'+
                    '</tr>' +
                    '<tr>' +
                        '<td class="center"><input type="checkbox" name="questions[' + questionCounter + '][answers][2][correct]"></td>' +
                        '<td><input type="text" name="questions[' + questionCounter + '][answers][2][text]" required></td>'+
                    '</tr>' +
                    '<tr>' +
                        '<td class="center"><input type="checkbox" name="questions[' + questionCounter + '][answers][3][correct]"></td>' +
                        '<td><input type="text" name="questions[' + questionCounter + '][answers][3][text]" required></td>'+
                    '</tr>' +
                '</tbody>' +
            '</table>' +
            '<button class="addNewAnswer" data-question="' + questionCounter + '"><img src="../images/add.png"> Add New Answer</button>'+
            '<button class="removeLastAnswer" data-question="' + questionCounter + '"><img src="../images/remove.png"> Remove Last Answer</button>' + 
        '</div>'
        );
        $('#question-' + questionCounter).dataTable();
        questionCounter++;
        $(this).attr('data-questionCounter', questionCounter);
        $('.question .addNewAnswer').off("click");
        $('.question .addNewAnswer').on("click", function(){
            e.preventDefault();
            var guid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
                return v.toString(16);
            });
            var question = $(this).data('question');
            $('#question-' + question).dataTable().fnDestroy();
            $('#question-' + question +' tr:last').after('<tr>' +
                        '<td><input type="checkbox" name="questions['
                        + question + '][answers][' + guid + '][correct]"></td>' +
                        '<td><input type="text" name="questions['
                        + question + '][answers][' + guid + '][text]"></td>' +
                    '</tr>'
                    );
            $('#question-' + question).dataTable();
            $('.removeLastAnswer[data-question="' + question + '"]').removeAttr('disabled');
            return false;
        });
        $('.question .removeLastAnswer').off("click");
        $('.question .removeLastAnswer').on("click", function(){
            var question = $(this).data('question');
            $('#question-' + question).dataTable().fnDestroy();
            $('#question-' + question + ' tr:last').remove();
            $('#question-' + question).dataTable();
            if($('#question-' + question + ' tbody>tr').length === 1){
                $(this).attr('disabled', 'disabled');
            }
            return false;
        });
        
        $('.removeLastQuestion').removeAttr('disabled');
        return false;
    });
    
    $('.removeLastQuestion').click(function(e){
        e.preventDefault();
        $('.question:last').remove();
        var questionCounter = $('.addNewQuestion').attr('data-questionCounter');
        questionCounter--;
        $('.addNewQuestion').attr('data-questionCounter', questionCounter);
        if($('.question').length === 1){
            $(this).attr('disabled', 'disabled');
        }
        return false;
    });
    
    var categoryCounter = 1;
    $('.addCategory').click(function(e){
        e.preventDefault();
        $('#categories').dataTable().fnDestroy();
        $('#categories tr:last').after('<tr>'+
                '<td><input type="checkbox" name="categories[' + categoryCounter + '][checked]" value="newCategory"></td>'+
                '<td><input type="textbox" name="categories[' + categoryCounter + '][name]" placeholder="Category Name"></td>'+
            '</tr>');
        $('#categories').dataTable();
        categoryCounter++;
        return false;
    }); 
    if(typeof plupload !== 'undefined'){
   	$("#uploader").plupload({
		// General settings
		runtimes : 'html5,flash,silverlight,html4',
		url : 'upload.php',

		// User can upload no more then 20 files in one go (sets multiple_queues to false)
		max_file_count: 20,
		
		chunk_size: '1mb',
		
		filters : {
			// Maximum file size
			max_file_size : '500mb',
			// Specify what files to browse for
			mime_types: [
				{title : "Image files", extensions : "jpg,jpeg,gif,png"},
				{title : "Video files", extensions : "mp4"}
			]
		},

		// Rename files by clicking on their titles
		rename: true,
		
		// Sort files
		sortable: true,

		// Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
		dragdrop: true,

		// Views to activate
		views: {
			list: true,
			thumbs: true, // Show thumbs
			active: 'thumbs'
		},

		// Flash settings
		flash_swf_url : '../../js/Moxie.swf',

		// Silverlight settings
		silverlight_xap_url : '../../js/Moxie.xap',

                
                init: {
                    FileUploaded: function(up, file, info){
                        var catString = '';
                        var catLength = categories.length;
                        for(var i =0; i < catLength; i++){
                            catString += '<tr>' + 
                                    '<td><input type="checkbox" name="categories[' + file.name + '][]" value="' + categories[i][0] + '"></td>' +
                                    '<td>' + categories[i][1] + '</td>' +
                                '</tr>';
                        }
                        var courseString = '';
                        var courseLength = courses.length;
                        for(var i = 0; i < courseLength; i++){
                            courseString += '<tr>' + 
                                    '<td><input type="radio" name="course[' + file.name + ']" value="' + courses[i][0] + '"></td>' +
                                    '<td>' + courses[i][1] + '</td>' +
                                '</tr>';
                        }
                        
                        $('#resources').append(
                            '<div class="resource-wrapper clearfix">' +
                            '<h3>File: ' + file.name + '</h3>' +
                            '<input type="hidden" name="filenames[]" value="' + file.name + '">' +
                            '<input type="hidden" name="types[' + file.name + ']" value="'+ file.type +'">' +
                            '<div class="column left">' +
                                '<h4>Categories</h4>' +
                                '<table class="datatable">'+
                                    '<thead>' +
                                        '<tr>' +
                                            '<th></th>' +
                                            '<th>Category</th>' +
                                        '</tr>' +
                                    '</thead>' +
                                    '<tbody>' +
                                        catString +
                                    '</tbody>' +
                                '</table>' +
                            '</div>' +
                            '<div class="column right">' +
                                '<h4>Course</h4>' +
                                '<table class="datatable">'+
                                    '<thead>' +
                                        '<tr>' +
                                            '<th></th>' +
                                            '<th>Course</th>' +
                                        '</tr>' +
                                    '</thead>' +
                                    '<tbody>' +
                                        courseString +
                                    '</tbody>' +
                                '</table>' +
                            '</div>' +
                            '</div>'
                        );
                        $('.datatable').dataTable();
                    },
                    UploadComplete: function(up, files){
                        if($('#resources-submit').length === 0){
                            $('#resources').append(
                                    '<input id="resources-submit" type="submit" value="Save">'
                            );
                        }else{
                            $('#resources-submit').prop('disabled', false);
                        }
                    },
                    FilesAdded: function(up, files){
                        if($('#resources-submit').length !== 0){
                            $('#resources-submit').prop('disabled', true);
                        }
                    }
                }
	});
    }
    
    $('.resources img').hover(function(){
        var img = $(this).attr('src');
        $('#imgPreview').append('<img src="' + img + '">');
        $('#imgPreview').show();
    }, function(){
        $('#imgPreview').hide();
        $('#imgPreview img').remove();
    });
});

