<?php

include '../init.php';

include 'loginCheck.php';

if(!isset($_GET['course']) || !isset($_GET['action'])){
    header("Location: courses.php");
    exit();
}

$course = array_values($dbContext['Courses']->find(urldecode($_GET['course'])))[0];
if(filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING) === 'activate'){
    $course->active = TRUE;
}elseif(filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING) === 'deactivate'){
    $course->active = FALSE;
}

$dbContext['Courses']->addOrEdit($course);
$dbContext['Courses']->save();

header("Location: courses.php");
exit();

