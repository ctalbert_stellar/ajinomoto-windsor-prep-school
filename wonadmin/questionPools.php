<?php
include '../init.php';

include 'loginCheck.php';

$title = "Ajinomoto Windsor Prep School Admin";
$pageName = 'wonadmin';
include 'header.php';

$questionPools = $dbContext['QuestionPools']->getAll();
?>
<section id='main'>
    <?php if(count($questionPools) > 0):?>
    <table class='datatable'>
        <thead>
            <tr>
                <th>Name</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($questionPools as $questionPool):?>
            <tr>
                <td><?=$questionPool->name?></td>
                <td><a href="editQuestionPool.php?<?=http_build_query(array('questionPool' => $questionPool->guid))?>">Edit</a></td>
            </tr>
        <?php endforeach;?>
        </tbody>
    </table>
    <?php else:?>
        <p>No Question Pools created yet.</p>
    <?php endif;?>
        <a href="createQuestionPool.php"><img src="../images/add.png"> Create New Question Pool</a>
</section>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?=Config::$siteRoot?>\/scripts\/lib\/jquery.min.js"><\/script>')</script>
<!-- Scripts at the bottom for speed -->
<script src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
<script src="scripts/main.js"></script>
</body>
</html>
