<nav class="clearfix">
    <ul>
        <li><a href='persons.php' title="Information about all registered users">Users</a></li>
        <li><a href='companies.php' title="Information about all companies with registered users">Companies</a></li>
        <li><a href='courses.php' title="Create or edit courses">Courses</a></li>
        <li><a href='questionPools.php' title="Create or edit question pools for use in courses">Question Pools</a></li>
        <!--<li><a href='resources.php' title="Upload or edit resources for use in pitches">Resources</a></li>-->
    </ul>
</nav>