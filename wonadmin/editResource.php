<?php
include '../init.php';

include 'loginCheck.php';

$courses = $dbContext['Courses']->getAll();
$categories = $dbContext['Categories']->getAll();
$resource = array_values($dbContext['Resources']->find(urldecode($_GET['resource'])))[0];

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    if(isset($_POST['categories'])){
        $resource->categories = array();
        foreach($_POST['categories'] as $category){
            $resource->categories[] = array_values($dbContext['Categories']->find($category))[0];
        }
    }
    
    if(isset($_POST['course'])){
        $resource->unlockedBy = $_POST['course'];
    }
    
    $dbContext['Resources']->addOrEdit($resource);
    $dbContext['Resources']->save();
    header("Location: resources.php");
    exit();
}

include 'header.php';
?>
<section id="main">
    <h2>Edit Resource</h2>
    <img src="<?=$resource->location?>" style="width: 40%;"><br>
    <p><?=$resource->location?></p>
    <form method="post">
        <div class="column left">
            <h3>Categories</h3>
            <table class="datatable">
                <thead>
                    <tr>
                        <th></th>
                        <th>Category Name</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($categories as $category):?>
                    <tr>
                        <td><input type="checkbox" name="categories[]" value="<?=$category->guid?>" <?=(in_array($category, $resource->categories)?"checked":"")?>></td>
                        <td><?=$category->name?></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        </div>
        <div class="column right">
            <h3>Courses</h3>
            <table class="datatable">
                <thead>
                    <tr>
                        <th></th>
                        <th>Course Title</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($courses as $course):?>
                    <tr>
                        <td><input type="radio" name="course" value="<?=$course->guid?>" <?=($course->guid === $resource->unlockedBy?"checked":"")?>></td>
                        <td><?=$course->title?></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
        <input type="submit" value="Save">
    </form>
</section>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?=Config::$siteRoot?>\/scripts\/lib\/jquery.min.js"><\/script>')</script>
<!-- Scripts at the bottom for speed -->
<script src="<?=Config::$siteRoot?>/scripts/lib/jquery-ui-1.10.4.min.js"></script>
<script src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
<script src="scripts/lib/plupload/plupload.full.min.js"></script>
<script type="text/javascript" src="scripts/lib/plupload/jquery.ui.plupload/jquery.ui.plupload.min.js"></script>
<script src="scripts/main.js"></script>