<?php
include '../init.php';

include 'loginCheck.php';


$questionPool = array_values($dbContext['QuestionPools']->find($_GET['questionPool']))[0];

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    function validateAttr($attr){
        return (isset($attr) && !empty($attr) && $attr !== FALSE);
    }
    
    $questionPoolName = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
    if(validateAttr($questionPoolName)){
        $questionPool->name = $questionPoolName;
    }
    
    $questions = $_POST['questions'];
    
    $currentQuestions = array();
    foreach($questionPool->questions as $q){
        $currentQuestions[] = $q->guid;
    }
    
    foreach($questions as $guid => $question){
        $questionObj = new Question();
        $guid = '{' . $guid . '}';
        if(in_array($guid, $currentQuestions)){
            $questionObj->guid = $guid;
        }


        $text = filter_var($question['text'], FILTER_SANITIZE_STRING);
        if(validateAttr($text)){
            $questionObj->questionText = $text;
            foreach($question['answers'] as $answer){
                $questionObj->potentialAnswers[] = filter_var($answer['text'], FILTER_SANITIZE_STRING);
                if(isset($answer['correct'])){
                    $questionObj->correctAnswers[] = filter_var($answer['text'], FILTER_SANITIZE_STRING);
                }
            }

            if(in_array($questionObj->guid, $currentQuestions)){
                $questionPool->questions[$questionObj->guid] = $questionObj;
            }else{
                $questionPool->questions[] = $questionObj;
            }
            
        }
    }
    
    $dbContext['QuestionPools']->addOrEdit($questionPool);
    $dbContext['QuestionPools']->save();
    
    header("Location: questionPools.php");
    exit();
}
$title = "Ajinomoto Windsor Prep School Admin";
$pageName = 'wonadmin';
include 'header.php';

?>
<section id='main'>
    <h2>Edit Question Pool</h2>
    <form method='post'>
    <p>
        <label for='name'>Name</label><br>
        <input type='text' name='name' id='name' value="<?=$questionPool->name?>" required>
    </p>
    <?php $i = 0;
    foreach($questionPool->questions as $question):
        ?>
    <div class='question'>
        <p>
            <label for='question-<?=$i?>-question'>Question Text</label><br>
            <input class="question-text" type='text' name='questions[<?=trim($question->guid, '{}')?>][text]' id='question-0-question'
                   value="<?=$question->questionText?>" required>
        </p>
        <table class='datatable question-table' id='question-<?=trim($question->guid, '{}')?>'>
            <thead>
                <tr>
                    <th>Correct Answer</th>
                    <th class='wide'>Answer Text</th>
                </tr>
            </thead>
            <tbody>
                <?php $j = 0;
                foreach($question->potentialAnswers as $answer):
                    ?>
                <tr>
                    <td class='center'><input type='checkbox' name='questions[<?=trim($question->guid, '{}')?>][answers][<?=$j?>][correct]'
                                              <?=(in_array($answer, $question->correctAnswers === null ? array() : $question->correctAnswers)?"checked='checked'":'')?>></td>
                    <td><input type='text' name='questions[<?=trim($question->guid, '{}')?>][answers][<?=$j?>][text]' value="<?=$answer?>" required></td>
                </tr>
                <?php $j++;
                endforeach;
                ?>
            </tbody>
        </table>
        <button class='addNewAnswer' data-question='<?=trim($question->guid, '{}')?>'><img src='../images/add.png'> Add New Answer</button>
        <button class='removeLastAnswer' data-question='<?=trim($question->guid, '{}')?>'><img src='../images/remove.png'> Remove Last Answer</button>
    </div>
    <?php $i++;
    endforeach;?>
    <button class='addNewQuestion' data-questionCounter="<?=$i?>"><img src='../images/add.png'> Add New Question</button>
    <button class='removeLastQuestion' data-questionCounter="<?=$i?>"><img src='../images/remove.png'> Remove Last Question</button>
    
    <input type='submit' value='Save'>
    </form>
</section>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?=Config::$siteRoot?>\/scripts\/lib\/jquery.min.js"><\/script>')</script>
<!-- Scripts at the bottom for speed -->
<script src="<?=Config::$siteRoot?>/scripts/lib/jquery-ui-1.10.3.custom.min"></script>
<script src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
<script src="scripts/main.js"></script>
</body>
</html>
