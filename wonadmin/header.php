<!DOCTYPE html>
<!--[if lt IE 8]>       <html class="no-js oldie"> <![endif]-->
<!--[if IE 8]>          <html class="no-js ie8"> <![endif]-->
<!--[if IE 9]>          <html class="no-js ie9"> <![endif]-->
<!--[if gt IE 9]><!-->  <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!-- TODO: Set page title -->
    <title><?=isset($title)?$title:"Set your page title";?></title>

    <!-- TODO: Set page description. -->
    <meta name="description" content="">
    
    <!-- TODO: Change Favicon. -->
    <link rel="icon" type="image/png" href="<?=Config::$siteRoot?>/favicon.png">

    <!-- Webfonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link rel="stylesheet" href="<?=Config::$siteRoot?>/styles/normalize.css">
    <link rel="stylesheet" href="<?=Config::$siteRoot?>/wonadmin/styles/main.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.0/css/jquery.dataTables.css">
    <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css" type="text/css" />
    <?php if(isset($styles)):?>
        <?php foreach($styles as $style):?>
    <link rel="stylesheet" href="<?= Config::$siteRoot . "/wonadmin/" . $style?>">
        <?php endforeach;?>
    <?php endif;?>

    <!-- Besides html5shiv and async analytics, all other scripts go before </body> -->
    <script src="<?=Config::$siteRoot?>/scripts/lib/html5shiv.js"></script>
    
    <?php if(!empty(Config::$googleAnalyticsId) && !IS_DEVELOPER): ?>
    <!-- Google Analytics -->
    <script>
    var _gaq=[['_setAccount','<?=Config::$googleAnalyticsId?>'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
    </script>
    <?php endif; ?>
</head>
<body class="<?=$pageName?>">
<!--[if lte IE 7]>
<div class="chromeframe">
	<p>You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
</div>
<![endif]-->
<noscript>
<div class="noscript">
	<p>Your browser has javascript disabled. This website will still display it's contents, but for the best experience, please enable javascript.</p>
</div>
</noscript>
<div id="site-wrapper">
<header id="site-header" class='<?=$pageName?>'>
	<span id="vanity">
		Hello, <?=$user->person->firstName . " " . $user->person->lastName?>
		<a href="../login.php?<?=http_build_query(array('action' => 'logout'))?>">Logout</a> | 
                <a href="../index.php">Return to Prep School</a>
	</span>
    <a href="/wonadmin/" class="heading"><h1 id="heading"><img src="../images/logo.png" alt="Ajinomoto Windsor Prep School Logo"> Admin</h1></a>
</header>
<?php include 'nav.php'?>

