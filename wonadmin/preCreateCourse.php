<?php
include '../init.php';

include 'loginCheck.php';

$title = "Ajinomoto Windsor Prep School Admin";
$pageName = "createcourse";
include 'header.php';
?>
<section id="main">
    <h2>First Thing's First...</h2>
    <p>Do you want to create a new question pool for your course, or create a course using an already existing pool?</p>
    <div class='button-wrapper'>
        <a href='createQuestionPool.php?<?=http_build_query(array('createCourse' => TRUE))?>'><button>New Question Pool</button></a>
    </div>
    <div class='button-wrapper'>
        <a href='createCourse.php'><button>Existing Question Pool</button></a>
    </div>
</section>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?=Config::$siteRoot?>\/scripts\/lib\/jquery.min.js"><\/script>')</script>
<!-- Scripts at the bottom for speed -->
<script src="<?=Config::$siteRoot?>/scripts/lib/jquery-ui-1.10.3.custom.min"></script>
<script src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
<script src="scripts/main.js"></script>
</body>
</html>
