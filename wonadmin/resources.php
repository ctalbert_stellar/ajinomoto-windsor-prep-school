<?php
include '../init.php';

include 'loginCheck.php';

$title = "Ajinomoto Windsor Prep School Admin";
$pageName = 'wonadmin';
include 'header.php';

$resources = $dbContext['Resources']->getAll();
?>
<section id="main">
    <h2>Resources</h2>
    <?php if(count($resources) > 0):?>
    <table class="datatable resources">
        <thead>
            <tr>
                <th>Preview</th>
                <th>Course</th>
                <th>Categories</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($resources as $resource):?>
            <tr>
                <td><img src="<?=$resource->preview?>"></td>
                <td><?=is_null($resource->unlockedBy)?"None":array_values($dbContext['Courses']->find($resource->unlockedBy))[0]->title?></td>
                <td><?php
                    $categoryNames = array();
                    foreach($resource->categories as $category):
                        $categoryNames[] = $category->name;
                    endforeach;?>
                    <?=implode(', ', $categoryNames)?>
                </td>
                <td><a href='editResource.php?<?=http_build_query(array('resource' => $resource->guid))?>'>Edit</a> | <a href='deleteResource.php?<?=http_build_query(array('resource' => $resource->guid))?>'>Delete</a></td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
    <div id="imgPreview">
        
    </div>
    <?php endif;?>
    <a href='uploadResources.php'><button><img src="../images/add.png"> Upload New Resources</button></a>
</section>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?=Config::$siteRoot?>\/scripts\/lib\/jquery.min.js"><\/script>')</script>
<!-- Scripts at the bottom for speed -->
<script src="<?=Config::$siteRoot?>/scripts/lib/jquery-ui-1.10.4.min.js"></script>
<script src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
<script src="scripts/main.js"></script>
</body>
</html>