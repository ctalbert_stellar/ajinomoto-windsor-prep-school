<?php
include '../init.php';

include 'loginCheck.php';

$title = "Ajinomoto Windsor Prep School Admin";
$pageName = 'wonadmin';
include 'header.php';

$courses = $dbContext['Courses']->getAll();
?>
<section id='main'>
    <h2>Courses</h2>
    
    <?php if(count($courses) > 0):?>
    <table class='datatable'>
        <thead>
            <tr>
                <th>Title</th>
                <th>Categories</th>
                <th>Question Pool</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($courses as $course):?>
            <tr>
                <td><?=$course->title?></td>
                <td>
                <?php
                $categoryNames = array();
                foreach($course->categories as $category):
                    $categoryNames[] = $category->name;
                endforeach;?>
                    <?=implode(', ', $categoryNames)?>
                </td>
                <td>
                    <a href="editQuestionPool.php?<?=http_build_query(array('questionPool' => $course->questionPool->guid))?>">Edit Question Pool - <?=$course->questionPool->name?></a>
                </td>
                <td>
                    <a href="editCourse.php?<?=http_build_query(array('course' => $course->guid))?>">Edit</a> |
                    <?php if($course->active):?>
                    <a href="activateCourse.php?<?=http_build_query(array('course' => $course->guid, 'action' => 'deactivate'))?>">Deactivate</a>
                    <?php else:?>
                    <a href="activateCourse.php?<?=http_build_query(array('course' => $course->guid, 'action' => 'activate'))?>">Activate</a>
                    <?php endif;?>|
                    <a href="courseStatistics.php?<?=http_build_query(array('course' => $course->guid))?>">Statistics</a>
                </td>
            </tr>
        <?php endforeach;?>
        </tbody>
    </table>
    <?php else:?>
    <p>No Courses created yet.</p>
    <?php endif;?>
    <a href="preCreateCourse.php"><button><img src="../images/add.png"> Create New Course</button></a>
</section>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?=Config::$siteRoot?>\/scripts\/lib\/jquery.min.js"><\/script>')</script>
<!-- Scripts at the bottom for speed -->
<script src="<?=Config::$siteRoot?>/scripts/lib/jquery-ui-1.10.3.custom.min"></script>
<script src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
<script src="scripts/main.js"></script>
</body>
</html>