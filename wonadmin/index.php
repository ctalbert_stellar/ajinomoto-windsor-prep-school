<?php

include '../init.php';

include 'loginCheck.php';

$title = "Ajinomoto Windsor Prep School Admin";
$pageName = "wonadmin";
include 'header.php';
?>
<section id="main">
    <h2>Please select one of the options above to administrate various aspects of the Prep School system.</h2>
    <dl>
        <dt><a href="persons.php">Users</a></dt>
        <dd>Information about a specific user such as course history, resources available, pitches saved, related company, etc.</dd>
        <dt><a href="companies.php">Companies</a></dt>
        <dd>Information about a given company within the system</dd>
        <dt><a href="courses.php">Courses</a></dt>
        <dd>Create and edit courses including required videos, question pool, number of questions to pull from that question pool, and resources unlocked. </dd>
        <dt><a href="questionPools.php">Question Pools</a></dt>
        <dd>Create and edit question pools for linking to courses and the questions & answers they contain</dd>
        <!--<dt><a href="resources.php">Resources</a></dt>
        <dd>Upload and remove resources that can be related to courses</dd>-->
    </dl>
</section>
