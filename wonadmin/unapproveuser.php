<?php

include '../init.php';

$dbuser = array_values($dbContext['Users']->find(urldecode($_GET['user'])))[0];

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    $dbuser->isApproved = FALSE;
    $dbContext['Users']->addOrEdit($dbuser);
    $dbContext['Users']->save();
    header('Location: persons.php');
    exit();
}

include 'loginCheck.php';

$title = "Ajinomoto Windsor Prep School Admin";
$pageName = "unapproveuser";
include 'header.php';
?>
<section id="main">
    <h2>Unapprove User</h2>
    <p>Are you sure you want to unapprove the following user?</p>
    <div class="info-block">
        <p><strong>Name:</strong> <?=$dbuser->person->firstName?> <?=$dbuser->person->lastName?></p>
        <p><strong>Email:</strong> <?=$dbuser->email?></p>
        <p><strong>Phone:</strong> <?=$dbuser->person->phone?></p>
        <p><strong>District Sales Manager:</strong> <?=(isset($dbuser->person->dsm) && !empty($dbuser->person->dsm))?$dbuser->person->dsm: 'N/A'?></p>
        <p><strong>Broker Number:</strong> <?=(isset($dbuser->person->brokerNumber) && !empty($dbuser->person->brokerNumber))?$dbuser->person->brokerNumber: 'N/A'?></p>
        <p><strong>Address:</strong><br>
            <?=array_values($dbuser->person->addresses)[0]->lineOne?><br>
            <?=(isset(array_values($dbuser->person->addresses)[0]->lineTwo) && !empty(array_values($dbuser->person->addresses)[0]->lineTwo))?array_values($dbuser->person->addresses)[0]->lineTwo.'<br>':''?>
            <?=array_values($dbuser->person->addresses)[0]->city?>,
            <?=array_values($dbuser->person->addresses)[0]->state?>
            <?=array_values($dbuser->person->addresses)[0]->zip?></p>
    </div>
    <h3>Company Information</h3>
    <div class="info-block">
        <p><strong>Name:</strong> <?=$dbuser->person->company->name?></p>
        <p><strong>Address:</strong><br>
            <?=array_values($dbuser->person->company->addresses)[0]->lineOne?><br>
            <?=(isset(array_values($dbuser->person->company->addresses)[0]->lineTwo) && !empty(array_values($dbuser->person->company->addresses)[0]->lineTwo))?array_values($dbuser->person->addresses)[0]->lineTwo.'<br>':''?>
            <?=array_values($dbuser->person->company->addresses)[0]->city?>,
            <?=array_values($dbuser->person->company->addresses)[0]->state?>
            <?=array_values($dbuser->person->company->addresses)[0]->zip?></p>
    </div>
    <div class="info-block">
    <form method="post">
        <input type="submit" value="Unapprove"> <a href="persons.php" class="pad-left">Cancel</a>
    </form>
    </div>
</section>