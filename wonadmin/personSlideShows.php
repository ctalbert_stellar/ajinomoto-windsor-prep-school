<?php
include '../init.php';

include 'loginCheck.php';
$title = "Ajinomoto Windsor Prep School Admin";
$pageName = 'wonadmin';
include 'header.php';

$person = array_values($dbContext['Persons']->find(urldecode($_GET['person'])))[0];
?>
<section id="main">
    <?php if(count($person->slideshows) > 0):?>
    <table class="datatable">
        <thead>
            <tr>
                <th>Date Updated</th>
                <th>Title</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($person->slideshows as $slideshow):?>
            <tr>
                <td><?=$slideshow->updated->format('m/d/y - g:i:s A')?></td>
                <td><?=$slideshow->title?></td>
                <td><a href="slideshowDetails.php?<?=http_build_query(array('slideshow' => $slideshow->guid))?>">View Details</a></td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
    <?php else:?>
    <p>This person has not created any slide shows as of this time.</p>
    <?php endif;?>
</section>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?=Config::$siteRoot?>\/scripts\/lib\/jquery.min.js"><\/script>')</script>
<!-- Scripts at the bottom for speed -->
<script src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
<script src="scripts/main.js"></script>

</body>
</html>
