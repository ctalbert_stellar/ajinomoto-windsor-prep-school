<?php
include '../init.php';

include 'loginCheck.php';

$company = array_values($dbContext['Companies']->find(urldecode($_GET['company'])))[0];

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    $name = filter_input(INPUT_POST, 'companyName', FILTER_SANITIZE_STRING);
    if(isset($name) && !empty($name) && $name !== FALSE){
        $company->name = $name;
    }
    
    $addresses = $_POST['address'];
    foreach($addresses as $guid => $address){
        $_guid = filter_var($guid, FILTER_SANITIZE_STRING);
        if(isset($_guid) && !empty($_guid) && $_guid !== FALSE){
            $addressObject = new Address($_guid);
            foreach($address as $key => $field){
                $$key = filter_var($address[$key], FILTER_SANITIZE_STRING);
                if(isset($$key) && !empty($$key) && $$key !== FALSE){
                    $addressObject->$key = $$key;
                }
            }
            $company->addresses[$_guid] = $addressObject;
        }
    }
    
    $dbContext['Companies']->addOrEdit($company);
    $dbContext['Companies']->save();
    header("Location: companies.php");
    exit();
}
$title = "Ajinomoto Windsor Prep School Admin";
$pageName = "editperson";
include 'header.php';
?>
<section id="main">
<h2>Edit Company</h2>
<form method="post">
    <p>
        <label for="companyName">Company Name</label>
        <input type="text" name="companyName" id="companyName" value="<?=$company->name?>">
    </p>
    <h3>Addresses</h3>
    <table class="datatable" id="companyAddresses">
        <thead>
            <tr>
                <th>Line One</th>
                <th>Line Two</th>
                <th>City</th>
                <th>State</th>
                <th>Zip</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($company->addresses as $address):?>
            <tr>
                <td><input type="text" name="address[<?=$address->guid?>][lineOne]" value='<?=$address->lineOne?>' placeholder="Line One"></td>
                <td><input type="text" name="address[<?=$address->guid?>][lineTwo]" value='<?=$address->lineTwo?>' placeholder="Line Two"></td>
                <td><input type="text" name="address[<?=$address->guid?>][city]" value='<?=$address->city?>' placeholder="City"></td>
                <td><input type="text" name="address[<?=$address->guid?>][state]" value='<?=$address->state?>' placeholder="State"></td>
                <td><input type="text" name="address[<?=$address->guid?>][zip]" value='<?=$address->zip?>' placeholder="Zip"></td>
            </tr>
        <?php endforeach;?>
        </tbody>
    </table>
    <button id="addAddress"><img src="../images/add.png">Add New Address</button>
    <input type="submit" value="Save">
</form>
</section>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?=Config::$siteRoot?>\/scripts\/lib\/jquery.min.js"><\/script>')</script>
<!-- Scripts at the bottom for speed -->
<script src="<?=Config::$siteRoot?>/scripts/lib/jquery-ui-1.10.3.custom.min"></script>
<script src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
<script src="scripts/main.js"></script>

</body>
</html>