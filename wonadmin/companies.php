<?php
include '../init.php';

include 'loginCheck.php';

$title = "Ajinomoto Windsor Prep School Admin";
$pageName = 'wonadmin';
include 'header.php';

$companies = $dbContext['Companies']->getAll();
?>
<section id='main'>
    <h2>Companies</h2>
    <table class='datatable'>
        <thead>
            <tr>
                <th>Name</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($companies as $company):?>
            <tr>
                <td><?=$company->name?></td>
                <td>
                    <a href="editCompany.php?<?=http_build_query(array('company' => $company->guid))?>">Edit</a> | <a href="companyStatistics.php?<?=http_build_query(array('company' => $company->guid))?>">Statistics</a>
                </td>
            </tr>
        <?php endforeach;?>
        </tbody>
    </table>
</section>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?=Config::$siteRoot?>\/scripts\/lib\/jquery.min.js"><\/script>')</script>
<!-- Scripts at the bottom for speed -->
<script src="<?=Config::$siteRoot?>/scripts/lib/jquery-ui-1.10.3.custom.min"></script>
<script src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
<script src="scripts/main.js"></script>

</body>
</html>

