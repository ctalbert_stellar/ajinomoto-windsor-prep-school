<?php
include '../init.php';

include 'loginCheck.php';

$resource = array_values($dbContext['Resources']->find(urldecode($_GET['resource'])))[0];
$course = array_values($dbContext['Courses']->find($resource->unlockedBy))[0];

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    $resource->isDeleted = TRUE;
    $dbContext['Resources']->addOrEdit($resource);
    $dbContext['Resources']->save();
    header('Location: resources.php');
    exit();
}

include 'header.php';
?>
<section id="main">
    <img src="<?=$resource->location?>" style="width: 40%;"><br>
    <p><?=$resource->location?></p>
    <p>Unlocked By: <?=$course->title?></p>
    <form method="post">
        <p>Are you sure you want to delete this resource? This will remove it from all slideshows that currently use it and any user resource collections.</p>
        <input type="submit" value="Delete"> <a href="resources.php">Cancel</a>
    </form>
</section>