<?php

include '../init.php';

include 'loginCheck.php';

$title = "Ajinomoto Windsor Prep School Admin";
$pageName = 'wonadmin';
include 'header.php';

$questionPool = new QuestionPool();

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    function validateAttr($attr){
        return (isset($attr) && !empty($attr) && $attr !== FALSE);
    }
    
    $questionPoolName = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
    if(validateAttr($questionPoolName)){
        $questionPool->name = $questionPoolName;
    }
    else{
        
    }
    
    $questions = $_POST['questions'];
    
    foreach($questions as $question){
        $questionObj = new Question();
        $text = filter_var($question['text'], FILTER_SANITIZE_STRING);
        if(validateAttr($text)){
            $questionObj->questionText = $text;
            foreach($question['answers'] as $answer){
                $questionObj->potentialAnswers[] = filter_var($answer['text'], FILTER_SANITIZE_STRING);
                if(isset($answer['correct'])){
                    $questionObj->correctAnswers[] = filter_var($answer['text'], FILTER_SANITIZE_STRING);
                }
            }
            $questionPool->questions[] = $questionObj;
        }
    }
    
    $dbContext['QuestionPools']->addOrEdit($questionPool);
    $dbContext['QuestionPools']->save();
    if(isset($_GET['createCourse'])){
        $locString = "Location: createCourse.php?" . http_build_query(array('questionPool' => $questionPool->guid));
        header($locString);
        exit();
    }
    header("Location: questionPools.php");
    exit();
}
?>
<section id='main'>
    <h2>Create Question Pool</h2>
    <form method='post'>
    <p>
        <label for='name'>Name</label><br>
        <input type='text' name='name' id='name' required>
    </p>
    <div class='question'>
        <p>
            <label for='question-0-question'>Question Text</label><br>
            <input class="question-text" type='text' name='questions[0][text]' id='question-0-question' required>
        </p>
        <table class='datatable question-table' id='question-0'>
            <thead>
                <tr>
                    <th>Correct Answer</th>
                    <th class='wide'>Answer Text</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class='center'><input type='checkbox' name='questions[0][answers][0][correct]'></td>
                    <td><input type='text' name='questions[0][answers][0][text]' required></td>
                </tr>
                <tr>
                    <td class='center'><input type='checkbox' name='questions[0][answers][1][correct]'></td>
                    <td><input type='text' name='questions[0][answers][1][text]' required></td>
                </tr>
                <tr>
                    <td class='center'><input type='checkbox' name='questions[0][answers][2][correct]'></td>
                    <td><input type='text' name='questions[0][answers][2][text]' required></td>
                </tr>
                <tr>
                    <td class='center'><input type='checkbox' name='questions[0][answers][3][correct]'></td>
                    <td><input type='text' name='questions[0][answers][3][text]' required></td>
                </tr>
            </tbody>
        </table>
        <button class='addNewAnswer' data-question='0'><img src='../images/add.png'> Add New Answer</button>
        <button class='removeLastAnswer' data-question='0'><img src='../images/remove.png'> Remove Last Answer</button>
    </div>
    <button class='addNewQuestion' data-questionCounter="1"><img src='../images/add.png'> Add New Question</button>
    <button class='removeLastQuestion' disabled="disabled"><img src='../images/remove.png'> Remove Last Question</button>
    <input type='submit' value='Save'>
    </form>
</section>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?=Config::$siteRoot?>\/scripts\/lib\/jquery.min.js"><\/script>')</script>
<!-- Scripts at the bottom for speed -->
<script src="<?=Config::$siteRoot?>/scripts/lib/jquery-ui-1.10.3.custom.min"></script>
<script src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
<script src="scripts/main.js"></script>
</body>
</html>