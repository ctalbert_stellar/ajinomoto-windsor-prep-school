<?php
if(!isset($_GET['courseHistory'])){
    die();
}

include '../init.php';

include 'loginCheck.php';
$title = "Ajinomoto Windsor Prep School Admin";
$pageName = 'Course History';
include 'header.php';

$courseHistory = array_values($dbContext['CourseHistories']->find(urldecode($_GET['courseHistory'])))[0];
$course = array_values($dbContext['Courses']->find($courseHistory->courseGUID))[0];
$person = array_values($dbContext['Persons']->find($courseHistory->person))[0];
?>
<section id="main">
    <div class="courseStatistics">
        <table>
            <tr>
                <td>Person</td>
                <td><?=$person->firstName?> <?=$person->lastName?></td>
            </tr>
            <tr>
                <td>Course</td>
                <td><?=$course->title?></td>
            </tr>
            <tr>
                <td>Date/Time</td>
                <td><?=$courseHistory->timestamp?></td>
            </tr>
            <tr>
                <td>Score</td>
                <td><?=$courseHistory->grade?></td>
            </tr>
        </table>
    </div>
    <?php foreach($courseHistory->questions as $questionHistory):?>
    <div class="questionStatistics">
        <table>
            <tr>
                <td>Question Text</td>
                <td><?=$questionHistory->questionText?></td>
            </tr>
            <tr>
                <td>Correctness</td>
                <td><?=($questionHistory->correct == 1?"Correct":"Wrong")?></td>
            </tr>
        </table>
        <table>
            <tr>
                <td>Answers</td>
                <td>User</td>
                <td>Correct</td>
            </tr>
            <?php foreach($questionHistory->potentialAnswers as $pAnswer):?>
            <tr>
                <td><?=$pAnswer?></td>
                <td><?=(is_array($questionHistory->answers)?(in_array($pAnswer, $questionHistory->answers)?"X":""):($pAnswer === $questionHistory->answers?"X":""))?></td>
                <td><?=(in_array($pAnswer, $questionHistory->correctAnswers)?"X":"")?></td>
            </tr>
            <?php endforeach;?>
        </table>
    </div>
    <?php endforeach;?>
</section>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?=Config::$siteRoot?>\/scripts\/lib\/jquery.min.js"><\/script>')</script>
<!-- Scripts at the bottom for speed -->
<script src="<?=Config::$siteRoot?>/scripts/lib/jquery-ui-1.10.4.min.js"></script>
<script src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
<script src="scripts/main.js"></script>
</body>
</html>