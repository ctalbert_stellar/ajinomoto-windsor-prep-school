<?php
if(!isset($_GET['company'])){
    die();
}

include '../init.php';

include 'loginCheck.php';
$title = "Ajinomoto Windsor Prep School Admin";
$pageName = 'wonadmin';
include 'header.php';

$company = array_values($dbContext['Companies']->find(urldecode($_GET['company'])))[0];
$persons = $dbContext['Persons']->find(array('company' => $company->guid));
$courses = $dbContext['Courses']->getAll();

$sumOfCourses = 0;
$sumOfScores = 0;
$sumOfCh = 0;
foreach($persons as $person){
    foreach($person->courseHistory as $ch){
        if($ch->grade > 70){
            $sumOfCourses++;
        }
        $sumOfScores += $ch->grade;
        $sumOfCh++;
    }
}
$avgOfCourses = $sumOfCourses/count($persons);
if($sumOfCh == 0){
    $avgOfScores = 0;
}else{
    $avgOfScores = ($sumOfScores/$sumOfCh)/count($persons);
}
?>

<section id="main">
    <div class="companyStatistics">
        <table>
            <tr>
                <td>Company Name</td>
                <td><?=$company->name?></td>
            </tr>
            <tr>
                <td>Number of Users</td>
                <td><?=count($persons)?></td>
            </tr>
            <tr>
                <td>Average Courses Passed</td>
                <td><?=$avgOfCourses?> out of <?=count($courses)?></td>
            </tr>
            <tr>
                <td>Average Score</td>
                <td><?=$avgOfScores?></td>
            </tr>
        </table>
    </div>
    <table class="datatable">
        <thead>
            <tr>
                <th>User</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($persons as $person):?>
            <tr>
                <td><?=$person->firstName?> <?=$person->lastName?></td>
                <td><a href="personCourseHistory.php?<?=http_build_query(array('person' => $person->guid))?>">User Statistics</a></td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</section>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?=Config::$siteRoot?>\/scripts\/lib\/jquery.min.js"><\/script>')</script>
<!-- Scripts at the bottom for speed -->
<script src="<?=Config::$siteRoot?>/scripts/lib/jquery-ui-1.10.4.min.js"></script>
<script src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
<script src="scripts/main.js"></script>

</body>
</html>