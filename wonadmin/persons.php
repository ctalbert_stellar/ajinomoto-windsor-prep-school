<?php
include '../init.php';

include 'loginCheck.php';

$title = "Ajinomoto Windsor Prep School Admin";
$pageName = "wonadmin";
include 'header.php';

$users = $dbContext['Users']->getAll();
?>
<section id="main">
    <h2>Persons</h2>
    <table class="datatable">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Broker Number</th>
                <th>Company</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($users as $user):?>
            <tr>
                <td><?=$user->person->firstName?> <?=$user->person->lastName?></td>
                <td><?=$user->email?></td>
                <td><?=(isset($user->person->brokerNumber) && !empty($user->person->brokerNumber))?$user->person->brokerNumber:'N/A'?></td>
                <td><?=$user->person->company->name?></td>
                <td>
                    <a href="editperson.php?<?=http_build_query(array('person' => $user->person->guid))?>">Edit</a> |
                    <a href="edituser.php?<?=http_build_query(array('user' => $user->guid))?>">Change Password</a> | 
                    <?php if($user->isApproved):?>
                        <a href="unapproveuser.php?<?=http_build_query(array('user' => $user->guid))?>">Unapprove</a> | 
                    <?php else:?>
                        <a href="approveuser.php?<?=http_build_query(array('user' => $user->guid))?>">Approve</a> | 
                    <?php endif;?>
                    <a href="personCourseHistory.php?<?=http_build_query(array('person' => $user->person->guid))?>">Course History</a>
                </td>
            </tr>
        <?php endforeach;?>
        </tbody>
    </table>
</section>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?=Config::$siteRoot?>\/scripts\/lib\/jquery.min.js"><\/script>')</script>
<!-- Scripts at the bottom for speed -->
<script src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
<script src="scripts/main.js"></script>

</body>
</html>