<?php
if(!isset($_GET['course'])){
    die();
}

include '../init.php';

include 'loginCheck.php';
$title = "Ajinomoto Windsor Prep School Admin";
$pageName = 'wonadmin';
include 'header.php';

$course = array_values($dbContext['Courses']->find(urldecode($_GET['course'])))[0];

$courseHistories = $dbContext['CourseHistories']->find(array('course'=>$course->guid));

$numOfPass = 0;
$numOfFail = 0;
$questions = array();
foreach($courseHistories as $ch){
    foreach($ch->questions as $qh){
        $questions[$qh->question] = array_values($dbContext['Questions']->find($qh->question))[0];
    }
    if($ch->grade >= 70){
        $numOfPass++;
    }else{
        $numOfFail++;
    }
}

$passFailRatio = null;
if(count($courseHistories) > 0 && $numOfFail > 0){
    $passFailRatio = $numOfPass/$numOfFail;
}
?>
<section id="main">
    <div class="courseStatistics">
        <table>
            <tr>
                <td>Course Title</td>
                <td><?=$course->title?></td>
            </tr>
            <tr>
                <td>Number of attempts</td>
                <td><?=count($courseHistories)?></td>
            </tr>
            <tr>
                <td>Number of passes</td>
                <td><?=$numOfPass?></td>
            </tr>
            <tr>
                <td>Number of fails</td>
                <td><?=$numOfFail?></td>
            </tr>
            <tr>
                <td>Pass/Fail Ratio</td>
                <td><?=is_null($passFailRatio)?"All attempts have passed":$passFailRatio?>*</td>
            </tr>
        </table>
        <aside>*Larger number is better</aside>
    </div>
    <table class="datatable">
        <thead>
            <tr>
                <th>Question Text</th>
                <th>Correctness Ratio</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php 
            foreach($questions as $question):
            $relatedQuestionHistories = $dbContext['QuestionHistories']->find(array('question' => $question->guid));
            $numCorrect = 0;
            $numIncorrect = 0;
            foreach($relatedQuestionHistories as $qh):
                if($qh->correct):
                    $numCorrect++;
                else:
                    $numIncorrect++;
                endif;
            endforeach;
                ?>
            <tr>
                <td><?=$question->questionText?></td>
                <td><?=($numIncorrect == 0)?"All Correct":($numCorrect/$numIncorrect)?>*</td>
                <td><a href="questionHistoryDetails.php?<?=http_build_query(array('question' => $question->guid))?>">Details</a></td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</section>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?=Config::$siteRoot?>\/scripts\/lib\/jquery.min.js"><\/script>')</script>
<!-- Scripts at the bottom for speed -->
<script src="<?=Config::$siteRoot?>/scripts/lib/jquery-ui-1.10.4.min.js"></script>
<script src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
<script src="scripts/main.js"></script>

</body>
</html>