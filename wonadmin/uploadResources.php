<?php
include '../init.php';

include 'loginCheck.php';

$courses = $dbContext['Courses']->getAll();
$categories = $dbContext['Categories']->getAll();

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    // crops proportionally based on width and height
    function generatePreview($file, $w, $h, $mimeType) {
        if(strtolower(substr($mimeType, 0, 5)) === 'video'){
            $ffmpeg = '/usr/bin/ffmpeg'; //put the relative path to the ffmpeg.exe file
            $second = 5; //specify the time to get the screen shot at (can easily be randomly generated)
            $video = "/opt/www/windsorwonsource.com/public/resources/$file";
            $image = "/opt/www/windsorwonsource.com/public/resources/previews/$file.jpg"; //define the output file
            //finally assemble the command and execute it
            $command = "$ffmpeg  -itsoffset -$second  -i $video -vcodec mjpeg -vframes 1 -an -f rawvideo -s {$w}×{$h} $image";
            $output = exec($command);
        }else{
            $image = "/opt/www/windsorwonsource.com/public/resources/$file";
            //Resize original image
            try{
            $im = new Imagick();
            $im->readimage($image);
            $height = $im->getimageheight();
            $width = $im->getimagewidth();
            
            if(($width/$height) > (4/3)){
                //image is too wide
                $properWidth = ($height * (4/3));
                $im->resizeimage($properWidth, $height, imagick::FILTER_LANCZOS, 1);
            }elseif(($width/$height) < (4/3)){
                //image is too tall
                $properHeight = ($width / (4/3));
                $im->resizeimage($width, $properHeight, imagick::FILTER_LANCZOS, 1);
            }else{
                //already at proper aspect ratio
            }
            
            if($width > 2048){
                $im->resizeimage(2048, 1536, imagick::FILTER_LANCZOS, 1);
            }
            $im->writeimagefile(fopen("/opt/www/windsorwonsource.com/public/resources/$file", 'wb'));
            $im->destroy();
            }  catch (Exception $ex){
                die($ex->getMessage());
            }
            
            
            //Create preview            
            $ima = new Imagick();
            $ima->pingimage($image);
            $ima->readimage($image);
            $ima->thumbnailimage($w, $h);
            $ima->writeimagefile(fopen("/opt/www/windsorwonsource.com/public/resources/previews/$file", 'wb'));
            $ima->destroy();
            
            
            return "/opt/www/windsorwonsource.com/public/resources/previews/$file";
        }
    }
    
    foreach($_POST['filenames'] as $file){
        generatePreview($file, 1024, 768, $_POST['types'][$file]);
        $resource = new Resource();
        $resource->location = Config::$siteRoot . '/resources/' .$file;
        foreach($_POST['categories'][$file] as $category){
            $resource->categories[] = array_values($dbContext['Categories']->find($category))[0];
        }
        $resource->unlockedBy = $_POST['course'][$file];
        $resource->mimeType = $_POST['types'][$file];
        if(substr($resource->mimeType, 0, 5) === 'video'){
            $resource->type = 'Video';
            $resource->preview = Config::$siteRoot .'/resources/previews/' . $file . '.jpg';
        }elseif(substr($resource->mimeType, 0, 5) === 'image'){
            $resource->type = 'Image';
            $resource->preview = Config::$siteRoot .'/resources/previews/' . $file;
        }else{
            throw new InvalidArgumentException;
        }
        $dbContext['Resources']->addOrEdit($resource);
        $dbContext['Resources']->save();
        
        foreach($dbContext['Persons']->getAll() as $person){
            if(count($person->courseHistory) > 0){
                foreach($person->courseHistory as $ch){
                    if($ch->grade >= 70 && ($resource->unlockedBy === $ch->courseGUID)){
                        $person->resources[$resource->guid] = $resource;
                        $dbContext['Persons']->addOrEdit($person);
                        $dbContext['Persons']->save();
                    }
                }
            }
        }
    }
    
    header("Location: resources.php");
    exit();
}
$title = "Ajinomoto Windsor Prep School Admin";
$pageName = 'wonadmin';
$styles[] = "/scripts/lib/plupload/jquery.ui.plupload/css/jquery.ui.plupload.css";
include 'header.php';

?>
<section id='main'>
    <h2>Upload New Resources</h2>
    <p class="warning">Please note that any slides that are not 4:3 will be resized accordingly.  Also, any images over 2048px wide or 1536px tall (iPad Retina display dimensions) will be scaled down.</p>
    <div id='uploader'>
        <p>Your browser doesn't have Flash, Silverlight or HTML5 support.</p>
    </div>
    <form method="post" id="resources">
        
    </form>
</section>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?=Config::$siteRoot?>\/scripts\/lib\/jquery.min.js"><\/script>')</script>
<!-- Scripts at the bottom for speed -->
<script src="<?=Config::$siteRoot?>/scripts/lib/jquery-ui-1.10.4.min.js"></script>
<script src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
<script src="scripts/lib/plupload/plupload.full.min.js"></script>
<script type="text/javascript" src="scripts/lib/plupload/jquery.ui.plupload/jquery.ui.plupload.min.js"></script>
<script type="text/javascript">
    var categories = [];
    var courses = [];
<?php foreach($courses as $course):?>
    courses.push(['<?=$course->guid?>', '<?=addslashes($course->title)?>']);
<?php endforeach;?>
<?php foreach($categories as $category):?>
    categories.push(['<?=$category->guid?>', '<?=addslashes($category->name)?>']);
<?php endforeach;?>
</script>
<script src="scripts/main.js"></script>
</body>
</html>