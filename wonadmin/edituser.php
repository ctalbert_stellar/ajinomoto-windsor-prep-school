<?php

include '../init.php';

include 'loginCheck.php';

$title = "Ajinomoto Windsor Prep School Admin";
$pageName = "edituser";
include 'header.php';

$dbuser = array_values($dbContext['Users']->find(urldecode($_GET['user'])))[0];
$errors = array();

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
    if(is_null($email) || empty($email)){
        //Do nothing.  We want the email to stay the same.
    }elseif(!filter_var($dbuser->email, FILTER_VALIDATE_EMAIL)){
        $errors['email'][] = "Invalid email address.";
    }
    
    $password = filter_input(INPUT_POST, 'password', FILTER_UNSAFE_RAW);
    $passwordConfirmation = filter_input(INPUT_POST, 'password2', FILTER_UNSAFE_RAW);
    if(empty($password) || is_null($password)){
        //Do nothing, we want the password to stay the same
    }elseif($password !== $passwordConfirmation){
        $errors['passwordConfirmation'][] = "Passwords do not match.";
    }else{
        $dbuser->createPassword($password);
    }
    
    if(isset($_POST['approved'])){
        $dbuser->isApproved = TRUE;
    }else{
        $dbuser->isApproved = FALSE;
    }
    
    if(isset($_POST['admin'])){
        $dbuser->isAdmin = TRUE;
    }else{
        $dbuser->isAdmin = FALSE;
    }
    
    $personGUID = filter_input(INPUT_POST, 'person-guid', FILTER_SANITIZE_STRING);
    if(empty($personGUID) || is_null($personGUID)){
        $errors['personGUID'][] = "Fatal error, user is invalid.  Try again later.  If you continue to receive this error, please contact support.";
    }else{
        $dbuser->person = array_values($dbContext['Persons']->find($personGUID))[0];
    }
    
    if(count($errors) === 0){
        $dbContext['Users']->addOrEdit($dbuser);
        $dbContext['Users']->save();
        header('Location:persons.php');
        exit();
    }
}
?>
<section id="main">
    <h2>Edit User</h2>
    <?php if(count($errors) > 0):?>
    <div class="message error">
        <h3>Unfortunately, there were some errors in your submission.</h3>
        <ul>
        <?php foreach($errors as $key => $error):?>
            <?php foreach($error as $e):?>
            <li><?=$e?></li>
            <?php endforeach;?>
        <?php endforeach;?>
        </ul>
    </div>
    <?php endif;?>
    <form method="post">
        <p>
            <label for="email">Email</label><br>
            <input type="email" id="email" name="email" value="<?=$dbuser->email?>" autocomplete="off">
        </p>
        <p>
            <label for="password">Password</label><br>
            <input type="password" id="password" name="password"><br>
            <label for="password2">Password Confirmation</label><br>
            <input type="password" id="password2" name="password2">
        </p>
        <p>
            <label for="approved">Approved</label>
            <input type="checkbox" id="approved" name="approved" <?=($dbuser->isApproved?"checked":"")?> autocomplete="off">
            <label for="admin">Admin</label>
            <input type="checkbox" id="admin" name="admin" <?=($dbuser->isAdmin?"checked":"")?> autocomplete="off">
        </p>
        <p>
            <input type="hidden" name="person-guid" value="<?=$dbuser->person->guid?>">
            <input type="submit" value="Save">
        </p>
    </form>
    <p>Did you mean to edit the person associated with this user? <a href="editperson.php?<?=http_build_query(array('person'=>$dbuser->person->guid))?>">Go to edit person instead</a>.
</section>