<?php
include '../init.php';

include 'loginCheck.php';

$person = array_values($dbContext['Persons']->find(urldecode($_GET['person'])))[0];
$companies = $dbContext['Companies']->getAll();

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    
    
    /**
     * Takes the name of both the attribute in the class and the post field as 
     * $name, the name of the object the attribute belongs to (in this case, the
     * default is the $person object) and the filter you want to use (most all 
     * filters will need string sanitization), then sanitizes the input from post
     * and inserts it into the object if it was able to sanitize.
     * 
     * @param string $name
     * @param string $object
     * @param enum $filter
     */
    function setAttr($name, $object = 'person', $filter = FILTER_SANITIZE_STRING){
        global ${$object};
        $attr = filter_input(INPUT_POST, $name, $filter);
        if(isset($attr) && !empty($attr) && $attr !== FALSE){
            //I actually found a use for variable variables            
            ${$object}->$name = $attr;
        }
    }
    
    function validateAttr(&$attr){
        return (isset($attr) && !empty($attr) && $attr !== FALSE);
    }
    
    
    setAttr('firstName');
    setAttr('lastName');
    setAttr('phone');
    setAttr('dsm');
    setAttr('brokerNumber');
    $addresses = $_POST['address'];
    foreach($addresses as $guid => $address){
        $_guid = filter_var($guid, FILTER_SANITIZE_STRING);
        if(isset($_guid) && !empty($_guid) && $_guid !== FALSE){
            $addressObject = new Address($_guid);
            foreach($address as $key => $field){
                $$key = filter_var($address[$key], FILTER_SANITIZE_STRING);
                if(isset($$key) && !empty($$key) && $$key !== FALSE){
                    $addressObject->$key = $$key;
                }
            }
            $person->addresses[$_guid] = $addressObject;
        }
    }
    
    $company = filter_input(INPUT_POST, 'company', FILTER_SANITIZE_STRING);
    if(validateAttr($company) && $company !== 'create'){
        $person->company = $companies[$company];
    }
    
    $companyName = filter_input(INPUT_POST, 'companyName', FILTER_SANITIZE_STRING);
    $companyAddressLineOne = filter_input(INPUT_POST, 'companyAddressLineOne', FILTER_SANITIZE_STRING);
    $companyAddressLineTwo = filter_input(INPUT_POST, 'companyAddressLineTwo', FILTER_SANITIZE_STRING);
    $companyAddressCity = filter_input(INPUT_POST, 'companyAddressCity', FILTER_SANITIZE_STRING);
    $companyAddressState = filter_input(INPUT_POST, 'companyAddressState', FILTER_SANITIZE_STRING);
    $companyAddressZip = filter_input(INPUT_POST, 'companyAddressZip', FILTER_SANITIZE_STRING);
    
    if(validateAttr($companyName)
            && validateAttr($companyAddressLineOne)
            && isset($companyAddressLineTwo)
            && validateAttr($companyAddressCity)
            && validateAttr($companyAddressState)
            && validateAttr($companyAddressZip)){
        $newCompany = new Company(null,
                $companyName,
                array(new Address(
                        null,
                        $companyAddressLineOne,
                        (empty($companyAddressLineTwo)?null:$companyAddressLineTwo),
                        $companyAddressCity,
                        $companyAddressState,
                        $companyAddressZip)));
        $person->company = $newCompany;
    }
    
    $dbContext['Persons']->addOrEdit($person);
    $dbContext['Persons']->save();
    header("Location: persons.php");
    exit();
}

$title = "Ajinomoto Windsor Prep School Admin";
$pageName = "editperson";
include 'header.php';

?>
<section id='main'>
<h2>Edit Person</h2>
<form method="post">
    <p>
        <label for="firstName">First Name</label><br>
        <input type="text" name="firstName" id="firstName" value="<?=$person->firstName?>">
    </p>
    <p>
        <label for="lastName">Last Name</label><br>
        <input type="text" name="lastName" id="lastName" value="<?=$person->lastName?>">
    </p>
    <p>
        <label for="phone">Phone</label><br>
        <input type="text" name="phone" id="phone" value="<?=$person->phone?>">
    </p>
    <p>
        <label for="dsm">District Sales Manager</label><br>
        <input type="text" name="dsm" id="dsm" value="<?=$person->dsm?>">
    </p>
    <p>
        <label for="brokerNumber">Broker Number</label><br>
        <input type="text" name="brokerNumber" id="brokerNumber" value="<?=$person->brokerNumber?>">
    </p>
    <h3>Addresses</h3>
    <table class="datatable" id="personAddresses">
        <thead>
            <tr>
                <th>Line One</th>
                <th>Line Two</th>
                <th>City</th>
                <th>State</th>
                <th>Zip</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($person->addresses as $address):?>
            <tr>
                <td><input type="text" name="address[<?=$address->guid?>][lineOne]" value='<?=$address->lineOne?>' placeholder="Line One"></td>
                <td><input type="text" name="address[<?=$address->guid?>][lineTwo]" value='<?=$address->lineTwo?>' placeholder="Line Two"></td>
                <td><input type="text" name="address[<?=$address->guid?>][city]" value='<?=$address->city?>' placeholder="City"></td>
                <td><input type="text" name="address[<?=$address->guid?>][state]" value='<?=$address->state?>' placeholder="State"></td>
                <td><input type="text" name="address[<?=$address->guid?>][zip]" value='<?=$address->zip?>' placeholder="Zip"></td>
            </tr>
        <?php endforeach;?>
        </tbody>
    </table>
    <button id="addAddress"><img src="../images/add.png">Add New Address</button>
    <h3>Company</h3>
    <p>
        <select name='company' id="companySelect">
            <?php foreach($companies as $company):?>
            <option value='<?=$company->guid?>' <?=($company->guid === $person->company->guid?"selected='selected'":"")?>><?=$company->name?></option>
            <?php endforeach;?>
            <option value='create'>Create New</option>
        </select>
    </p>
    <div id='newCompany' class='hidden'>
        <h3>Add Company</h3>
        <p>
            <label for='companyName'>Company Name</label><br>
            <input type='text' name='companyName' id='companyName'>
        </p>
        <p>
            <label for='companyAddressLineOne'>Address Line One</label><br>
            <input type='text' name='companyAddressLineOne' id='companyAddressLineOne'>
        </p>
        <p>
            <label for='companyAddressLineTwo'>Address Line Two</label><br>
            <input type='text' name='companyAddressLineTwo' id='companyAddressLineTwo'>
        </p>
        <p>
            <label for='companyCity'>City</label><br>
            <input type='text' name='companyAddressCity' id='companyCity'>
        </p>
        <p>
            <label for='companyState'>State</label><br>
            <input type='text' name='companyAddressState' id='companyState'>
        </p>
        <p>
            <label for='companyZip'>Zip</label><br>
            <input type='text' name='companyAddressZip' id='companyZip'>
        </p>
    </div>
    <a href='persons.php'><button onclick='window.location = "persons.php"; return false;'>Cancel</button></a>
    <input type="submit" value="Save">
</form>
</section>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?=Config::$siteRoot?>\/scripts\/lib\/jquery.min.js"><\/script>')</script>
<!-- Scripts at the bottom for speed -->
<script src="<?=Config::$siteRoot?>/scripts/lib/jquery-ui-1.10.3.custom.min"></script>
<script src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
<script src="scripts/main.js"></script>

</body>
</html>