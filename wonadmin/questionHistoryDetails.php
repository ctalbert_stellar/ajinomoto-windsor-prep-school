<?php
if(!isset($_GET['question'])){
    die();
}

include '../init.php';
include 'loginCheck.php';
$title = "Ajinomoto Windsor Prep School Admin";
$pageName = 'wonadmin';
include 'header.php';

$question = array_values($dbContext['Questions']->find($_GET['question']))[0];
$questionHistories = $dbContext['QuestionHistories']->find(array('question' => $question->guid));

$numCorrect = 0;
$numIncorrect = 0;
$potentialAnswerFrequency = array();
foreach($question->potentialAnswers as $pa){
    $potentialAnswerFrequency[$pa] = 0;
}
foreach($questionHistories as $qh){
    if(is_array($qh->answers)){
        foreach($qh->answers as $a){
            $potentialAnswerFrequency[$a]++;
        }
    }else{
        $potentialAnswerFrequency[$qh->answers]++;
    }
    if($qh->correct){
        $numCorrect++;
    }else{
        $numIncorrect++;
    }
}
?>
<section id="main">
    <div class="questionStatistics">
        <table>
            <tr>
                <td>Question Text</td>
                <td><?=$question->questionText?></td>
            </tr>
            <tr>
                <td>Number of Times Correct</td>
                <td><?=$numCorrect?></td>
            </tr>
            <tr>
                <td>Number of Times Incorrect</td>
                <td><?=$numIncorrect?></td>
            </tr>
            <tr>
                <td>Correctness Ratio</td>
                <td><?=($numIncorrect == 0)?"All Correct":$numCorrect/$numIncorrect . "*"?></td>
            </tr>
        </table>
        <aside>* Larger numbers are better</aside>
        
        <p>Answer Frequency</p>
        <table>
            <?php foreach($potentialAnswerFrequency as $answer => $frequency):?>
            <tr>
                <td><?=is_array($question->correctAnswers)?(in_array($answer, $question->correctAnswers)?"**$answer":"$answer"):($answer == $question->correctAnswers?"**$answer":"$answer")?></td>
                <td><?=$frequency?></td>
            </tr>
            <?php endforeach;?>
            <tr>
                <td>Total</td>
                <td><?=array_sum($potentialAnswerFrequency)?></td>
            </tr>
        </table>
        <aside>** Correct Answer</aside>
    </div>
</section>

