<?php

include '../init.php';

include 'loginCheck.php';

$title = "Ajinomoto Windsor Prep School Admin";
$pageName = "createcourse";
include 'header.php';

$course = array_values($dbContext['Courses']->find(urldecode($_GET['course'])))[0];
$dbcategories = $dbContext['Categories']->getAll();

$errors = array();

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    function validateAttr($attr){
        return (isset($attr) && !empty($attr) && $attr !== FALSE);
    }
    $course->title = filter_input(INPUT_POST, 'courseTitle', FILTER_SANITIZE_STRING);
    if(!validateAttr($course->title)){
        $errors[] = "You must enter a valid course title";
    }
    
    $videoURL = filter_input(INPUT_POST, 'video', FILTER_SANITIZE_URL);
    $course->videos = new Video(null, $videoURL);
    if(!validateAttr($videoURL)){
        $errors[] = "You must enter a valid link to the course video on YouTube";
    }
    
    $course->description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
    if(!validateAttr($course->description)){
        $errors[] = "You must enter a valid course description";
    }
    
    if(isset($_POST['categories'])){
        $categories = $_POST['categories'];
        $catGUIDs = array();
        foreach($categories as $category){
            if(isset($category['checked'])){
                if($category['checked'] == 'newCategory'){
                    if(isset($category['name'])){
                        $course->categories[] = new Category(null, $category['name']);
                    }
                }else if(!isset($course->categories[$category['checked']])){
                    $catGUIDs[] = $category['checked'];
                    $course->categories[$category['checked']] = array_values($dbContext['Categories']->find($category['checked']))[0];
                }else{
                    $catGUIDs[] = $category['checked'];
                }
            }
        }
        
        $removalArray = array();
        foreach($course->categories as $cat){
            if(!in_array($cat->guid, $catGUIDs)){
                $removalArray[] = $cat->guid;
            }
        }
        
        foreach($removalArray as $remove){
            unset($course->categories[$remove]);
        }
    }else{
        $errors[] = "You must select at least one category";
    }
    
    $course->numberOfQuestions = filter_input(INPUT_POST, 'numberOfQuestions', FILTER_SANITIZE_NUMBER_INT);
    if(!validateAttr($course->numberOfQuestions)){
        $errors[] = "You must enter the number of questions you want to use for this question pool";
    }
    
    if(count($errors) <= 0){
        $dbContext['Courses']->addOrEdit($course);
        $dbContext['Courses']->save();
        header('Location: courses.php');
    }
    
}
?>
<section id="main">
    <h2>Create New Course</h2>
    <?php if(count($errors) > 0):?>
    <div class='error'>
        <h2>Errors</h2>
        <ul>
            <?php foreach($errors as $error):?>
            <li><?=$error?></li>
            <?php endforeach;?>
        </ul>
    </div>
    <?php endif;?>
    <form method='post'>
        <p>
            <label for='courseTitle'>Title</label><br>
            <input type='text' name='courseTitle' id='courseTitle' value='<?=(isset($course->title)?$course->title:"")?>'>        
        </p>
        <p>
            <label for='video'>Video Link</label><br>
            <input type='text' name='video' id='video' value='<?=(isset($course->videos->location)?$course->videos->location:"")?>'>
        </p>
        <p>
            <label for='description'>Description</label>
            <textarea name='description' id='description'><?=$course->description?></textarea>
        </p>
        <div class='column left'>
            <h3>Categories</h3>
            <table class='datatable' id='categories'>
                <thead>
                    <tr>
                        <th></th>
                        <th>Category</th>
                    </tr>
                </thead>
                <tbody>
                <?php if(count($dbcategories) > 0):?>
                    <?php foreach($dbcategories as $category):?>
                        <tr>
                            <td><input type='checkbox' name='categories[<?=$category->guid?>][checked]' value='<?=$category->guid?>' <?=(isset($course->categories)?(in_array($category, $course->categories)?"checked":""):"")?>></td>
                            <td><?=$category->name?></td>
                        </tr>
                    <?php endforeach;?>
                <?php else:?>
                        <tr>
                            <td><input type='checkbox' name='categories[0][checked]' value='newCategory'></td>
                            <td><input type='textbox' name='categories[0][name]' placeholder='Category Name'></td>
                        </tr>
                <?php endif;?>
                </tbody>
            </table>
            <button class='addCategory'><img src='../images/add.png'> Create New Category</button>
        </div>
        <div class='column right'>
            <h3>Question Pools</h3>
            <p>Current Question Pool: <?=$course->questionPool->name?></p>
            <p>Due to the statistical tracking in place, editing the question pool associated with a course is not possible. If you need to change the question pool associated with a course, you will need to deactivate this course and create a new one.</p>
            <p>
                <label for='numOfQuestions'>Number of Questions from Pool</label>
                <input type='text' name='numberOfQuestions' id='numOfQuestions' value='<?=$course->numberOfQuestions?>'>
            </p>
        </div>
        <div class='clearfix'>
            <input type='submit' value='Save'>
        </div>
    </form>
</section>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?=Config::$siteRoot?>\/scripts\/lib\/jquery.min.js"><\/script>')</script>
<!-- Scripts at the bottom for speed -->
<script src="<?=Config::$siteRoot?>/scripts/lib/jquery-ui-1.10.3.custom.min"></script>
<script src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
<script src="scripts/main.js"></script>
</body>
</html>

