<?php

include '../init.php';
require '../includes/phpmailer/PHPMailerAutoload.php';

$dbuser = array_values($dbContext['Users']->find(urldecode($_GET['user'])))[0];

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    $dbuser->isApproved = TRUE;
    $dbContext['Users']->addOrEdit($dbuser);
    $dbContext['Users']->save();
    $mail = new PHPMailer;

    $mail->isSMTP();
    $mail->Host = 'mail.ajinomotowindsorprepschool.com';
    $mail->SMTPAuth = true;
    $mail->Username = 'info@ajinomotowindsorprepschool.com';
    $mail->Password = 'letM3know';
    $mail->SMTPSecure = 'tls';
    $mail->Port = 587;

    $mail->From = 'info@ajinomotowindsorprepschool.com';
    $mail->FromName = 'Ajinomoto Windsor Prep School';

    $mail->addAddress($dbuser->email);

    $mail->Subject = "You've been approved for Ajinomoto Windsor Prep School";
    $mail->Body = "Our administrators have reviewed your information and approved your account for use with Ajinomoto Windsor Prep School. Visit http://ajinomotowindsorprepschool.com today to get started!";

    $mail->send();

    header('Location: persons.php');
    exit();
}

include 'loginCheck.php';

$title = "Prep School Admin";
$pageName = "approveuser";
include 'header.php';
?>
<section id="main">
    <h2>Approve User</h2>
    <p>Are you sure you want to approve the following user?</p>
    <div class="info-block">
        <p><strong>Name:</strong> <?=$dbuser->person->firstName?> <?=$dbuser->person->lastName?></p>
        <p><strong>Email:</strong> <?=$dbuser->email?></p>
        <p><strong>Phone:</strong> <?=$dbuser->person->phone?></p>
        <p><strong>District Sales Manager:</strong> <?=(isset($dbuser->person->dsm) && !empty($dbuser->person->dsm))?$dbuser->person->dsm: 'N/A'?></p>
        <p><strong>Broker Number:</strong> <?=(isset($dbuser->person->brokerNumber) && !empty($dbuser->person->brokerNumber))?$dbuser->person->brokerNumber: 'N/A'?></p>
        <p><strong>Address:</strong><br>
            <?=array_values($dbuser->person->addresses)[0]->lineOne?><br>
            <?=(isset(array_values($dbuser->person->addresses)[0]->lineTwo) && !empty(array_values($dbuser->person->addresses)[0]->lineTwo))?array_values($dbuser->person->addresses)[0]->lineTwo.'<br>':''?>
            <?=array_values($dbuser->person->addresses)[0]->city?>,
            <?=array_values($dbuser->person->addresses)[0]->state?>
            <?=array_values($dbuser->person->addresses)[0]->zip?></p>
    </div>
    <h3>Company Information</h3>
    <div class="info-block">
        <p><strong>Name:</strong> <?=$dbuser->person->company->name?></p>
        <p><strong>Address:</strong><br>
            <?=array_values($dbuser->person->company->addresses)[0]->lineOne?><br>
            <?=(isset(array_values($dbuser->person->company->addresses)[0]->lineTwo) && !empty(array_values($dbuser->person->company->addresses)[0]->lineTwo))?array_values($dbuser->person->addresses)[0]->lineTwo.'<br>':''?>
            <?=array_values($dbuser->person->company->addresses)[0]->city?>,
            <?=array_values($dbuser->person->company->addresses)[0]->state?>
            <?=array_values($dbuser->person->company->addresses)[0]->zip?></p>
    </div>
    <div class="info-block">
    <form method="post">
        <input type="submit" value="Approve"> <a href="persons.php" class="pad-left">Cancel</a>
    </form>
    </div>
</section>