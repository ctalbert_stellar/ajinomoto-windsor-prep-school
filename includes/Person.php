<?php
require_once 'Model.php';
class Person extends Model{
    public $firstName;
    public $lastName;
    public $company;
    public $addresses;
    public $phone;
    public $dsm;
    public $brokerNumber;
    public $resources;
    public $courseHistory;
    public $slideshows;
    
    public function __construct(
            $guid = null,
            $firstName = null,
            $lastName = null,
            $company = null,
            $addresses = null,
            $phone = null,
            $dsm = null,
            $brokerNumber = null,
            $resources = null,
            $courseHistory = null,
            $slideshows = null) {
        if(!is_null($guid)){
            $this->guid = $guid;
        }
        
        if(!is_null($firstName)){
            $this->firstName = $firstName;
        }
        
        if(!is_null($lastName)){
            $this->lastName = $lastName;
        }
        
        if(!is_null($company)){
            $this->company = $company;
        }
        
        if(!is_null($addresses)){
            $this->addresses = $addresses;
        }else{
            $this->addresses = array();
        }
        
        if(!is_null($phone)){
            $this->phone = $phone;
        }
        
        if(!is_null($dsm)){
            $this->dsm = $dsm;
        }
        
        if(!is_null($brokerNumber)){
            $this->brokerNumber = $brokerNumber;
        }
        
        if(!is_null($resources)){
            $this->resources = $resources;
        }
        
        if(!is_null($courseHistory)){
            $this->courseHistory = $courseHistory;
        }
        
        if(!is_null($slideshows)){
            $this->slideshows = $slideshows;
        }
        
        parent::__construct();
    }
}
