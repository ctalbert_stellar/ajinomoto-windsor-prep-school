<?php
require_once 'dbTable.php';
require_once 'Slideshow.php';
class TblSlideshows extends dbTable{
    public function __construct(&$db, &$dbContext) {
        parent::__construct($db, $dbContext, "Slideshows");
    }
    
    public function generateContainerFromStatement($statement){
        foreach($statement->fetchAll(PDO::FETCH_OBJ) as $row){
            $this->container[$row->guid] = new Slideshow(
                    $row->guid,
                    $row->title,
                    $this->dbContext['Slides']->find(array('slideshow' => $row->guid)),
                    new DateTime($row->updated),
                    $row->person
                    );
            usort($this->container[$row->guid]->slides, function($a, $b){
                if($a->order === $b->order){
                    return 0;
                }
                return ($a->order < $b->order) ? -1: 1;
            });
        }
        
        return $this->container;
    }
    
    public function save(){
        $insertOrUpdateQuery = "INSERT INTO {$this->table}"
            . "("
                . "`guid`,"
                . "`title`,"
                . "`updated`,"
                . "`person`"
            . ")VALUES("
                . ":guid,"
                . ":title,"
                . ":updated,"
                . ":person"
            . ")ON DUPLICATE KEY UPDATE"
                . "`title` = VALUES(`title`),"
                . "`updated` = VALUES(`updated`),"
                . "`person` = VALUES(`person`)";
        $insertOrUpdateStatement = $this->db->prepare($insertOrUpdateQuery);
        
        $deleteQuery = "DELETE FROM {$this->table} WHERE `guid` = :guid";
        $deleteStatement = $this->db->prepare($deleteQuery);
        
        foreach($this->container as $slideshow){
            if($slideshow->hasChanged){
                $insertOrUpdateStatement->execute(
                        array(
                            'guid' => $slideshow->guid,
                            'title' => $slideshow->title,
                            'updated' => null,
                            'person' => $slideshow->person)
                        );
                $this->updateSlides($slideshow);
            }
            
            if($slideshow->isDeleted){
                $deleteStatement->execute(array('guid' => $slideshow->guid));
            }
        }
    }
    
    private function updateSlides($slideshow){
        if(isset($slideshow->slides) && is_array($slideshow->slides)){
            
            $oldSlideshow = array_values($this->dbContext['Slideshows']->find($slideshow->guid))[0];
            
            foreach($oldSlideshow->slides as $slide){
                $this->dbContext['Slides']->delete($slide);
            }
            $this->dbContext['Slides']->save();
            
            foreach($slideshow->slides as $slide){
                $this->dbContext['Slides']->addOrEdit($slide);
            }
            $this->dbContext['Slides']->save();
        }
    }
}
