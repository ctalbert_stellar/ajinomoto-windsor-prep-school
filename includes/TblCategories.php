<?php
require_once 'dbTable.php';
require_once 'Category.php';
class TblCategories extends dbTable{
    public function __construct(&$db, &$dbContext) {
        parent::__construct($db, $dbContext, "Categories");
    }
    
    public function generateContainerFromStatement($statement) {
        foreach($statement->fetchAll(PDO::FETCH_OBJ) as $row){ 
            $this->container[$row->guid] = new Category(
                    $row->guid,
                    $row->name
                    );
        }
        
        return $this->container;
    }
    
    public function findByResourceGUID($resourceGUID){
        return $this->findByRelation($resourceGUID);
    }
    
    public function findByCourseGUID($courseGUID){
        return $this->findByRelation($courseGUID);
    }
    
    private function findByRelation($guid){
        $query = "SELECT * FROM `CategoryRelations` WHERE `relative` = :guid";
        $statement = $this->db->prepare($query);
        
        $statement->execute(array('guid' => $guid));
        
        if($statement->rowCount() > 0){
            $first = TRUE;
            foreach($statement->fetchAll(PDO::FETCH_OBJ) as $row){
                $this->find($row->category, $first);
                if($first){
                    $first = FALSE;
                }
            }
        }else{
            $this->container = array();
        }
        
        return $this->container;
    }
    
    public function save(){
        $insertOrUpdateQuery = "INSERT INTO {$this->table}"
            . "("
                . "`guid`,"
                . "`name`"
            . ")VALUES("
                . ":guid,"
                . ":name"
            . ")ON DUPLICATE KEY UPDATE"
                . "`name` = VALUES(`name`)";
        
        $deleteQuery = "DELETE FROM {$this->table}"
            . "WHERE `guid` = :guid";
        
        $deleteStatement = $this->db->prepare($deleteQuery);
            
        $insertOrUpdateStatement = $this->db->prepare($insertOrUpdateQuery);
        
        foreach($this->container as $category){
            if($category->hasChanged){
                $insertOrUpdateStatement->execute(array('guid'=> $category->guid, 'name' => $category->name));
                $category->hasChanged = false;
            }
            
            if($category->isDeleted){
                $deleteStatement->execute(array($deletedGUID));
            }
            
        }
    }
}
