<?php
require_once 'Resource.php';
class Slide extends Resource {
    public $slideshow;
    public $order;
    public $previewImage;
    public $resource;
    
    public function __construct(
            $guid = null,
            $type = null,
            $mimeType = null,
            $location = null,
            $categories = null,
            $slideshow = null,
            $order = null,
            $previewImage = null,
            $resourceGUID = null) {
        if(!is_null($guid)){
            $this->guid = $guid;
        }
        
        parent::__construct(null, $type, $mimeType, $location, $categories, $previewImage);
        
        if(!is_null($slideshow)){
            $this->slideshow = $slideshow;
        }
        
        if(!is_null($order)){
            $this->order = $order;
        }
        
        if(!is_null($previewImage)){
            $this->previewImage = $previewImage;
        }
        
        if(!is_null($resourceGUID)){
            $this->resource = $resourceGUID;
        }
    }
    
    public function createFromResource($resource, $slideshow, $order){
        $this->__construct(
                null,
                $resource->type,
                $resource->mimeType,
                $resource->location,
                $resource->categories,
                $slideshow->guid,
                $order,
                $resource->previewImage,
                $resource->guid
                );
    }
}
