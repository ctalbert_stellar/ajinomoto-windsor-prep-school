<?php
require_once 'dbContext.php';
require_once 'dbTable.php';
require_once 'Company.php';
require_once 'Address.php';
class TblCompanies extends dbTable{
    public function __construct(&$db, &$dbContext) {
        parent::__construct($db, $dbContext, "Companies");
    }
    
    public function generateContainerFromStatement($statement) {
        foreach($statement->fetchAll(PDO::FETCH_OBJ) as $row){ 
            $this->container[$row->guid] = new Company(
                    $row->guid,
                    $row->name,
                    $this->dbContext['Addresses']->findByCompanyGUID($row->guid)
                    );
        }
        
        return $this->container;
    }
    
    public function save(){
        
        $insertOrUpdateQuery = "INSERT INTO {$this->table}"
            . "("
                . "`guid`,"
                . "`name`"
            . ")VALUES("
                . ":guid,"
                . ":name"
            . ")ON DUPLICATE KEY UPDATE"
                . "`name` = VALUES(`name`)";
        $insertOrUpdateStatement = $this->db->prepare($insertOrUpdateQuery);
        
        $deleteQuery = "DELETE FROM {$this->table}"
            . "WHERE `guid` = :guid";
        $deleteStatement = $this->db->prepare($deleteQuery);
        
        foreach($this->container as $company){
            if($company->hasChanged){
                $this->updateAddresses($company);
                $insertOrUpdateStatement->execute(array('guid'=> $company->guid, 'name' => $company->name));
            }
            
            if($company->isDeleted){
                $deleteStatement->execute(array('guid' => $company->guid));
            }
        }
    }
    
    private function updateAddresses($company){        
        $addressQuery = "SELECT * FROM `AddressRelations` WHERE `relative` = :guid";
        $addressStatement = $this->db->prepare($addressQuery);
        $addressStatement->execute(array('guid' => $company->guid));
        
        $currentAddresses = array();
        $newAddresses = array();
        
        foreach($addressStatement->fetchAll(PDO::FETCH_OBJ) as $currentAddress){
            $currentAddresses[] = $currentAddress->Address;
        }
        
        $addressRelationInsertQuery = "INSERT INTO `AddressRelations`"
                    . "("
                       . "`address`,"
                       . "`relative`"
                    . ")VALUES("
                       . ":address,"
                       . ":relative"
                    . ")";
        
        $addressRelationInsertStatement = $this->db->prepare($addressRelationInsertQuery);
        
        foreach($company->addresses as $address){
            $newAddresses[] = $address->guid;
            $this->dbContext['Addresses']->addOrEdit($address);
            $this->dbContext['Addresses']->save();
            if(!in_array($address->guid, $currentAddresses)){
                $addressRelationInsertStatement->execute(array('address'=>$address->guid, 'relative' => $company->guid));
            }
        }
        $addressRelationDeleteQuery = "DELETE FROM `AddressRelations` WHERE `address` = :guid";
        $addressRelationDeleteStatement = $this->db->prepare($addressRelationDeleteQuery);
        foreach($currentAddresses as $current){
            if(!in_array($current, $newAddresses)){
                $addressRelationDeleteStatement->execute(array('guid'=>$current));
            }
        }
    }
}
