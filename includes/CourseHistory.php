<?php
require_once 'Course.php';
class CourseHistory extends Course {
    public $timestamp;
    public $person;
    public $grade;
    public $questions;
    public $courseGUID;
    
    public function __construct(
            $guid = null,
            $title = null,
            $categories = null,
            $questionPool = null,
            $numberOfQuestion = null,
            $videos = null,
            $timestamp = null,
            $person = null,
            $grade = null,
            $questions = null,
            $courseGUID = null) {
        parent::__construct(null, $title, $categories, $questionPool, $numberOfQuestion, $videos);
        
        if(!is_null($guid)){
            $this->guid = $guid;
        }
        
        if(!is_null($timestamp)){
            $this->timestamp = $timestamp;
        }
        if(!is_null($person)){
            $this->person = $person;
        }
        if(!is_null($grade)){
            $this->grade = $grade;
        }
        if(!is_null($questions)){
            $this->questions = $questions;
        }
        if(!is_null($courseGUID)){
            $this->courseGUID = $courseGUID;
        }
    }
    
    public function getGrade(){
        $totalCorrect = 0;
        foreach($this->questions as $question){
            if($question->correct){
                $totalCorrect++;
            }
        }
        $this->grade = ($totalCorrect/count($this->questions)) * 100;
        return $this->grade;
    }
}
