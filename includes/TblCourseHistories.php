<?php
require_once 'dbTable.php';
require_once 'CourseHistory.php';
class TblCourseHistories extends dbTable{
    public function __construct(&$db, &$dbContext) {
        parent::__construct($db, $dbContext, "CourseHistories");
    }
    
    public function generateContainerFromStatement($statement) {
        foreach($statement->fetchAll(PDO::FETCH_OBJ) as $row){
            $course = array_values($this->dbContext['Courses']->find($row->course))[0];
            $this->container[$row->guid] = new CourseHistory(
                    $row->guid,
                    $course->title,
                    $course->categories,
                    $this->dbContext['QuestionPools']->find($course->questionPool->guid),
                    $course->numberOfQuestions,
                    $course->videos,
                    $row->timestamp,
                    $row->person,
                    $row->grade,
                    $this->dbContext['QuestionHistories']->find(array('courseHistory' => $row->guid)),
                    $row->course
                );
        }
        
        return $this->container;
    }
    
    public function save(){
        $insertOrUpdateQuery = "INSERT INTO `{$this->table}`"
            . "("
                . "`guid`,"
                . "`course`,"
                . "`person`,"
                . "`timestamp`,"
                . "`grade`"
            . ")VALUES("
                . ":guid,"
                . ":course,"
                . ":person,"
                . ":timestamp,"
                . ":grade"
            . ")ON DUPLICATE KEY UPDATE"
                . "`course` = VALUES(`course`),"
                . "`person` = VALUES(`person`),"
                . "`timestamp` = VALUES(`timestamp`),"
                . "`grade` = VALUES(`grade`)";
        $insertOrUpdateStatement = $this->db->prepare($insertOrUpdateQuery);
        
        $deleteQuery = "DELETE * FROM {$this->table} WHERE `guid` = :guid";
        $deleteStatement = $this->db->prepare($deleteQuery);
                
        
        foreach($this->container as $courseHistory){
            if($courseHistory->hasChanged){
                $insertOrUpdateStatement->execute(array(
                    'guid' => $courseHistory->guid,
                    'course' => $courseHistory->courseGUID,
                    'person' => (!isset($courseHistory->person->guid)?$courseHistory->person:$courseHistory->person->guid),
                    'timestamp' => $courseHistory->timestamp,
                    'grade' => $courseHistory->grade
                ));
            }
            
            if($courseHistory->isDeleted){
                $deleteStatement->execute(array('guid' => $courseHistory->guid));
            }
        }
    }
    
    public function findByPersonGUID($guid){
        $this->find(array('person' => $guid));
        return $this->container;
    }
}
