<?php
require_once 'dbTable.php';
require_once 'Course.php';
class TblCourses extends dbTable{
    public function __construct(&$db, &$dbContext) {
        parent::__construct($db, $dbContext, "Courses");
    }
    
    public function generateContainerFromStatement($statement) {
        foreach($statement->fetchAll(PDO::FETCH_OBJ) as $row){ 
            $this->container[$row->guid] = new Course(
                    $row->guid,
                    $row->title,
                    $this->dbContext['Categories']->findByCourseGUID($row->guid),
                    array_values($this->dbContext['QuestionPools']->find($row->questionPool))[0],
                    $row->numberOfQuestions,
                    array_values($this->dbContext['Videos']->findByCourseGUID($row->guid))[0],
                    $row->active,
                    $row->description
                    );
        }
        
        return $this->container;
    }
    
    public function findByCategory($category){
        $query = "SELECT `relative` FROM `CategoryRelations` where `Category` = :catGUID";
        $statement = $this->db->prepare($query);
        $statement->execute(array('catGUID' => $category->guid));
        $return = array();
        foreach($statement->fetchAll(PDO::FETCH_OBJ) as $row){
            $temp = array_values($this->find($row->relative));
            if(count($temp) > 0){
                $return[$temp[0]->guid] = $temp[0];
            }
        }
        $this->container = $return;
        return $this->container;
    }
    
    public function getAllApproved(){
        $query = "SELECT * FROM `{$this->table}` WHERE `active` = 1";
        $statement = $this->db->query($query);
        return $this->generateContainerFromStatement($statement);
    }
    
    public function save(){
        $insertOrUpdateQuery = "INSERT INTO {$this->table}"
            . "("
                . "`guid`,"
                . "`title`,"
                . "`questionPool`,"
                . "`numberOfQuestions`,"
                . "`active`,"
                . "`description`"
            . ")VALUES("
                . ":guid,"
                . ":title,"
                . ":questionPool,"
                . ":numberOfQuestions,"
                . ":active,"
                . ":description"
            . ")ON DUPLICATE KEY UPDATE"
                . "`title` = VALUES(`title`),"
                . "`questionPool` = VALUES(`questionPool`),"
                . "`numberOfQuestions` = VALUES(`numberOfQuestions`),"
                . "`active` = VALUES(`active`),"
                . "`description` = VALUES(`description`)";
        $insertOrUpdateStatement = $this->db->prepare($insertOrUpdateQuery);
        
        $deleteQuery = "DELETE FROM {$this->table}"
            . "WHERE `guid` = :guid";
        $deleteStatement = $this->db->prepare($deleteQuery);
        
        foreach($this->container as $course){
            if($course->hasChanged){
                $this->UpdateCategories($course);               
                $insertOrUpdateStatement->execute(
                    array('guid'=> $course->guid,
                        'title' => $course->title,
                        'questionPool' => $course->questionPool->guid,
                        'numberOfQuestions' => $course->numberOfQuestions,
                        'active' => $course->active,
                        'description' => $course->description));
                $this->UpdateVideos($course); 
            }
            
            if($course->isDeleted){
                $deleteStatement->execute(array('guid' => $course->guid));
            }
        }
    }
    
    private function UpdateCategories($course){
        $categoryQuery = "SELECT * FROM `CategoryRelations` WHERE `relative` = :guid";
        $categoryStatement = $this->db->prepare($categoryQuery);
        $categoryStatement->execute(array('guid' => $course->guid));
        
        $currentCategories = array();
        $newCategories = array();
        
        foreach($categoryStatement->fetchAll(PDO::FETCH_OBJ) as $currentCategory){
            $currentCategories[] = $currentCategory->category;
        }
        
        $categoryRelationInsertQuery = "INSERT INTO `CategoryRelations`"
                    . "("
                       . "`category`,"
                       . "`relative`"
                    . ")VALUES("
                       . ":category,"
                       . ":relative"
                    . ")";
        
        $categoryRelationInsertStatement = $this->db->prepare($categoryRelationInsertQuery);
        
        foreach($course->categories as $category){
            $newCategories[] = $category->guid;
            $this->dbContext['Categories']->addOrEdit($category);
            $this->dbContext['Categories']->save();
            if(!in_array($category->guid, $currentCategories)){
                $categoryRelationInsertStatement->execute(array('category'=>$category->guid, 'relative' => $course->guid));
            }
        }
        $categoryRelationDeleteQuery = "DELETE FROM `CategoryRelations` WHERE `category` = :guid";
        $categoryRelationDeleteStatement = $this->db->prepare($categoryRelationDeleteQuery);
        foreach($currentCategories as $current){
            if(!in_array($current, $newCategories)){
                $categoryRelationDeleteStatement->execute(array('guid'=>$category->guid));
            }
        }
    }
    
    private function UpdateVideos($course){
        $videoRelationsQuery = "INSERT INTO `VideoRelations`"
                . "("
                    . "`video`,"
                    . "`course`"
                . ")VALUES("
                    . ":video,"
                    . ":course"
                . ")";
        
        $videoDeleteQuery = "DELETE FROM `VideoRelations` WHERE `course` = :id";
        $videoDeleteStatement = $this->db->prepare($videoDeleteQuery);
        $videoDeleteStatement->execute(array('id' => $course->guid));
        $videoRelationsStatement = $this->db->prepare($videoRelationsQuery);
        $this->dbContext['Videos']->addOrEdit($course->videos);
        $this->dbContext['Videos']->save();
        $videoRelationsStatement->execute(array('video' => $course->videos->guid, 'course' => $course->guid));
    }
}
