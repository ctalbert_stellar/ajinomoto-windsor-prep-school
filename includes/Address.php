<?php
require_once 'Model.php';
class Address extends Model{
    public $lineOne;
    public $lineTwo;
    public $city;
    public $state;
    public $zip;
    
    public function __construct(
            $guid = null,
            $lineOne = null,
            $lineTwo = null,
            $city = null,
            $state = null,
            $zip = null
            ) {
        if(!is_null($guid)){
            $this->guid = $guid;
        }
        if(!is_null($lineOne)){
            $this->lineOne = $lineOne;
        }
        if(!is_null($lineTwo)){
            $this->lineTwo = $lineTwo;
        }
        if(!is_null($city)){
            $this->city = $city;
        }
        if(!is_null($state)){
            $this->state = $state;
        }
        if(!is_null($zip)){
            $this->zip = $zip;
        }
        
        parent::__construct();
    }
}
