<?php
require_once 'dbTable.php';
require_once 'Resource.php';
class TblResources extends dbTable{
    public function __construct(&$db, &$dbContext) {
        parent::__construct($db, $dbContext, "Resources");
    }


    public function generateContainerFromStatement($statement){
        foreach($statement->fetchAll(PDO::FETCH_OBJ) as $row){ 
            $this->container[$row->guid] = new Resource(
                    $row->guid,
                    $row->type,
                    $row->mimeType,
                    $row->location,
                    $this->dbContext['Categories']->findByResourceGUID($row->guid),
                    $row->unlockedBy,
                    $row->preview
                    );
        }
        return $this->container;
    }

    public function findByPersonGUID($guid){
        return $this->findByRelative($guid);
    }
    
    private function findByRelative($guid){
        $selectQuery = "SELECT * FROM `ResourceRelations` WHERE `relative` = :guid";
        $selectStatement = $this->db->prepare($selectQuery);
        $selectStatement->execute(array('guid' => $guid));
        $first = TRUE;
        foreach($selectStatement->fetchAll(PDO::FETCH_OBJ) as $row){
            $this->find($row->resource, $first);
            if($first){
                $first = FALSE;
            }
        }
        
        return $this->container;
    }
    
    public function save(){
        $insertOrUpdateQuery = "INSERT INTO {$this->table}"
            . "("
                . "`guid`,"
                . "`type`,"
                . "`mimeType`,"
                . "`location`,"
                . "`unlockedBy`,"
                . "`preview`"
            . ")VALUES("
                . ":guid,"
                . ":type,"
                . ":mimeType,"
                . ":location,"
                . ":unlockedBy,"
                . ":preview"
            . ")ON DUPLICATE KEY UPDATE"
                . "`type` = VALUES(`type`),"
                . "`mimeType` = VALUES(`mimeType`),"
                . "`location` = VALUES(`location`),"
                . "`unlockedBy` = VALUES(`unlockedBy`),"
                . "`preview` = VALUES(`preview`)";
        $insertOrUpdateStatement = $this->db->prepare($insertOrUpdateQuery);
        
        $deleteQuery = "DELETE FROM {$this->table}"
            . " WHERE `guid` = :guid";
        $deleteRelations = "DELETE FROM `ResourceRelations` WHERE `resource` = :guid";
        $deleteStatement = $this->db->prepare($deleteQuery);
        $deleteRelationsStatement = $this->db->prepare($deleteRelations);
        foreach($this->container as $resource){
            if($resource->hasChanged){
                $this->UpdateCategories($resource);
                $insertOrUpdateStatement->execute(
                    array(
                        'guid' => $resource->guid,
                        'type' => $resource->type,
                        'mimeType' => $resource->mimeType,
                        'location' => $resource->location,
                        'unlockedBy' => $resource->unlockedBy,
                        'preview' => $resource->preview
                    ));
            }
            
            if($resource->isDeleted){
                $slides = $this->dbContext['Slides']->find(array('resource' => $resource->guid));
                foreach($slides as $slide){
                    $slide->isDeleted = TRUE;
                    $this->dbContext['Slides']->addOrEdit($slide);
                }
                $this->dbContext['Slides']->save();
                unlink(Config::$siteAbsPath . $resource->location);
                $deleteRelationsStatement->execute(array('guid' => $resource->guid));
                $deleteStatement->execute(array('guid' => $resource->guid));
            }
        }
    }
    
    private function UpdateCategories($resource){
        $categoryQuery = "SELECT * FROM `CategoryRelations` WHERE `relative` = :guid";
        $categoryStatement = $this->db->prepare($categoryQuery);
        $categoryStatement->execute(array('guid' => $resource->guid));
        
        $currentCategories = array();
        $newCategories = array();
        
        foreach($categoryStatement->fetchAll(PDO::FETCH_OBJ) as $currentCategory){
            $currentCategories[] = $currentCategory->category;
        }
        
        $categoryRelationInsertQuery = "INSERT INTO `CategoryRelations`"
                    . "("
                       . "`category`,"
                       . "`relative`"
                    . ")VALUES("
                       . ":category,"
                       . ":relative"
                    . ")";
        
        $categoryRelationInsertStatement = $this->db->prepare($categoryRelationInsertQuery);
        
        foreach($resource->categories as $category){
            $newCategories[] = $category->guid;
            $this->dbContext['Categories']->addOrEdit($category);
            $this->dbContext['Categories']->save();
            if(!in_array($category->guid, $currentCategories)){
                $categoryRelationInsertStatement->execute(array('category'=>$category->guid, 'relative' => $resource->guid));
            }
        }
        $categoryRelationDeleteQuery = "DELETE FROM `CategoryRelations` WHERE `category` = :guid AND `relative` = :relative";
        $categoryRelationDeleteStatement = $this->db->prepare($categoryRelationDeleteQuery);
        foreach($currentCategories as $current){
            if(!in_array($current, $newCategories)){
                $categoryRelationDeleteStatement->execute(array('guid'=>$current, 'relative' => $resource->guid));
            }
        }
    }
}
