<?php
require_once 'dbTable.php';
require_once 'QuestionPool.php';
class TblQuestionPools extends dbTable{
    public function __construct(&$db, &$dbContext) {
        parent::__construct($db, $dbContext, "QuestionPools");
    }
    
    public function generateContainerFromStatement($statement){
        foreach($statement->fetchAll(PDO::FETCH_OBJ) as $row){
            $this->container[$row->guid] = new QuestionPool(
                    $row->guid,
                    $row->name                    
                    );
            $this->container[$row->guid]->questions = $this->findQuestions($row->guid);
        }
        
        return $this->container;
    }
    
    private function findQuestions($poolGUID){
        $selectQuery = "SELECT * FROM `QuestionPoolRelations` WHERE `pool` = :guid";
        $selectStatement = $this->db->prepare($selectQuery);
        
        $selectStatement->execute(array('guid' => $poolGUID));
        
        $questions = array();
        foreach($selectStatement->fetchAll(PDO::FETCH_OBJ) as $row){
            $questions[$row->question] = array_values($this->dbContext['Questions']->find($row->question))[0];
        }
        return $questions;
    }
    
    public function save(){
        $insertOrUpdateQuery = "INSERT INTO {$this->table}"
            . "("
                . "`guid`,"
                . "`name`"
            . ")VALUES("
                . ":guid,"
                . ":name"
            . ")ON DUPLICATE KEY UPDATE"
                . "`name` = VALUES(`name`)";
            
        $insertOrUpdateStatement = $this->db->prepare($insertOrUpdateQuery);
        
        $deleteQuery = "DELETE FROM {$this->table}"
            . "WHERE `guid` = :guid";
        $deleteStatement = $this->db->prepare($deleteQuery);
        
        foreach($this->container as $questionPool){
            if($questionPool->hasChanged){
                $insertOrUpdateStatement->execute(array('guid'=>$questionPool->guid, 'name'=>$questionPool->name));
                $this->updateQuestions($questionPool);
            }
            
            if($questionPool->isDeleted){
                $deleteStatement->execute(array('guid' => $questionPool->guid));
            }
        }
    }
    
    private function updateQuestions($questionPool){
        $questionQuery = "SELECT * FROM `QuestionPoolRelations` WHERE `pool` = :guid";
        $questionStatement = $this->db->prepare($questionQuery);
        $questionStatement->execute(array('guid' => $questionPool->guid));
        
        $currentQuestions = array();
        $newQuestions = array();
        
        foreach($questionStatement->fetchAll(PDO::FETCH_OBJ) as $currentQuestion){
            $currentQuestions[] = $currentQuestion->question;
        }
        
        $questionRelationInsertQuery = "INSERT INTO `QuestionPoolRelations`"
                . "("
                    . "`pool`,"
                    . "`question`"
                . ")VALUES("
                    . ":pool,"
                    . ":question"
                . ")";
        $questionRelationInsertStatement = $this->db->prepare($questionRelationInsertQuery);
        
        foreach($questionPool->questions as $question){
            $newQuestions[] = $question->guid;
            $this->dbContext['Questions']->addOrEdit($question);
            $this->dbContext['Questions']->save();
            if(!in_array($question->guid, $currentQuestions)){
                $questionRelationInsertStatement->execute(array('pool' => $questionPool->guid, 'question' => $question->guid));
            }
        }
        
        $questionPoolRelationDeleteQuery = "DELETE FROM `QuestionPoolRelations` WHERE `question` = :guid AND `pool` = :pool";
        $questionPoolRelationDeleteStatement = $this->db->prepare($questionPoolRelationDeleteQuery);
        foreach($currentQuestions as $current){
            if(!in_array($current, $newQuestions)){
                $questionPoolRelationDeleteStatement->execute(array('guid'=>$current, 'pool' => $questionPool->guid));
            }
        }
    }
}
