<?php
require_once 'dbTable.php';
require_once 'Slide.php';
class TblSlides extends dbTable{
    public function __construct(&$db, &$dbContext) {
        parent::__construct($db, $dbContext, "Slides");
    }

    public function generateContainerFromStatement($statement){
        foreach($statement->fetchAll(PDO::FETCH_OBJ) as $row){
            $resource = array_values($this->dbContext['Resources']->find($row->resource))[0];
            $this->container[$row->guid] = new Slide(
                $row->guid,
                $resource->type,
                $resource->mimeType,
                $resource->location,
                $this->dbContext['Categories']->find(array('guid' => $resource->guid)),
                $row->slideshow,
                $row->order,
                $row->previewImage,
                $row->resource
                );
        }
        
        
        
        return $this->container;
    }
    
    public function save(){
        $insertOrUpdateQuery = "INSERT INTO {$this->table}"
            . "("
                . "`guid`,"
                . "`type`,"
                . "`mimeType`,"
                . "`location`,"
                . "`slideshow`,"
                . "`order`,"
                . "`previewImage`,"
                . "`resource`"
            . ")VALUES("
                . ":guid,"
                . ":type,"
                . ":mimeType,"
                . ":location,"
                . ":slideshow,"
                . ":order,"
                . ":previewImage,"
                . ":resourceGUID"
            . ")ON DUPLICATE KEY UPDATE"
                . "`type` = VALUES(`type`),"
                . "`mimeType` = VALUES(`mimeType`),"
                . "`location` = VALUES(`location`),"
                . "`order` = VALUES(`order`),"
                . "`previewImage` = VALUES(`previewImage`),"
                . "`resource` = VALUES(`resource`)";
        $insertOrUpdateStatement = $this->db->prepare($insertOrUpdateQuery);
        
        $deleteQuery = "DELETE FROM `{$this->table}`"
            . " WHERE `guid` = :guid";
        $deleteStatement = $this->db->prepare($deleteQuery);
        foreach($this->container as $slide){
            if($slide->hasChanged){
                $this->UpdateCategories($slide);
                $insertOrUpdateStatement->execute(
                    array(
                        'guid' => $slide->guid,
                        'type' => $slide->type,
                        'mimeType' => $slide->mimeType,
                        'location' => $slide->location,
                        'slideshow' => $slide->slideshow,
                        'order' => $slide->order,
                        'previewImage' => $slide->previewImage,
                        'resourceGUID' => $slide->resource
                    ));
            }
            
            if($slide->isDeleted){
                $deleteStatement->execute(array('guid' => $slide->guid));
            }
        }
    }
    
    private function UpdateCategories($slide){
        $categoryQuery = "SELECT * FROM `CategoryRelations` WHERE `relative` = :guid";
        $categoryStatement = $this->db->prepare($categoryQuery);
        $categoryStatement->execute(array('guid' => $slide->guid));
        
        $currentCategories = array();
        $newCategories = array();
        
        foreach($categoryStatement->fetchAll(PDO::FETCH_OBJ) as $currentCategory){
            $currentCategories[] = $currentCategory->category;
        }
        
        $categoryRelationInsertQuery = "INSERT INTO `CategoryRelations`"
                    . "("
                       . "`category`,"
                       . "`relative`"
                    . ")VALUES("
                       . ":category,"
                       . ":relative"
                    . ")";
        
        $categoryRelationInsertStatement = $this->db->prepare($categoryRelationInsertQuery);
        
        foreach($slide->categories as $category){
            $newCategories[] = $category->guid;
            $this->dbContext['Categories']->addOrEdit($category);
            $this->dbContext['Categories']->save();
            if(!in_array($category->guid, $currentCategories)){
                $categoryRelationInsertStatement->execute(array('category'=>$category->guid, 'relative' => $category->guid));
            }
        }
        $categoryRelationDeleteQuery = "DELETE FROM `CategoryRelations` WHERE `category` = :guid";
        $categoryRelationDeleteStatement = $this->db->prepare($categoryRelationDeleteQuery);
        foreach($currentCategories as $current){
            if(!in_array($current, $newCategories)){
                $categoryRelationDeleteStatement->execute(array('guid'=>$current));
            }
        }
    }
}
