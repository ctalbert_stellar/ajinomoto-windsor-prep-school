<?php
require_once 'Model.php';
require_once 'passwordFunctions.php';
class User extends Model{
    public $email;
    public $isApproved;
    public $isAdmin;
    public $person;
    public $password;
    
    public function __construct(
            $guid = null,
            $email = null,
            $isApproved = null,
            $isAdmin = null,
            $person = null,
            $password = null) {
        if(!is_null($guid)){
            $this->guid = $guid;
        }
        if(!is_null($email)){
            $this->email = $email;
        }
        if(!is_null($isApproved)){
            $this->isApproved = $isApproved;
        }
        if(!is_null($isAdmin)){
            $this->isAdmin = $isAdmin;
        }
        if(!is_null($person)){
            $this->person = $person;
        }else{
            $this->person = new Person();
        }
        
        if(!is_null($password)){
            $this->password = $password;
        }
        parent::__construct();
    }
    
    public function createPassword($password){
        $this->password = create_hash($password);
    }
    
    public function validatePassword($password){
        return validate_password($password, $this->password);
    }
    
    
}
