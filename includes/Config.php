<?php 
/****************************************************************
  * Sitewide Configuration
  * This file contains all kinds of static information used all
  * over the site, database info, social media links, default
  * meta tags, and some developer switches. For non site specific
  * constants, see the Values.php file.
  ***************************************************************/
class Config{

	//Site name for title
	public static $siteName = '';

	//Description for meta tags
	public static $metaDescription = '';

	/**
	  * Site Root is usually empty in production, but could be
	  * a subfolder of the host. ex: /dev
	  */
	public static $siteRoot = 'http://ajinomotowindsorprepschool.com';

	//Server-side path to the document root for your site
	public static $siteAbsPath = '/opt/www/windsorwonsource.com/public';
	
	//Social Media URLs Set 'em here then forget 'em
	public static $facebookURL = '';
	public static $twitterURL = '';
	public static $youtubeURL = '';
	public static $pinterestURL = '';
	
	//Database Info
	public static $dbUser = 'root';
	public static $dbPass = 'c9fC3WTzKA3aJP4';
	public static $dbHost = 'localhost';
	public static $dbSchema = 'wonsource';
        public static $dbCharSet = 'utf8';
        
        /**
         * Database Object Type
         * 
         * Default for now is MySQLi.  If you want to use PDO, set this
         * variable to "PDO" and the $db object will be changed accordingly.
         */
        public static $dbObjectType = 'PDO';
        
        //PDO Specific Options -- Will be ignored if not using PDO
        public static $dbPDOErrorMode = PDO::ERRMODE_EXCEPTION;

	/**
	  * Google Analytics Site Id
	  * If this is empty, or the IS_DEVELOPER flag is set,
	  * Google Analytics tracking code is left out.
	  * Format is: UA-XXXXX-X
	  */
	public static $googleAnalyticsId = '';

	//Timezone for PHP and MySQL functions.
	public static $timezone = 'America/New_York';
	
	//Debugging Variables
	public static $debug = FALSE;
	public static $developerIPs = array('');

}// end class Config