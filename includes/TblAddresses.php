<?php
require_once 'dbTable.php';
require_once 'Address.php';
class TblAddresses extends dbTable{
    public function __construct(&$db, &$dbContext) {
        parent::__construct($db, $dbContext, "Addresses");
    }
    
    public function generateContainerFromStatement($statement) {
        foreach($statement->fetchAll(PDO::FETCH_OBJ) as $row){ 
            $this->container[$row->guid] = new Address(
                    $row->guid,
                    $row->lineOne,
                    $row->lineTwo,
                    $row->city,
                    $row->state,
                    $row->zip
                    );
        }
        
        return $this->container;
    }
    
    public function findByPersonGUID($personGUID){
        return $this->findRelation($personGUID);
    }
    
    public function findByCompanyGUID($companyGUID){
        return $this->findRelation($companyGUID);
    }
    
    private function findRelation($guid){
        $query = "SELECT * FROM `AddressRelations` WHERE `relative` = :guid";
        $statement = $this->db->prepare($query);
        
        $statement->execute(array('guid' => $guid));
        
        foreach($statement->fetchAll(PDO::FETCH_OBJ) as $row){
            if(!isset($reset)){
                $reset = TRUE;
            }
            $this->find($row->Address, $reset);
            $reset = FALSE;
        }
        return $this->container;
    }
    
    public function save(){
        $insertOrUpdateQuery = "INSERT INTO `{$this->table}`"
                    . "("
                        . "`guid`,"
                        . "`lineOne`,"
                        . "`lineTwo`,"
                        . "`city`,"
                        . "`state`,"
                        . "`zip`"
                    . ")VALUES("
                        . ":guid,"
                        . ":lineOne,"
                        . ":lineTwo,"
                        . ":city,"
                        . ":state,"
                        . ":zip"
                    . ")ON DUPLICATE KEY UPDATE"
                        . "`lineOne` = VALUES(`lineOne`),"
                        . "`lineTwo` = VALUES(`lineTwo`),"
                        . "`city` = VALUES(`city`),"
                        . "`state` = VALUES(`state`),"
                        . "`zip` = VALUES(`zip`)";
        $insertOrUpdateStatement = $this->db->prepare($insertOrUpdateQuery);

        $deleteQuery = "DELETE FROM `{$this->table}`"
                . "WHERE `guid` = :guid";
        $deleteStatement = $this->db->prepare($deleteQuery);

        foreach($this->container as $address){
            if($address->hasChanged){
                $insertOrUpdateStatement->execute(array(
                    'guid' => $address->guid,
                    'lineOne' => $address->lineOne,
                    'lineTwo' => $address->lineTwo,
                    'city' => $address->city,
                    'state' => $address->state,
                    'zip' => $address->zip
                ));
            }

            if($address->isDeleted){
                $deleteStatement->execute(array('guid' => $address->guid));
                unset($this->container[$address->guid]);
            }
        }
    }
}
