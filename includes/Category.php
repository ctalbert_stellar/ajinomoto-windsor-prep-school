<?php
require_once 'Model.php';
class Category extends Model{
    public $name;
    
    public function __construct(
            $guid = null,
            $name = null
            ) {
        if(!is_null($guid)){
            $this->guid = $guid;
        }
        if(!is_null($name)){
            $this->name = $name;
        }
        
        parent::__construct();
    }
}
