<?php
require_once 'Question.php';
class QuestionHistory extends Question{
    public $correct;
    public $answers;
    public $question;
    public $courseHistory;
    
    public function __construct(
            $guid = null,
            $questionText = null,
            $potentialAnswers = null,
            $correctAnswers = null,
            $correct = null,
            $answers = null,
            $question = null,
            $courseHistory = null) {
        parent::__construct(null, $questionText, $potentialAnswers, $correctAnswers);
        if(!is_null($guid)){
            $this->guid = $guid;
        }
        if(!is_null($correct)){
            $this->correct = $correct;
        }
        if(!is_null($answers)){
            $this->answers = $answers;
        }
        if(!is_null($question)){
            $this->question = $question;
        }
        if(!is_null($courseHistory)){
            $this->courseHistory = $courseHistory;
        }
    }
}
