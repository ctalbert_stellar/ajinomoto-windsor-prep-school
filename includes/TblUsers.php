<?php
require_once 'dbTable.php';
require_once 'User.php';
class TblUsers extends dbTable{
    public function __construct(&$db, &$dbContext) {
        parent::__construct($db, $dbContext, 'Users');
    }
    
    public function generateContainerFromStatement($statement){
        foreach($statement->fetchAll(PDO::FETCH_OBJ) as $user){
            $person = array_values($this->dbContext['Persons']->find($user->person))[0];
            $this->container[$user->guid] = new User(
                    $user->guid,
                    $user->email,
                    $user->isApproved,
                    $user->isAdmin,
                    $person,
                    $user->password
                    );
        }
        
        return $this->container;
    }
    
    public function save(){
        $insertOrUpdateQuery = "INSERT INTO {$this->table}"
            . "("
                . "`guid`,"
                . "`email`,"
                . "`isApproved`,"
                . "`isAdmin`,"
                . "`person`,"
                . "`password`"
            . ")VALUES("
                . ":guid,"
                . ":email,"
                . ":isApproved,"
                . ":isAdmin,"
                . ":person,"
                . ":password"
            . ")ON DUPLICATE KEY UPDATE"
                . "`email` = VALUES(`email`),"
                . "`isApproved` = VALUES(`isApproved`),"
                . "`isAdmin` = VALUES(`isAdmin`),"
                . "`person` = VALUES(`person`),"
                . "`password` = VALUES(`password`)";
        $insertOrUpdateStatement = $this->db->prepare($insertOrUpdateQuery);
        
        $deleteQuery = "DELETE FROM {$this->table} WHERE `guid` = :guid";
        $deleteStatement = $this->db->prepare($deleteQuery);
        
        foreach($this->container as $user){
            if($user->hasChanged){
                $this->dbContext['Persons']->addOrEdit($user->person);
                $this->dbContext['Persons']->save();
                $insertOrUpdateStatement->execute(
                        array(
                            'guid' => $user->guid,
                            'email' => $user->email,
                            'isApproved' => $user->isApproved,
                            'isAdmin' => $user->isAdmin,
                            'person' => $user->person->guid,
                            'password' => $user->password)
                        );
            }
            
            if($user->isDeleted){
                $deleteStatement->execute(
                        array(
                            'guid' => $user->guid
                        ));
            }
        }
    }
    
    public function login($email, $password){
        $users = $this->find(array('email' => $email));
        if(count($users) > 0){
            $tempUser = array_values($users)[0];
            if($tempUser->validatePassword($password)){
                $_SESSION['userGUID'] = $tempUser->guid;
                $_SESSION['userEmail'] = $tempUser->email;
                $_SESSION['userLoggedIn'] = TRUE;
                $_SESSION['user'] = $tempUser;
                return TRUE;
            }
        }
        return FALSE;
    }
}
