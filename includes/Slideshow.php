<?php
require_once 'Model.php';
class Slideshow extends Model{
    public $title;
    public $slides;
    public $updated;
    public $person;
    
    public function __construct(
            $guid = null,
            $title = null,
            $slides = null,
            $updated = null,
            $person = null) {
        parent::__construct();
        
        if(!is_null($guid)){
            $this->guid = $guid;
        }
        
        if(!is_null($title)){
            $this->title = $title;
        }
        
        if(!is_null($slides)){
            $this->slides = $slides;
        }
        
        if(!is_null($updated)){
            $this->updated = $updated;
        }else{
            $updated = new DateTime();
        }
        
        if(!is_null($person)){
            $this->person = $person;
        }
    }
}
