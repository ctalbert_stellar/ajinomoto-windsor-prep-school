<?php
require_once 'dbTable.php';
require_once 'Question.php';
class TblQuestions extends dbTable{
    public function __construct(&$db, &$dbContext) {
        parent::__construct($db, $dbContext, "Questions");
    }
    
    public function generateContainerFromStatement($statement){
        foreach($statement->fetchAll(PDO::FETCH_OBJ) as $row){
            $this->container[$row->guid] = new Question(
                    $row->guid,
                    $row->questionText,
                    json_decode($row->potentialAnswers),
                    json_decode($row->correctAnswers)
                    );
        }
        
        return $this->container;
    }
        
    public function save(){
        $insertOrUpdateQuery = "INSERT INTO {$this->table}"
            . "("
                . "`guid`,"
                . "`questionText`,"
                . "`potentialAnswers`,"
                . "`correctAnswers`"
            . ")VALUES("
                . ":guid,"
                . ":questionText,"
                . ":potentialAnswers,"
                . ":correctAnswers"
            . ")ON DUPLICATE KEY UPDATE"
                . "`questionText` = VALUES(`questionText`),"
                . "`potentialAnswers` = VALUES(`potentialAnswers`),"
                . "`correctAnswers` = VALUES(`correctAnswers`)";
        $insertOrUpdateStatement = $this->db->prepare($insertOrUpdateQuery);
        
        $deleteQuery = "DELETE FROM {$this->table} WHERE `guid` = :guid";
        $deleteStatement = $this->db->prepare($deleteQuery);
        
        foreach($this->container as $question){
            if($question->hasChanged){
                $insertOrUpdateStatement->execute(array(
                    'guid' => $question->guid,
                    'questionText' => $question->questionText,
                    'potentialAnswers' => json_encode($question->potentialAnswers),
                    'correctAnswers' => json_encode($question->correctAnswers)
                    ));
            }
            
            if($question->isDeleted){
                $deleteStatement->execute(array('guid' => $question->guid));
            }
        }
    }
}
