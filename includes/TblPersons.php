<?php
require_once 'dbTable.php';
require_once 'Person.php';
class TblPersons extends dbTable{
    public function __construct(&$db, &$dbContext) {
        parent::__construct($db, $dbContext, "Persons");
    }
   
    public function generateContainerFromStatement($statement) {
        foreach($statement->fetchAll(PDO::FETCH_OBJ) as $row){ 
            $this->container[$row->guid] = new Person(
                    $row->guid,
                    $row->firstName,
                    $row->lastName,
                    array_values($this->dbContext['Companies']->find($row->company))[0],
                    $this->dbContext['Addresses']->findByPersonGUID($row->guid),
                    $row->phone,
                    $row->dsm,
                    $row->brokerNumber,
                    $this->dbContext['Resources']->findByPersonGUID($row->guid),
                    $this->dbContext['CourseHistories']->find(array('person' => $row->guid)),
                    $this->dbContext['Slideshows']->find(array('person' => $row->guid))
                    );
        }
        return $this->container;
    }

        public function save(){
        $insertOrUpdateQuery = "INSERT INTO {$this->table}"
            . "("
                . "`guid`,"
                . "`firstName`,"
                . "`lastName`,"
                . "`company`,"
                . "`phone`,"
                . "`dsm`,"
                . "`brokerNumber`"
            . ")VALUES("
                . ":guid,"
                . ":firstName,"
                . ":lastName,"
                . ":company,"
                . ":phone,"
                . ":dsm,"
                . ":brokerNumber"
            . ")ON DUPLICATE KEY UPDATE"
                . "`firstName` = VALUES(`firstName`),"
                . "`lastName` = VALUES(`lastName`),"
                . "`company` = VALUES(`company`),"
                . "`phone` = VALUES(`phone`),"
                . "`dsm` = VALUES(`dsm`),"
                . "`brokerNumber` = VALUES(`brokerNumber`)";
        $insertOrUpdateStatement = $this->db->prepare($insertOrUpdateQuery);
        
        $deleteQuery = "DELETE FROM {$this->table} WHERE `guid` = :guid";
        $deleteStatement = $this->db->prepare($deleteQuery);
        foreach($this->container as $person){
            if($person->hasChanged){
                $this->dbContext['Companies']->addOrEdit($person->company);
                $this->dbContext['Companies']->save();
                
                $this->updateAddresses($person);
                $this->updateResources($person);
                $this->updateCourseHistory($person); 

                $insertOrUpdateStatement->execute(array(
                    'guid' => $person->guid,
                    'firstName' => $person->firstName,
                    'lastName' => $person->lastName,
                    'company' => $person->company->guid,
                    'phone' => $person->phone,
                    'dsm' => $person->dsm,
                    'brokerNumber' => $person->brokerNumber
                ));
            }
            
            if($person->isDeleted){
                $deleteStatement->execute(array('guid' => $person->guid));
            }
        }
    }
    
    private function updateAddresses($person){   
        $addressQuery = "SELECT * FROM `AddressRelations` WHERE `relative` = :guid";
        $addressStatement = $this->db->prepare($addressQuery);
        $addressStatement->execute(array('guid' => $person->guid));
        
        $currentAddresses = array();
        $newAddresses = array();
        
        foreach($addressStatement->fetchAll(PDO::FETCH_OBJ) as $currentAddress){
            $currentAddresses[] = $currentAddress->Address;
        }
        
        $addressRelationInsertQuery = "INSERT INTO `AddressRelations`"
                    . "("
                       . "`address`,"
                       . "`relative`"
                    . ")VALUES("
                       . ":address,"
                       . ":relative"
                    . ")";
        
        $addressRelationInsertStatement = $this->db->prepare($addressRelationInsertQuery);
        
        foreach($person->addresses as $address){
            $newAddresses[] = $address->guid;
            $this->dbContext['Addresses']->addOrEdit($address);
            $this->dbContext['Addresses']->save();
            if(!in_array($address->guid, $currentAddresses)){
                $addressRelationInsertStatement->execute(array('address'=>$address->guid, 'relative' => $person->guid));
            }
        }
        $addressRelationDeleteQuery = "DELETE FROM `AddressRelations` WHERE `address` = :guid";
        $addressRelationDeleteStatement = $this->db->prepare($addressRelationDeleteQuery);
        foreach($currentAddresses as $current){
            if(!in_array($current, $newAddresses)){
                $addressRelationDeleteStatement->execute(array('guid'=>$current));
            }
        }
    }
    
    private function updateResources($person){
        if(isset($person->resources)){
            $resourcesQuery = "SELECT * FROM `ResourceRelations` WHERE `relative` = :guid";
            $resourceStatement = $this->db->prepare($resourcesQuery);
            $resourceStatement->execute(array('guid' => $person->guid));

            $currentResources = array();
            $newResources = array();
            if($resourceStatement->rowCount() > 0){
                foreach($resourceStatement->fetchAll(PDO::FETCH_OBJ) as $row){
                    $currentResources[] = $row->resource;
                }
            }

            $resourceRelationInsertQuery = "INSERT INTO `ResourceRelations`"
                    . "("
                        . "`resource`,"
                        . "`relative`"
                    . ")VALUES("
                        . ":resource,"
                        . ":relative"
                    . ")";
            $resourceRelationInsertStatement = $this->db->prepare($resourceRelationInsertQuery);

            foreach($person->resources as $resource){
                $newResources[] = $resource->guid;
                if(!in_array($resource->guid, $currentResources)){
                    $resourceRelationInsertStatement->execute(
                            array(
                                'resource' => $resource->guid,
                                'relative' => $person->guid
                            ));
                }
            }

            $resourceRelationDeleteQuery = "DELETE FROM `ResourceRelations` WHERE `resource` = :guid";
            $resourceRelationDeleteStatement = $this->db->prepare($resourceRelationDeleteQuery);

            foreach($currentResources as $resource){
                if(!in_array($resource, $newResources)){
                    $resourceRelationDeleteStatement->execute(array('guid' => $resource));
                }
            }
        }
    }
    
    private function updateCourseHistory($person){
        if(isset($person->courseHistory)){
            foreach($person->courseHistory as $courseHistory){
                $this->dbContext['CourseHistories']->addOrEdit($courseHistory);
            }
            $this->dbContext['CourseHistories']->save();
        }
    }
}
