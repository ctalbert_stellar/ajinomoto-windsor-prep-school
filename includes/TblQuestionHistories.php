<?php
require_once 'dbTable.php';
require_once 'QuestionHistory.php';
class TblQuestionHistories extends dbTable{
    public function __construct(&$db, &$dbContext) {
        parent::__construct($db, $dbContext, "QuestionHistories");
    }

    public function generateContainerFromStatement($statement){
        foreach($statement->fetchAll(PDO::FETCH_OBJ) as $row){ 
            $question = array_values($this->dbContext['Questions']->find($row->question))[0];
            $this->container[$row->guid] = new QuestionHistory(
                    $row->guid,
                    $question->questionText,
                    $question->potentialAnswers,
                    $question->correctAnswers,
                    $row->correct,
                    json_decode($row->answers),
                    $row->question,
                    $row->courseHistory
                    );
        }
        
        return $this->container;
    }
    
    public function save(){
        $insertOrUpdateQuery = "INSERT INTO {$this->table}"
            . "("
                . "`guid`,"
                . "`correct`,"
                . "`answers`,"
                . "`question`,"
                . "`courseHistory`"
            . ")VALUES("
                . ":guid,"
                . ":correct,"
                . ":answers,"
                . ":question,"
                . ":courseHistory"
            . ")ON DUPLICATE KEY UPDATE"
                . "`guid` = VALUES(`guid`),"
                . "`correct` = VALUES(`correct`),"
                . "`answers` = VALUES(`answers`),"
                . "`question` = VALUES(`question`),"
                . "`courseHistory` = VALUES(`courseHistory`)";
        $insertOrUpdateStatement = $this->db->prepare($insertOrUpdateQuery);
        
        $deleteQuery = "DELETE FROM {$this->table} WHERE `guid` = :guid";
        $deleteStatement = $this->db->prepare($deleteQuery);
        
        foreach($this->container as $questionHistory){
            if($questionHistory->hasChanged){
                $insertOrUpdateStatement->execute(
                        array(
                            'guid' => $questionHistory->guid,
                            'correct' => $questionHistory->correct,
                            'answers' => json_encode($questionHistory->answers),
                            'question' => $questionHistory->question,
                            'courseHistory' => $questionHistory->courseHistory
                        ));
            }
            
            if($questionHistory->isDeleted){
                $deleteStatement->execute(array('guid' => $questionHistory->guid));
            }
        }
    }
}
