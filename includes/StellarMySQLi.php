<?php
/****************************************************************
  * StellarMySQLi
  * This class is an extension to PHP's default mysqli class,
  * it adds a few helpful methods for retrieving data from an
  * array and inserting an array into your tables.
  ***************************************************************/
class StellarMySQLi extends mysqli{

        /**
	 * query_and_fetch_assoc($query)
         * 
         * @author Chris Hendrickson <chris37879@gmail.com>
	 * @param string $query A string that is executed on the server.
	 * @return array|bool A multidimensional array of results,
	 *          or FALSE on failure.
	 */
	function query_and_fetch_assoc($query){
		$results = $this->query($query);
		if($this->errno > 0){
			return FALSE;
		}
		if($results->num_rows > 0){			
			for($i = 0; $i < $results->num_rows; $i++){
				$rows[] = $results->fetch_assoc();
			}
			return $rows;
		}else{
			return FALSE;
		}
	}

        /**
         * insert_array($table, $data, $exclude)
         * 
         * @author Chris Hendrickson <chris37879@gmail.com>
         * @param string $table The name of a table
         *          in the MySQL database
         * @param array $data A multidimensional array of data to 
         *         store into the $table specified.
         * @param array $exclude An array of keys in the data to be
         *            excluded from the insert.
         * @return array An array containing any error that happened,
         *           or an array of information about the insert
         *           such as the insert id, number of rows, 
         *           and any info messages from MySQL
         */
	function insert_array($table, $data, $exclude = array()) {
	
	    $fields = $values = array();
	
	    if( !is_array($exclude) ) $exclude = array($exclude);
	
	    foreach( array_keys($data) as $key ) {
	        if( !in_array($key, $exclude) ) {
	            $fields[] = "`$key`";
	            $values[] = "'" . mysqli_real_escape_string($this, $data[$key]) . "'";
	        }
	    }
	
	    $fields = implode(",", $fields);
	    $values = implode(",", $values);
	
	    if( mysqli_query($this, "INSERT INTO `$table` ($fields) VALUES ($values)") ) {
	        return array( "mysql_error" => false,
	                      "mysql_insert_id" => mysqli_insert_id($this),
	                      "mysql_affected_rows" => mysqli_affected_rows($this),
	                      "mysql_info" => mysqli_info($this)
	                    );
	    } else {
	        return array( "mysql_error" => mysqli_error($this) );
	    }
	
	}
}//end class StellarMySQLi