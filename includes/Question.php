<?php
require_once 'Model.php';
class Question extends Model{
    public $guid;
    public $questionText;
    public $potentialAnswers;
    public $correctAnswers;
    
    public function __construct(
            $guid = null,
            $questionText = null,
            $potentialAnswers = null,
            $correctAnswers = null
            ) {
        if(!is_null($guid)){
            $this->guid = $guid;
        }
        if(!is_null($questionText)){
            $this->questionText = $questionText;
        }
        if(!is_null($potentialAnswers)){
            $this->potentialAnswers = $potentialAnswers;
        }
        if(!is_null($correctAnswers)){
            $this->correctAnswers = $correctAnswers;
        }
        
        parent::__construct();
    }
    
    public function checkAnswers($answers){
        foreach($answers as $answer){
            if(!in_array($answer, $this->correctAnswers)){
                return false;
            }
        }
        return true;
    }
    
    public function getAnswers(){
        shuffle($this->potentialAnswers);
        return $this->potentialAnswers;
    }
}
