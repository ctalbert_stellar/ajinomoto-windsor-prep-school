<?php
include 'FileGenerator.php';

class iCalGenerator extends FileGenerator{
    /* Class Members */
    private $name;
    private $organizerName;
    private $organizerEmail;
    private $startYear;
    private $startMonth;
    private $startDay;
    private $startTime;
    private $endYear;
    private $endMonth;
    private $endDay;
    private $endTime;

    private $timezone;

    private static $ICALFORMAT;


/*
    private static $ICALFORMAT = <<<EOT
BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//CHendrickson Boilerplate//NONSGML v1.0//EN
BEGIN:VEVENT
UID:uid1@example.com
DTSTAMP;TZID=%16$0s:%4$0s%5$02d%6$02dT%7$06d
ORGANIZER;CN=%2$0s:MAILTO:%3$0s
DTSTART;TZID=%16$0s:%8$0s%9$02d%10$02dT%11$06d
DTEND;TZID=%16$0s:%12$0s%13$02d%14$02dT%15$06d
SUMMARY:%1$0s
END:VEVENT
END:VCALENDAR
EOT;
*/

    //Constructor
    function __construct(){
        parent::__construct();
        $this->setContentType('text/calendar');
        self::$ICALFORMAT =
        'BEGIN:VCALENDAR'.PHP_EOL.
		'VERSION:2.0'.PHP_EOL.
		'PRODID:-//CHendrickson Boilerplate//NONSGML v1.0//EN'.PHP_EOL.
		'BEGIN:VEVENT'.PHP_EOL.
		'UID:uid1@example.com'.PHP_EOL.
		'DTSTAMP;TZID=%16$0s:%4$0s%5$02d%6$02dT%7$06d'.PHP_EOL.
		'ORGANIZER;CN=%2$0s:MAILTO:%3$0s'.PHP_EOL.
		'DTSTART;TZID=%16$0s:%8$0s%9$02d%10$02dT%11$06d'.PHP_EOL.
		'DTEND;TZID=%16$0s:%12$0s%13$02d%14$02dT%15$06d'.PHP_EOL.
		'SUMMARY:%1$0s'.PHP_EOL.
		'END:VEVENT'.PHP_EOL.
		'END:VCALENDAR';
    }

    public function getFileData(){
        //Save the old Timezone
        $oldTZ = @date_default_timezone_get();
        //Change the Timezone to what we set
        date_default_timezone_set($this->getTimezone());

        $currentYear = intval(date('Y'));
        $currentMonth = intval(date('n'));
        $currentDay = intval(date('j'));
        $currentTime = intval(date('Gis'));

        //Change the timezone back.
        date_default_timezone_set($oldTZ);

        return sprintf(
                    self::$ICALFORMAT,
                    $this->getName(),
                    $this->getOrganizerName(),
                    $this->getOrganizerEmail(),
                    $currentYear, $currentMonth, $currentDay, $currentTime,
                    $this->getStartYear(), $this->getStartMonth(), $this->getStartDay(), $this->getStartTime(),
                    $this->getEndYear(), $this->getEndMonth(), $this->getEndDay(), $this->getEndTime(),
                    $this->getTimezone()
                    );
    }


    /* Setters and Getters */

    public function getName(){
        return $this->name;
    }
    public function setName($name){
        $this->name = $name;
    }

    public function getOrganizerName(){
        return $this->organizerName;
    }
    public function setOrganizerName($organizerName){
        $this->organizerName = $organizerName;
    }

    public function getOrganizerEmail(){
        return $this->organizerEmail;
    }
    public function setOrganizerEmail($organizerEmail){
        $this->organizerEmail = $organizerEmail;
    }

    public function getStartYear(){
        return $this->startYear;
    }
    public function setStartYear($startYear){
        $this->startYear = $startYear;
    }

    public function getStartMonth(){
        return $this->startMonth;
    }
    public function setStartMonth($startMonth){
        $this->startMonth = $startMonth;
    }

    public function getStartDay(){
        return $this->startDay;
    }
    public function setStartDay($startDay){
        $this->startDay = $startDay;
    }

    public function getStartTime(){
        return $this->startTime;
    }
    public function setStartTime($startTime){
        $this->startTime = $startTime;
    }

    public function getEndYear(){
        return $this->endYear;
    }
    public function setEndYear($endYear){
        $this->endYear = $endYear;
    }

    public function getEndMonth(){
        return $this->endMonth;
    }
    public function setEndMonth($endMonth){
        $this->endMonth = $endMonth;
    }

    public function getEndDay(){
        return $this->endDay;
    }
    public function setEndDay($endDay){
        $this->endDay = $endDay;
    }

    public function getEndTime(){
        return $this->endTime;
    }
    public function setEndTime($endTime){
        $this->endTime = $endTime;
    }

    public function getTimezone(){
        return $this->timezone;
    }
    public function setTimezone($_timezone){
        $this->timezone = $_timezone;
    }


}