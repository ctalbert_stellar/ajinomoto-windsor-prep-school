<?php

abstract class FileGenerator{

    /* Class Members */
    private $filename;
    private $path;
    private $contentType;

    /* Force concrete class to define these methods */
    abstract protected function getFileData(); //Get the file from the generator.

    public function __construct(){
        return;
    }
    /* Common Methods */
    public function saveFile(){
        if(@file_put_contents($this->getFullFilename(), $this->getFileData(), LOCK_EX) === FALSE){
            throw new Exception('Unable to Save File: ' . $this->getFullFilename);
        }
    }

    public function getFullFilename(){
        return $this->getPath() . $this->getFilename();
    }


    /* Setters and Getters */

    public function getFilename(){
        return $this->filename;
    }
    public function setFilename($filename){
        $this->filename = $filename;
    }

    public function getPath(){
        return $this->path;
    }
    //This function will normalize the path to end in a DIRECTORY_SEPERATOR
    public function setPath($path){
        $temp = ((substr($path, -1) === DIRECTORY_SEPARATOR) ? $path : $path . DIRECTORY_SEPARATOR);
        if(is_dir($temp)){
            $this->path = $temp;
        }else{
            throw new Exception('Invalid Path: ' . $temp);
        }
    }

    public function getContentType(){
        return $this->contentType;
    }
    public function setContentType($contentType){
        $this->contentType = $contentType;
    }

}