<?php
require_once 'dbTable.php';
require_once 'Video.php';
class TblVideos extends dbTable{
    public function __construct(&$db, &$dbContext) {
        parent::__construct($db, $dbContext, "Videos");
    }
    
    public function generateContainerFromStatement($statement){
        foreach($statement->fetchAll(PDO::FETCH_OBJ) as $row){
            $this->container[$row->guid] = new Video(
                    $row->guid,
                    $row->location
                    );
        }
        
        return $this->container;
    }
    
    public function save(){
        $insertOrUpdateQuery = "INSERT INTO {$this->table}"
            . "("
                . "`guid`,"
                . "`location`"
            . ")VALUES("
                . ":guid,"
                . ":location"
            . ") ON DUPLICATE KEY UPDATE"
                . "`location` = VALUES(`location`)";
            
        $insertOrUpdateStatement = $this->db->prepare($insertOrUpdateQuery);
        
        $deleteQuery = "DELETE FROM {$this->table}"
                . "WHERE `guid` = :guid";
            
        $deleteStatement = $this->db->prepare($deleteQuery);
    
        foreach($this->container as $video)
        {
            if($video->hasChanged){
                $insertOrUpdateStatement->execute(array('guid'=> $video->guid, 'location' => $video->location));
            }
            
            if($video->isDeleted){
                $deleteStatement->execute(array('guid' => $video->guid));
            }
        }
    }
    
    public function findByCourseGUID($guid) {
        $videoRelationsQuery = "SELECT `video`"
                . "FROM `VideoRelations`"
                . "WHERE `course` = :guid";
        $videoRelationsStatement = $this->db->prepare($videoRelationsQuery);
        $videoRelationsStatement->execute(array('guid' => $guid));
        
        $results = $videoRelationsStatement->fetch(PDO::FETCH_OBJ);
        
        return $this->find($results->video);
        
    }
}
