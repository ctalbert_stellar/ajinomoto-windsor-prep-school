<?php
require_once 'Model.php';
class Course extends Model{
    public $title;
    public $categories;
    public $questionPool;
    public $numberOfQuestions;
    public $videos;
    public $questions = null;
    public $active;
    public $description;
    
    public function __construct(
            $guid = null,
            $title = null,
            $categories = null,
            $questionPool = null,
            $numberOfQuestions = null,
            $videos = null,
            $active = null,
            $description = null
            ) {
        if(!is_null($guid)){
            $this->guid = $guid;
        }
        if(!is_null($title)){
            $this->title = $title;
        }
        if(!is_null($categories)){
            $this->categories = $categories;
        }
        if(!is_null($questionPool)){
            $this->questionPool = $questionPool;
        }else{
            $this->questionPool = new QuestionPool();
        }
        if(!is_null($numberOfQuestions)){
            $this->numberOfQuestions = $numberOfQuestions;
        }
        if(!is_null($videos)){
            $this->videos = $videos;
        }
        if(!is_null($active)){
            $this->active = $active;
        }
        if(!is_null($description)){
            $this->description = $description;
        }
        
        parent::__construct();
    }
    
    public function getQuestions($numberOfQuestions = NULL){
        if(is_null($numberOfQuestions)){
            $numberOfQuestions = $this->numberOfQuestions;
        }
        
        $potentialQuestions = array();
        foreach($this->questionPool->questions as $potentialQuestion){
            $potentialQuestions[] = $potentialQuestion;
        }

        shuffle($potentialQuestions);

        $this->questions = array();
        while(($element = array_pop($potentialQuestions)) && count($this->questions) < $numberOfQuestions){
            $this->questions[$element->guid] = $element;
        }

        return $this->questions;
    }
}
