<?php
require_once 'Model.php';
class Resource extends Model{
    public $type;
    public $mimeType;
    public $location;
    public $categories;
    public $unlockedBy;
    public $preview;
    
    public function __construct(
            $guid = null,
            $type = null,
            $mimeType = null,
            $location = null,
            $categories = null,
            $unlockedBy = null,
            $preview = null
            ) {
        if(!is_null($guid)){
            $this->guid = $guid;
        }
        if(!is_null($type)){
            $this->type = $type;
        }
        if(!is_null($mimeType)){
            $this->mimeType = $mimeType;
        }
        if(!is_null($location)){
            $this->location = $location;
        }
        if(!is_null($categories)){
            $this->categories = $categories;
        }
        if(!is_null($unlockedBy)){
            $this->unlockedBy = $unlockedBy;
        }
        if(!is_null($preview)){
            $this->preview = $preview;
        }
        
        parent::__construct();
    }
}
