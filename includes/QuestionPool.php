<?php
require_once 'Model.php';
class QuestionPool extends Model {
    public $name;
    public $questions;
    
    public function __construct(
            $guid = null,
            $name = null,
            $questions = null
            ) {
        if(!is_null($guid)){
            $this->guid = $guid;
        }
        if(!is_null($name)){
            $this->name = $name;
        }
        if(!is_null($questions)){
            $this->questions = $questions;
        }
        
        parent::__construct();
    }
}
