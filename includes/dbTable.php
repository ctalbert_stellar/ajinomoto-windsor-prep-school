<?php
abstract class dbTable implements ArrayAccess{
    //Table name
    protected $table = "";
    
    public $tableChanged = FALSE;
    
    /**
     *
     * @var PDO 
     */
    protected $db = null;
    protected $dbContext = null;
    
    public $container;
    
    public function __construct(&$db, &$dbContext, $table) {
        $this->container = array();
        $this->db = $db;
        $this->dbContext = $dbContext;
        $this->table = $table;
    }
    
    public function find($arguments, $reset = TRUE){
        if($reset){
            $this->container = array();
        }
        $statement = null;
        if(!is_array($arguments)){
            $selectQuery = "SELECT * "
                . "FROM `{$this->table}` "
                . "WHERE `guid` = :field "
                . "ORDER BY `guid` ASC";
            $statement = $this->db->prepare($selectQuery);
            $statement->execute(array(
                'field' => $arguments
            ));
        }else{
            $selectQuery = "SELECT * "
                    . "FROM `{$this->table}` "
                    . "WHERE";
            $first = TRUE;
            foreach($arguments as $field => $value){
                if($first){
                    $first = FALSE;
                    $selectQuery .= " `$field` = :$field";
                }else{
                    $selectQuery .= " AND `$field` = :$field";
                }
            }
            $selectQuery .= " ORDER BY `guid` ASC";
            $statement = $this->db->prepare($selectQuery);
            $statement->execute($arguments);
        }
        if($statement->rowCount() < 1){
            $this->container = array();
        }
        
        return $this->generateContainerFromStatement($statement);
    }
    
    public function getAll(){
        $this->container = array();
        $query = "SELECT * FROM {$this->table}";
        $statement = $this->db->prepare($query);
        $statement->execute();
        return $this->generateContainerFromStatement($statement);
    }
    
    abstract function generateContainerFromStatement($statement);


    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }
    public function offsetExists($offset) {
        return isset($this->container[$offset]);
    }
    public function offsetUnset($offset) {
        unset($this->container[$offset]);
    }
    public function offsetGet($offset) {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }
    
    public function addOrEdit($object){
        if(is_subclass_of($object, 'Model')){
            $object->hasChanged = TRUE;
            $this->container[$object->guid] = $object;
        }else{
            throw new InvalidArgumentException('Object is not a model');
        }
    }
    
    public function delete($object){
        if(is_subclass_of($object, 'Model')){
            $object->hasChanged = FALSE;
            $object->isDeleted = TRUE;
        }else{
            throw new InvalidArgumentException('Object is not a model');
        }
    }
}
