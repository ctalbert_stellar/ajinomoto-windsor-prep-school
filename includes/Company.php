<?php
require_once 'Model.php';
class Company extends Model{
    public $name;
    public $addresses;
    public $brokers;
    
    public function __construct(
            $guid = null,
            $name = null,
            $addresses = null,
            $brokers = null) {
        
        if(!is_null($guid)){
            $this->guid = $guid;
        }
        
        if(!is_null($name)){
            $this->name = $name;
        }
        
        if(!is_null($addresses)){
            $this->addresses = $addresses;
        }
        
        if(!is_null($brokers)){
            $this->brokers = $brokers;
        }
        
        parent::__construct();
    }
}
