<?php
require_once 'Model.php';
class Video extends Model{
    public $location;
    
    public function __construct(
            $guid = null,
            $location = null
            ) {
        if(!is_null($guid)){
            $this->guid = $guid;
        }
        if(!is_null($location)){
            $this->location = $location;
        }
        
        parent::__construct();
    }
}
