<?php

/**
 * Description of StellarPDO
 * This class is designed to be an extension to PDO for use within Stellar 
 * Studios projects, primarily adding useful functions that users will be used
 * to having from StellarMySQLi
 *
 * @author Barry Hylton <blhylton@blhylton.com>
 */
class StellarPDO extends PDO{
    /**
     * Queries and Fetch Associative Array
     * 
     * Runs the specified query and returns an array with the relative column
     * names set as the key in the key value pairs.  Returns false on error.
     * 
     * @author Barry Hylton <blhylton@blhylton.com>
     * @param string $query The query to be run
     * @return array|bool Returns the associative array with database values.
     * Returns FALSE on error.
     */    
    function query_and_fetch_assoc($query)
    {
        try{
            $results = $this->query($query);
            
            //Fetch returns false when no rows left, so if there are no rows it
            //will be false.
            return $results->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $ex) {
            return FALSE;
        }
    }
    
    /**
     * Insert Array Into Database Table
     * 
     * Takes a two dimensional array and inserts it into a specified table.
     * 
     * @author Barry Hylton <blhylton@blhylton.com>
     * @param string $table The table into which you want the data inserted
     * @param array $data Two-dimensional array of the data to be inserted, with
     * the associated keys being the column names.
     * @param array $exclude Keys in the array to exclude from the SQL query
     * @return bool
     */

    function insert_array($table, $data, $exclude = array())
    {
        $keys = array();
        $params = array();
        foreach ($data[0] as $key=>$value)
        {
            $keys[] = $key;
        }
        
        $params = $keys;
        
        implode(", ", $keys);
        implode(', :', $params);
        $params = ':' . $params;
        $query = "INSERT INTO `$table` ($keys) VALUES ($params)";
        
        $result = $this->prepare($query);
        
        foreach($data as $datum)
        {
            $result->execute($datum);
        }
    }
    
    /**
     * Try to Insert Array Into Database Table
     * 
     * Takes a two dimensional array and inserts it into a specified table.
     * Returns true on success and false on error.
     * 
     * @author Barry Hylton <blhylton@blhylton.com>
     * @param string $table The table into which you want the data inserted
     * @param array $data Two-dimensional array of the data to be inserted, with
     * the associated keys being the column names.
     * @param array $exclude Keys in the array to exclude from the SQL query
     */
    function try_insert_array($table, $data, $exclude = array())
    {
        try{
            insert_array($table, $data, $exclude);
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }
}

?>
