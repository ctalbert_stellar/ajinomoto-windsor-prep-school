<?php
require_once 'TblPersons.php';
require_once 'TblCompanies.php';
require_once 'TblAddresses.php';
require_once 'TblQuestions.php';
require_once 'TblQuestionPools.php';
require_once 'TblCourses.php';
require_once 'TblVideos.php';
require_once 'TblCategories.php';
require_once 'TblResources.php';
require_once 'TblSlides.php';
require_once 'TblSlideshows.php';
require_once 'TblCourseHistories.php';
require_once 'TblQuestionHistories.php';
require_once 'TblUsers.php';
class dbContext implements ArrayAccess{
    public $container = array();
    
    public function __construct(&$db) {
        $this->container['Persons'] = new TblPersons($db, $this);
        $this->container['Companies'] = new TblCompanies($db, $this);
        $this->container['Addresses'] = new TblAddresses($db, $this);
        $this->container['Questions'] = new TblQuestions($db, $this);
        $this->container['QuestionPools'] = new TblQuestionPools($db, $this);
        $this->container['Courses'] = new TblCourses($db, $this);
        $this->container['Videos'] = new TblVideos($db, $this);
        $this->container['Categories'] = new TblCategories($db, $this);
        $this->container['Resources'] = new TblResources($db, $this);
        $this->container['Slides'] = new TblSlides($db, $this);
        $this->container['Slideshows'] = new TblSlideshows($db, $this);
        $this->container['CourseHistories'] = new TblCourseHistories($db, $this);
        $this->container['QuestionHistories'] = new TblQuestionHistories($db, $this);
        $this->container['Users'] = new TblUsers($db, $this);
    }
    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }
    public function offsetExists($offset) {
        return isset($this->container[$offset]);
    }
    public function offsetUnset($offset) {
        unset($this->container[$offset]);
    }
    public function offsetGet($offset) {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }
}
