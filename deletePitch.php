<?php
if(!isset($_GET['slideshow'])){
    header("Location: pitchBuilderOverview.php");
    exit();
}
//Start the system
include_once('init.php');

include 'loginCheck.php';

$slideshow = array_values($dbContext['Slideshows']->find($_GET['slideshow']))[0];

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    $slideshow = array_values($dbContext['Slideshows']->find($_POST['slideshow']))[0];
    $dbContext['Slideshows']->delete($slideshow);
    $dbContext['Slideshows']->save();
    header("Location: pitchBuilderOverview.php");
    exit();
}
$title = "Delete Pitch";
$pageName = 'deletePitch';
//Include HTML head
include_once('head.php');
//Include page header
include_once('header.php');
?>
<section id="main">
    <?php include 'nav.php'?>
    <div class="content-padding">
        <h3>Are you sure you want to delete this pitch?</h3>
        <ul style="font-size: 22px;">
            <li>Title: <?=$slideshow->title?></li>
            <li>Last Updated: <?=$slideshow->updated->format('d/m/Y')?></li>
            <li>Number of Slides: <?=count($slideshow->slides)?>
        </ul>
        <form method="post">
            <div class='button-wrapper'>
                <input type="submit" class='button blue' value="Delete">
            </div>
            <input type="hidden" name="slideshow" value="<?=$slideshow->guid?>">
            <div class="button-wrapper">
                <a class='button blue' href="pitchBuilderOverview.php">Cancel</a>
            </div>
        </form>
    </div>
</section>
<?php include 'footer.php'?>