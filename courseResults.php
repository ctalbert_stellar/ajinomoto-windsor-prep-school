<?php
//Start the system
include_once('init.php');

include 'loginCheck.php';

$title = "Course Questions";
$pageName = 'courseQuestions';
$pageTitle = 'Sales Coach';
$pageIcon = 'images/sales-coach.png';;

$course = $_SESSION['course'];
$questionHistories = $_SESSION['questionHistories'];

$_SESSION['questionHistories'] = array();
$_SESSION['questionProgress'] = 1;
$_SESSION['questions'] = $course->getQuestions();

$courseHistory = new CourseHistory(null,
        $course->title,
        $course->categories,
        $course->questionPool,
        $course->numberOfQuestions,
        $course->videos,
        date('Y-m-d h:i:s'),
        $user->person,
        null,
        $questionHistories,
        $course->guid);

foreach($questionHistories as $questionHistory){
    $questionHistory->courseHistory = $courseHistory->guid;
    $dbContext['QuestionHistories']->addOrEdit($questionHistory);
}
$dbContext['QuestionHistories']->save();

$grade = $courseHistory->getGrade();

$dbContext['CourseHistories']->addOrEdit($courseHistory);
$dbContext['CourseHistories']->save();

if($grade >= 70){
    $relatedResources = $dbContext['Resources']->find(array('unlockedBy' => $course->guid));
    $user->person->resources = array_merge($user->person->resources, $relatedResources); 
}
$user->person->courseHistories[] = $courseHistory;
$dbContext['Users']->addOrEdit($user);
$dbContext['Users']->save();
        //Include HTML head
        include_once('head.php');
?>
<section id='main'>
    <div class="modal-wrapper">
        <div class="large-modal">
            <h2>Your Individual Answer Results:
                <span class="number-correct"><?=round(($grade/100)*$course->numberOfQuestions)?>
                    of <?=$course->numberOfQuestions?> Correct</span>
                <img src="images/close-button.gif" class="close-button">
            </h2>

            <div class="modal-question-wrapper">
                <!-- <?php echo(json_encode($questionHistories));?> -->
            <?php foreach($questionHistories as $question):?>
                <div class="question-results clearfix <?=$question->correct?"green":"red"?>">
                    <div class="column left">
                        <p><?=$question->questionText?></p>
                        <h4><span class="label">Your Answer:</span> <?= $question->answers ?></h4>
                    </div>
                    <div class="column right">
                        <h3><?=$question->correct?"Correct":"Incorrect"?></h3>
                    </div>
                </div>
            <?php endforeach;?>
            </div>
        </div>
    </div>
    <div id="site-wrapper">
        <?php
            //Include page header
            include_once('header.php');
        ?>
        <div id="nav-dropdown">
            <?php include 'nav.php';?>
        </div>
        <h2 class="header">Your Score:</h2>
        <hr class="course-border">
            <?php if($grade >= 70):?>
            <div class='center full-width'>
                <h2 class="green score"><?=round($grade)?>%</h2>
                <h2 class='results-header'>Congratulations, You have passed this course!</h2>
            </div>
            <p class='results-content'>It is our hope that, by watching the preceding video and taking the related quiz, you learned more about Ajinomoto Windsor Foods products, and that those facts will be useful to you in your future sales pitches.</p>
            <hr class="course-border">
            <span class='next-step'>What would you like to do now?</span>
            <div class="button-wrapper">
                <a href='course.php?<?=http_build_query(array('course' => $course->guid))?>' class='button'>Retake Course</a>
            </div>
            <div class='button-wrapper'>
                <a href='courseOverview.php' class='button'>Start Another Course</a>
            </div>
            <?php else:?>
            <div class='center full-width'>
                <h2 class='red score'><?=round($grade)?>%</h2>
                <h2 class='results-header'>Sorry... You have failed this course!</h2>
            </div>
            <p class='results-content'>But don’t despair! You have the option to rewatch the video or retake the test below. If you rewatch the video, please pay close attention to the details and read each test question carefully before answering. Good luck!</p>
            <hr class='course-border'>
            <span class='next-step'>What would you like to do now?</span>
            <div class="button-wrapper">
                <a href='course.php?<?=http_build_query(array('course' => $course->guid))?>' class='button'>Retake Course</a>
            </div>
            <div class="button-wrapper">
                <a href='courseOverview.php' class='button'>Take Another Course</a>
            </div>
            <?php endif;?>
        </div>
    </div>
</section>
<?php include 'footer.php';?>
