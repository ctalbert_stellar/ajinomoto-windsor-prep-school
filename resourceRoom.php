<?php
//Start the system
include_once('init.php');

$title = "Resource Room";
$pageName = "resourceRoom";
$pageTitle = "Resource Room";
$pageIcon = "images/resource-room.png";

//Check for login Session
include 'loginCheck.php';

$resources = $dbContext['Resources']->getAll();

$scripts = '<script type="text/javascript" src="scripts/lib/jquery.knob.js"></script>';
//Include HTML head
include_once('head.php');
?>

<section id="main">
    <div id="site-wrapper">
        <?php
        //Include page header
        include_once('header.php');
        ?>
        <div id="nav-dropdown">
            <?php include 'nav.php';?>
        </div>
        <div class='top-boxes-wrapper'>
            <div id='progress-box'>
                <div id="progress">
                    <h2>Your Progress</h2>
                    <div style="display: inline; width: 160px; height: 160px;"><input class="knob" data-fgcolor='#fff' data-bgcolor='rgba(255,255,255,0.2)' value="<?=((count($user->person->resources)/count($resources)) * 100)?>"></div>
                </div>
                 <div id='progress-box-footer'>
                    <h2><?=count($user->person->resources)?> of <?=count($resources)?></h2>
                    Resources Unlocked
                </div>
            </div>
            <div id='resource-box'>
                <div class="cycle-slideshow"
                     data-cycle-fx="scrollHorz"
                     data-cycle-timeout="6000"
                     data-cycle-slides=".cycle-slide"
                     data-cycle-pause-on-hover="true"
                     data-cycle-overlay-template='<a href="{{slide}}">Download This Resource</a>'>
                    <div class="cycle-overlay custom"></div>
                    <div class='cycle-prev'></div>
                    <div class='cycle-next'></div>
                    <?php foreach($user->person->resources as $resource):?>
                    <div class="cycle-slide" data-slide="<?=$resource->location?>" style="background-image: url('<?=$resource->preview?>'); background-size: cover; background-position: center center;">
                        &nbsp;
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
        
        <div id='bottom-boxes-wrapper'>
            <div id='windsor-brands'>
                <h3>Windsor Brands & Websites</h3>
                <div class='brand-box'>
                    <a href="http://bernardifoods.com" target="_blank"><img src="images/brandlogos/bernardi-foods.png"><br/>
                    bernardifoods.com</a>
                </div>
                <div class='brand-box'>
                    <a href="http://joseole.com" target="_blank"><img src="images/brandlogos/jose-ole.png"><br/>
                    joseole.com</a>
                </div>
                <div class='brand-box'>
                    <a href="http://vipfoods.com" target="_blank"><img src="images/brandlogos/vip.png"><br/>
                    vipfoods.com</a>
                </div>
                <div class='brand-box'>
                    <a href="http://theoriginalchilibowlfoodservice.com" target="_blank"><img src="images/brandlogos/the-original-chili-bowl.png"><br/>
                    theoriginalchilibowl<br />foodservice.com</a>
                </div>
                <div class='brand-box'>
                    <a href="http://ling-ling.com" target="_blank"><img src="images/brandlogos/ling-ling.png"><br/>
                    ling-ling.com</a>
                </div>
                <div class='brand-box'>
                    <a href="http://theoriginalchilibowlfoodservice.com" target="_blank"><img src="images/brandlogos/whiteys.png"><br/>
                    theoriginalchilibowl<br />foodservice.com</a>
                </div>
                <div class='brand-box'>
                    <a href="http://fredsforstarters.com" target="_blank"><img src="images/brandlogos/freds-for-starters.png"><br/>
                    fredsforstarters.com</a>
                </div>
                <div class='brand-box'>
                    <a href="http://posadafoodservice.com" target="_blank"><img src="images/brandlogos/posada.png"><br/>
                    posadafoodservice.com</a>
                </div>
                <div class='brand-box'>
                    <a href="http://goldentigerfoodservice.com" target="_blank"><img src="images/brandlogos/golden-tiger.png"><br/>
                    goldentiger<br />foodservice.com</a>
                </div>
                <div class='brand-box'>
                    <a href="http://taipeifood.com" target="_blank"><img src="images/brandlogos/tai-pai.png"><br/>
                    taipeifood.com</a>
                </div>
                <div class='brand-box'>
                    <a href="http://windsorfoods.com" target="_blank"><img src="images/windsor-logo.png"><br/>
                    windsorfoods.com</a>
                </div>
            </div>
            <div id="windsor-apps">
                <h3>Windsor Tools</h3>
                <a href="http://extranet.windsorfoods.com">extranet.windsorfoods.com</a><br/>
                <a href="http://windsorfoodsposonline.com">windsorfoodsposonline.com</a>
                <div class="app-box clearfix">
                    <div class="left">
                        <h4>Food Cost Calculator</h4>
                        Calculate Food Costs<br/>
                        Show Profits
                    </div>
                    <div class="right">
                        <a href="https://itunes.apple.com/us/app/windsor-food-cost-calculator/id540466103?mt=8" target="_blank">
                            <img src="images/Download_on_the_App_Store_Badge_US-UK_135x40.png">            
                        </a>
                        <a href="https://play.google.com/store/apps/details?id=com.stellarstudios.windsor.food_cost_calculator" target="_blank">
                            <img src="images/playstoreIcon.png">
                        </a>
                    </div>
                </div>
                <div class="app-box clearfix">
                    <div class="left">
                        <h4>Wheelhouse</h4>
                        POS Material<br/>
                        Sales sheets, videos, etc.
                    </div>
                    <div class="right">
                        <a href="https://itunes.apple.com/us/app/windsor-food-cost-calculator/id540466103?mt=8" target="_blank">
                            <img src="images/Download_on_the_App_Store_Badge_US-UK_135x40.png">            
                        </a>
                        <a href="https://play.google.com/store/apps/details?id=com.stellarstudios.windsor.food_cost_calculator" target="_blank">
                            <img src="images/playstoreIcon.png">
                        </a>
                    </div>
                </div>
                <div class="app-box clearfix">
                    <div class="left">
                        <h4>PitchViewer</h4>
                        View your saved pitches
                        on your mobile device
                        and have available offline
                    </div>
                    <div class="right">
                        <div class="coming-soon">Coming Soon!</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include_once('footer.php');