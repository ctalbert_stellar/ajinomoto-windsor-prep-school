<?php
$courseGUID = filter_input(INPUT_GET, 'course', FILTER_SANITIZE_STRING);

if(!isset($courseGUID) || empty($courseGUID) || $courseGUID === FALSE){
    header("Location: courseOverview.php");
    exit();
}

//Start the system
include_once('init.php');

include 'loginCheck.php';

$title = "Course Questions";
$pageName = 'courseQuestions';
$pageTitle = 'Sales Coach';
$pageIcon = 'images/sales-coach.png';


if(!isset($_SESSION['course'])){
    $course = array_values($dbContext['Courses']->find($courseGUID))[0];
    $_SESSION['course'] = $course;
    $_SESSION['questions'] = $course->getQuestions();
    $_SESSION['questionProgress'] = 1;
}

$course = $_SESSION['course'];
$questions = $_SESSION['questions'];
$question = array_values($questions)[$_SESSION['questionProgress'] - 1];
if($_SESSION['questionProgress'] === 1){
    $_SESSION['questionHistories'] = array();
}
if($_SERVER['REQUEST_METHOD'] === 'POST'){
    $questionHistory = new QuestionHistory();
    $questionHistory->questionText = $question->questionText;
    $questionHistory->potentialAnswers = $question->potentialAnswers;
    $questionHistory->correctAnswers = $question->correctAnswers;
    if(is_array($_POST['answers'])){
        $questionHistory->correct = $question->checkAnswers($_POST['answers']);
    }else{
        $questionHistory->correct = $question->checkAnswers(array($_POST['answers']));
    }
    $questionHistory->answers = $_POST['answers'];
    $questionHistory->question = $question->guid;
    $dbContext['QuestionHistories']->addOrEdit($questionHistory);
    $dbContext['QuestionHistories']->save();
    $_SESSION['questionHistories'][] = $questionHistory;
    $_SESSION['questionProgress']++;
    if($_SESSION['questionProgress'] > count($questions)){
        header("Location: courseResults.php");
        exit();
    }
    $question = array_values($questions)[$_SESSION['questionProgress'] - 1];
}

$alphabet = range('A', 'Z');

//Include HTML head
            include_once('head.php');
?>
<form method='post'>
    <section id='main'>
        <div id="site-wrapper">
            <?php
            //Include page header
            include_once('header.php');
            ?>
            <div id="nav-dropdown">
                 <?php include 'nav.php';?>
            </div>
            <h2 class="header"><?=$course->title?>: Question <?=$_SESSION['questionProgress']?> of <?=count($questions)?></h2>
            <hr class="course-border">
            <p class="question"><?=$question->questionText?></p>
            <div class="answers-wrapper">
            <?php
            $idx = 0;
            foreach($question->potentialAnswers as $answer):?>
            <div class="answer"><span style="display: inline-block; width: 35px;"><?=$alphabet[$idx];?>.</span><input type="radio" name="answers" id="<?=$answer?>" value="<?=$answer?>"><label for="<?=$answer?>"><span><span></span></span><?=$answer?></label></div>
            <?php
            $idx++;
            endforeach;?>
            </div>
            <hr class="course-border">
            <input type="submit" id="submit" class="bottom-submit" value="Submit Answer">
        </div>
    </section>
</form>
<script type="text/javascript">
    window.onbeforeunload = confirmExit;
    function confirmExit(){
        return "Are you sure you want to leave this course? All progress will be lost.";
    }
    
    document.getElementById('submit').onclick = function(){
        window.onbeforeunload = null;  
    };
</script>

<?php include 'footer.php';?>