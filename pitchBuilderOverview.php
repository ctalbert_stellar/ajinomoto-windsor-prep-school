<?php
//Start the system
include_once('init.php');

$title = "Pitch Builder Overview";
$pageName = "pitchBuilderOverview";
$pageTitle = "Pitch Builder";
$pageIcon = "images/pitch-builder.png";

//Check for login Session
include 'loginCheck.php';

$person = $user->person;

$script = '';
if($_SERVER['REQUEST_METHOD'] === 'POST'){
    switch($_POST['type']){
        case 'create':
            $pitchTitle = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            if(isset($pitchTitle) && !empty($pitchTitle) && $pitchTitle !== FALSE){
                $slideshow = new Slideshow();
                $slideshow->title = $pitchTitle;
                $slideshow->person = $person->guid;
                $dbContext['Slideshows']->addOrEdit($slideshow);
                $dbContext['Slideshows']->save();
                $locationString = "Location: pitchBuilder.php?" . http_build_query(array('slideshow' => $slideshow->guid));
                header($locationString);
                exit();
            }else{
                $script = '<script type="text/javascript">alert("There was an error with your pitch title.  Please try again");</script>';
            }
            break;
        case 'copy':
            $origSlideshow = array_values($dbContext['Slideshows']->find(urldecode(filter_input(INPUT_POST, 'guid', FILTER_SANITIZE_STRING))));
            $pitchTitle = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
            
            if(isset($pitchTitle) && !empty($pitchTitle) && $pitchTitle !== FALSE){
                $slideshow = new Slideshow();
                $slideshow->title = $pitchTitle;
                $slideshow->person = $person->guid;
                foreach($origSlideshow->slides as $slide){
                    $slideshow->slides[] = new Slide(
                            null,
                            $slide->type,
                            $slide->mimeType,
                            $slide->location,
                            $slide->categories,
                            $slideshow->guid,
                            $slide->order,
                            $slide->previewImage,
                            $slide->resource
                            );
                }
                $dbContext['Slideshows']->addOrEdit($slideshow);
                $dbContext['Slideshows']->save();
                $locationString = "Location: pitchBuilder.php?" . http_build_query(array('slideshow' => $slideshow->guid));
                header($locationString);
                exit();
            }else{
                $script = '<script type="text/javascript">alert("There was an error with your pitch title.  Please try again");</script>';
            }
            break;
        case 'rename':
            $slideshow = array_values($dbContext['Slideshows']->find(urldecode(filter_input(INPUT_POST, 'guid', FILTER_SANITIZE_STRING))))[0];

            $pitchTitle = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);

            if(isset($pitchTitle) && !empty($pitchTitle) && $pitchTitle !== FALSE){
                $slideshow->title = $pitchTitle;
                $dbContext['Slideshows']->addOrEdit($slideshow);
                $dbContext['Slideshows']->save();
                header("Location: pitchBuilderOverview.php");
                exit();
            }else{
                $script = '<script type="text/javascript">alert("There was an error with your pitch title.  Please try again");</script>';
            }
            break;
        case 'delete':
            $slideshow = array_values($dbContext['Slideshows']->find(urldecode(filter_input(INPUT_POST, 'guid', FILTER_SANITIZE_STRING))))[0];
            $dbContext['Slideshows']->delete($slideshow);
            $dbContext['Slideshows']->save();
            header("Location: pitchBuilderOverview.php");
            exit();
            break;
    }
}
?>
<?php //Include HTML head
        include_once('head.php');?>
<section id="main">
    <div id='site-wrapper'>
        <?php
        //Include page header
        include_once('header.php');
        ?>
        <div id='nav-dropdown'>
            <?php include 'nav.php';?>
        </div>
        <div id="slideshow-list">
            <a href='#' id="create-link">
                <div class='box'>
                    <div class="box-contents">
                        <img src="images/add-large.png">
                    </div>
                    <div class="box-footer">
                        <span class="link-like wide">
                            <img src="images/new-pitch.png"><br />
                            Create New Pitch
                        </span>
                    </div>
                </div>
            </a>
            <?php if(count($person->slideshows) > 0): $i = 3;?>
                <?php foreach($person->slideshows as $slideshow):?>
                    <div class="box<?=($i%3 === 0)?' margin':''?>">
                        <div class="box-contents">
                            <div class='date'><?=$slideshow->updated->format('m/d/Y')?></div>
                            <div class="title"><?=$slideshow->title?></div>
                        </div>
                        <div class="box-footer">
                            <a href="pitchBuilder.php?<?= http_build_query(array("slideshow" => $slideshow->guid))?>" target="_blank"><img src='images/edit-pitch.png'><br />Edit</a>
                            <span class="link-like rename" data-slideshow="<?=urlencode($slideshow->guid)?>" data-title="<?=$slideshow->title?>"><img src='images/rename-pitch.png'> <br />Rename</span>
                            <span class="link-like copy" data-slideshow="<?=urlencode($slideshow->guid)?>" data-title="<?=$slideshow->title?> Copy"><img src='images/copy-pitch.png'><br />Duplicate</span>
                            <span class="link-like delete" data-slideshow="<?=urlencode($slideshow->guid)?>" data-title="<?=$slideshow->title?>" data-updated="<?=$slideshow->updated->format("m/d/Y")?>" data-slides="<?=count($slideshow->slides)?>"><img src='images/delete-pitch.png'> <br />Delete</span>
                        </div>
                    </div>
                <?php
                $i++;
                endforeach;?>
            <?php endif;?>
        </div>
    </div>
</section>
<div id="modal-wrapper">
    <div id="create" class="modal clearfix">
        <?php if(count($person->resources) > 0):?>
            <h3>Create New Pitch</h3>
            <p>Enter the name of your new pitch below and submit to get started.  Once you've entered a valid name and pressed the "Get Started" button, the Prep School Pitch Builder will open with all your available resources.</p>
            <form id="form-create" method="post" target="_blank">
                <input name="title" type="text">
                <input type="submit" value="Get Started">
                <input type="hidden" name="type" value="create">
                <button class="cancel">Cancel</button>
            </form>
        <?php else:?>
            <h3>No Resources Unlocked</h3>
            <p>You'll have to utilize the <a href="courseLanding.php">Sales Coach</a> and unlock some resources before you can start building a pitch. </p>
        <?php endif;?>
    </div>
    <div id="copy" class="modal clearfix">
        <h3>Duplicate Pitch</h3>
        <p>Decided to save some time and reuse part of an old slideshow? Great! Enter the name of your new pitch and we'll send you to the editor so you can make any changes needed.</p>
        <form id="form-copy" method="post" target="_blank">
            <input name="title" type="text">
            <input type="submit" value="Duplicate Pitch">
            <input type="hidden" name="type" value="copy">
            <input type="hidden" name="guid" value="">
            <button class="cancel">Cancel</button>
        </form>
    </div>
    <div id="rename" class="modal clearfix">
        <h3>Rename Pitch</h3>
        <p>Decided you didn't like the name of your pitch? Enter your new name below and click "Update Name" to save your changes.</p>
        <form id="form-rename" method="post">
            <input name="title" type="text">
            <input type="submit" value="Update Name">
            <button class="cancel">Cancel</button>
            <input type="hidden" name="type" value="rename">
            <input type="hidden" name="guid" value="">
        </form>
    </div>
    <div id="delete" class="modal clearfix">
        <h3>Delete Pitch</h3>
        <p>Are you sure you want to delete this pitch?</p>
        <ul id="delete-info">

        </ul>
        <form id="form-delete" method="post">
            <input type="submit" value="Delete Pitch">
            <button class="cancel">Cancel</button>
            <input type="hidden" name="type" value="delete">
            <input type="hidden" name="guid">
        </form>
    </div>
</div>
<?=$script?>
<?php include_once('footer.php');