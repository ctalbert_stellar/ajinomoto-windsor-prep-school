<?php

function slidesAction(array &$response, array $params, dbContext $dbContext){
    $identifier = $_GET['identifier'];
    $slideGUID = $params[0];
    $slideDB = $dbContext['Slides']->find('{'.$slideGUID.'}');
    if(!is_array($slideDB)){
        throw new Exception("Slide not found", 404);
    }
    $slide = array_values($slideDB)[0];
    
    if(is_null($slide)){
        throw new Exception("Slide not found", 500);
    }
    
    $response['guid'] = trim($slide->guid, '{}');
    $response['type'] = $slide->type;
    $response['mimetype'] = $slide->mimeType;
    $response['url'] = $slide->location;
    if($slide->type === "Video"){
        $response['preview'] = base64_encode(file_get_contents($slide->previewImage));
    }
}

/**
 * /slides/:guid?identifier=:userGUID - Get using this url returns the structure below.
 * {
 *  "guid": "E18F6CCB-424F-4F1B-8175-DA2B193DD3F8",
 *  "type": "image",
 *  "mimetype": "image/png",
 *  "url": "http://192.168.13.149:8000/resources/slides/E18F6CCB-424F-4F1B-8175-DA2B193DD3F8.png",
 *  "error": false
 * }
 */