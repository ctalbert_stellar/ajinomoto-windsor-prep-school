<?php
//Buffer output to be discarded before json encode
ob_start();
//Load Models
//TODO: Load Model Definitions Here

//HTTPStatusCodeEnum
require_once('HTTPStatusCode.class.php');

//Load API Functions
require_once 'api-functions.php';

require_once '../init.php';




$response = array(
    'error' => FALSE,
    'message' => ''
);

//Database Connection
/*try{
    $db = new PDO('mysql:host=localhost;dbname=wheelhouse;charset=utf8', 'root', 'c9fC3WTzKA3aJP4', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
}catch (Exception $ex){
    $response['error'] = TRUE;
    $response['message'] = $ex->getMessage();
    ob_end_clean();
    echo json_encode($response);
    http_response_code(HTTPStatusCode::INTERNAL_SERVER_ERROR);
    exit();
}*/

//API Actions
require_once 'action-login.php';
require_once 'action-slideshows.php';
require_once 'action-slides.php';

$serverRequestURI = explode('?', $_SERVER['REQUEST_URI']);
$serverRequestURI = $serverRequestURI[0];

//Your URI will always start with a /, the 1 in array slice lops that first element off the exploded URI.
//Increment the number if your api is in a folder. ex: /api/login using 2 would trim the api bit as well.
$request = array_slice(explode('/', $serverRequestURI), 2);

$action = $request[0] . 'Action';
$params = array_slice($request, 1);

var_dump($params);

try{
    if(function_exists($action)){
        $action($response, $params, $dbContext);
    }else{
        throw new Exception('Not Implemented: ' . $serverRequestURI, HTTPStatusCode::INTERNAL_SERVER_ERROR);
    }
}catch (Exception $ex){
    $response['error'] = TRUE;
    $response['message'] = $ex->getMessage();

    /*  If an error code came in, use that as the
           http status code, otherwise throw Internal Server Error */
    $response['statusCode'] = $ex->getCode() ? $ex->getCode() : HTTPStatusCode::INTERNAL_SERVER_ERROR;
}

http_response_code(isset($response['statusCode']) && !empty($response['statusCode']) ? $response['statusCode'] : HTTPStatusCode::OK);
ob_end_clean();
header("Content-type: application/json");
echo json_encode($response);
