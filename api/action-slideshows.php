<?php

function slideshowsAction(array &$response, array $params, dbContext $dbContext){
    $identifier = $_GET['identifier'];
    
    if(isset($params[0]) && !empty($params[0])){
        //A specific Slideshow was requested.
        $slideshowId = $params[0];
        $slideshow = array_values($dbContext['Slideshows']->find('{'.$slideshowId.'}'))[0];
        $user = array_values($dbContext['Users']->find('{'.$identifier.'}'))[0];
        if(!isset($slideshow)){
            throw new Exception("Slideshow doesn't exist", 404);
        }
        
        if(!isset($user)){
            throw new Exception("Invalid user", 417);
        }
        $response['guid'] = trim($slideshow->guid, '{}');
        $response['name'] = html_entity_decode($slideshow->title, ENT_QUOTES);
        $response['owner'] = trim($user->guid, '{}');
        $response['slides'] = array();
        
        foreach($slideshow->slides as $slide){
            $response['slides'][] = trim($slide->guid, '{}');
        }
    }else{
        //A list of all this user's slideshows was passed.
        $users = $dbContext['Users']->find('{' . $identifier . '}');
        $user = null;
        if(count($users) === 0){
            throw new Exception("Unknown User", 404);
        }else{
            $user = array_values($users)[0];
        }
        $response['slideshows'] = array();
        foreach($user->person->slideshows as $slideshow){
            $response['slideshows'][] = array(
                'guid' => trim($slideshow->guid, '{}'),
                'name' => html_entity_decode($slideshow->title, ENT_QUOTES),
                'preview' => array(
                    'guid' => $slideshow->slides[0]->guid,
                    'type' => "Image",
                    'mimetype' => (substr($slideshow->slides[0]->mimetype, 0, 5) === 'video'? $slideshow->slides[0]->mimetype: 'image/jpg'),
                    'url' => $slideshow->slides[0]->previewImage,
                    'base64img' => base64_encode(file_get_contents($slideshow->slides[0]->previewImage))
                )
            );
        }
    }
}

/**
 * /slideshows/:guid?identifier=:userGUID - Retrieve a specific slide show
 * {
 *   "guid": "D06473CE-A670-4448-99AA-C92F7F360D06",
 *   "name": "Slideshow 1",
 *   "owner": "0e1ad868-12a4-4f06-a6de-9edd08c9ce42",
 *   "slides": [
 *     "E18F6CCB-424F-4F1B-8175-DA2B193DD3F8",
 *     "F8BFBF96-FB5E-4FD0-9763-C2BC2891F57B",
 *     "12F3FC8B-F8D8-49B6-B234-FEF1A841DD26",
 *     "268B23F7-1743-46EE-A5EF-3127CB07D5DE",
 *     "90F9E01A-D25D-4A10-A375-259CA9C2BCCE"
 *   ],
 *   "error": false
 * }
 */

/**
 * /slideshows?identifier=:userGUID - Retrieve a list of all slideshows
 * {
 *   "error": false,
 *   "slideshows": [
 *     {
 *       "guid": "D06473CE-A670-4448-99AA-C92F7F360D06",
 *       "name": "Slideshow 1",
 *       "preview": {
 *         "guid": "E18F6CCB-424F-4F1B-8175-DA2B193DD3F8",
 *         "type": "image",
 *         "mimetype": "image/png",
 *         "url": "http://192.168.13.149:8000/resources/slides/E18F6CCB-424F-4F1B-8175-DA2B193DD3F8.png",
 *         "base64img": ""
 *       }
 *     },
 *     {
 *       "guid": "A0B8300D-5BA8-4200-BB9B-EA30819D0264",
 *       "name": "Slideshow 2",
 *       "preview": {
 *         "guid": "52016496-CE85-433A-8E9C-D5F6110063F0",
 *         "type": "image",
 *         "mimetype": "image/png",
 *         "url": "http://192.168.13.149:8000/resources/slides/52016496-CE85-433A-8E9C-D5F6110063F0.png",
 *         "base64img": ""
 *       }
 *     },
 *     {
 *       "guid": "635DC5E7-E720-42A9-932E-D144DD3C7D8A",
 *       "name": "Slideshow 3",
 *       "preview": {
 *         "guid": "EF7C97EB-F3FB-413B-A716-BA2F535B6D13",
 *         "type": "image",
 *         "mimetype": "image/png",
 *         "url": "http://192.168.13.149:8000/resources/slides/EF7C97EB-F3FB-413B-A716-BA2F535B6D13.png",
 *         "base64img": ""
 *       }
 *     },
 *     {
 *       "guid": "BFDDC5AE-62B7-4DED-BEA1-EA43BCA9A388",
 *       "name": "Slideshow 4",
 *       "preview": {
 *         "guid": "E18F6CCB-424F-4F1B-8175-DA2B193DD3F8",
 *         "type": "image",
 *         "mimetype": "image/png",
 *         "url": "http://192.168.13.149:8000/resources/slides/E18F6CCB-424F-4F1B-8175-DA2B193DD3F8.png",
 *         "base64img": ""
 *       }
 *     },
 *     {
 *       "guid": "5922762E-5F34-4A0C-BCB1-A0AA6C3F5FF6",
 *       "name": "Slideshow 5",
 *       "preview": {
 *         "guid": "268B23F7-1743-46EE-A5EF-3127CB07D5DE",
 *         "type": "image",
 *         "mimetype": "image/png",
 *         "url": "http://192.168.13.149:8000/resources/slides/268B23F7-1743-46EE-A5EF-3127CB07D5DE.png",
 *         "base64img": ""
 *       }
 *     },
 *     {
 *       "guid": "3FE058E9-6FDD-43B0-8AF9-63D5D77B9115",
 *       "name": "Slideshow 6",
 *       "preview": {
 *         "guid": "E18F6CCB-424F-4F1B-8175-DA2B193DD3F8",
 *         "type": "image",
 *         "mimetype": "image/png",
 *         "url": "http://192.168.13.149:8000/resources/slides/E18F6CCB-424F-4F1B-8175-DA2B193DD3F8.png",
 *         "base64img": ""
 *       }
 *     },
 *     {
 *       "guid": "5E9BC09B-56D9-4D51-AE91-65D7E996DBB0",
 *       "name": "Slideshow 7"
 *     },
 *     {
 *       "guid": "5E9BC09B-56D9-4D51-AE91-85D7E996DBB0",
 *       "name": "Slideshow 8"
 *     }
 *   ]
 * }
 */