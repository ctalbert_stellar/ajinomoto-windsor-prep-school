<?php

function loginAction(array &$response, array $params, dbContext &$dbContext){
    $request = json_decode(file_get_contents("php://input"));
    $email = $request->email;
    $password = $request->password;
    
    $login = $dbContext['Users']->login($email, $password);
    
    if($login === FALSE){
        throw new Exception("Invalid Login Credentials", 401);
    }else{
        $user = array_values($dbContext['Users']->find(array('email'=>$email)))[0];
        $response['user']['email'] = $user->email;
        $response['user']['guid'] = trim($user->guid, '{}');
    }
}

/**
 * /login - Posted a username and a password, log the user in and return a structure like below
 *
 *   {
 *      "error": false,
 *      "debug": {},
 *      "user": {
 *          "email": "chendrickson@stellarstudios.com",
 *          "guid": "0e1ad868-12a4-4f06-a6de-9edd08c9ce42"
 *      }
 *   }
 */