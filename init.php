<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

//A little autoload magic
function application_autoloader($class) {
    $class = strtolower($class);
    $class_filename = strtolower($class).'.php';
    $class_root = dirname(__FILE__) . '/includes';
    $cache_file = "{$class_root}/cache/classpaths.cache";
    $path_cache = (file_exists($cache_file)) ? unserialize(file_get_contents($cache_file)) : array();
    if (!is_array($path_cache)) { $path_cache = array(); }
 
    if (array_key_exists($class, $path_cache)) {
        /* Load class using path from cache file (if the file still exists) */
        if (file_exists($path_cache[$class])) { require_once $path_cache[$class]; }
 
    } else {
        /* Determine the location of the file within the $class_root and, if found, load and cache it */
        $directories = new RecursiveDirectoryIterator($class_root);
        foreach(new RecursiveIteratorIterator($directories) as $file) {
            if (strtolower($file->getFilename()) == $class_filename) {
                $full_path = $file->getRealPath();
                $path_cache[$class] = $full_path;
                require_once $full_path;
                break;
            }
        }   
 
    }
 
    $serialized_paths = serialize($path_cache);
    if ($serialized_paths != $path_cache) { file_put_contents($cache_file, $serialized_paths); }
}
 
spl_autoload_register('application_autoloader');

//Set the timezone so any date operations work as expected.
date_default_timezone_set(Config::$timezone);

$db = new PDO('mysql:dbname='.Config::$dbSchema.';host='.Config::$dbHost, Config::$dbUser, Config::$dbPass);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


$db->query("SET time_zone = '+5:00'");

//Set IS_DEVELOPER to TRUE or FALSE
define('IS_DEVELOPER', in_array($_SERVER['REMOTE_ADDR'], Config::$developerIPs));

$dbContext = new dbContext($db);

session_start();
if(isset($_SESSION['userLoggedIn'])){
    if($_SESSION['userLoggedIn']){
        $user = array_values($dbContext['Users']->find($_SESSION['userGUID']))[0];
    }
}