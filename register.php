<?php
$pageName = "registration";
require_once 'includes/stateList.php';
require_once 'init.php';
$title = 'Ajinotomo Windsor Prep School Registration';
require_once 'head.php';
require_once 'includes/phpmailer/PHPMailerAutoload.php';


$errors = array();
$user = new User();
$person = new Person();
$company = new Company();
$address = new Address();
$companyAddress = new Address();
$password = "";

$companies = $dbContext['Companies']->getAll();

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    $user->email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
    if(is_null($user->email) || empty($user->email)){
        $errors['email'][] = "Email is required.";
    }elseif(!filter_var($user->email, FILTER_VALIDATE_EMAIL)){
        $errors['email'][] = "Invalid email address.";
    }else{
        $users = $dbContext['Users']->find(array('email' => $user->email));
        if(count($users) > 0){
            $errors['email'][] = "There is already a user with this email. Are you sure you didn't mean to <a href='login.php'>log in</a>?";
        }
    }
    
    $password = filter_input(INPUT_POST, 'password', FILTER_UNSAFE_RAW);
    $passwordConfirmation = filter_input(INPUT_POST, 'passwordConfirm', FILTER_UNSAFE_RAW);
    if(empty($password) || is_null($password)){
        $errors['password'][] = "You must supply a password.";
    }elseif(empty($passwordConfirmation) || is_null($passwordConfirmation)){
        $errors['passwordConfirmation'][] = "You must supply your password again.";
    }elseif($password !== $passwordConfirmation){
        $errors['passwordConfirmation'][] = "Passwords do not match.";
    }else{
        $user->createPassword($password);
    }
    
    $person->firstName = filter_input(INPUT_POST, 'firstName', FILTER_SANITIZE_STRING);
    if(is_null($person->firstName) || empty($person->firstName)){
        $errors['firstName'][] = "Your first name is required.";
    }elseif(!$person->firstName){
        $errors['firstName'][] = "First name is in an invalid format.";
    }
    
    $person->lastName = filter_input(INPUT_POST, 'lastName', FILTER_SANITIZE_STRING);
    if(is_null($person->lastName) || empty($person->lastName)){
        $errors['lastName'][] = "Your last name is required.";
    }elseif(!$person->lastName){
        $errors['lastName'][] = "Last name is in an invalid format.";
    }
    
    $person->phone = filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_STRING);
    if(is_null($person->phone) || empty($person->phone)){
        $errors['lastName'][] = "Your phone number is required.";
    }elseif(!$person->lastName){
        $errors['lastName'][] = "Phone Number is in an invalid format.";
    }
    
    $person->dsm = filter_input(INPUT_POST, 'dsm', FILTER_SANITIZE_STRING);
    if($person->dsm === FALSE){
        $person->dsm = "";
    }
    $person->brokerNumber = filter_input(INPUT_POST, 'brokerNumber', FILTER_SANITIZE_STRING);
    if($person->brokerNumber === FALSE){
        $person->brokerNumber = "";
    }
    
    $address->lineOne = filter_input(INPUT_POST, 'address1', FILTER_SANITIZE_STRING);
    if(is_null($address->lineOne) || empty($address->lineOne)){
        $errors['address1'][] = "Your address is required.";
    }elseif(!$address->lineOne){
        $errors['address1'][] = "Address Line One is in an invalid format.";
    }
    
    $address->lineTwo = filter_input(INPUT_POST, 'address2', FILTER_SANITIZE_STRING);
    if(!$address->lineTwo){
        $address->lineTwo = "";
    }
    
    $address->city = filter_input(INPUT_POST, 'city', FILTER_SANITIZE_STRING);
    if(is_null($address->city) || empty($address->city)){
        $errors['city'][] = "Your city is required.";
    }elseif(!$address->city){
        $errors['city'][] = "City is in an invalid format.";
    }
    
    $address->state = filter_input(INPUT_POST, 'state', FILTER_SANITIZE_STRING);
    if(is_null($address->state) || empty($address->state)){
        $errors['state'][] = "Your state is required.";
    }elseif(!$address->state){
        $errors['state'][] = "State is in an invalid format.";
    }elseif(!in_array(ucwords($address->state), $states) && !array_key_exists(strtoupper($address->state), $states)){
        $errors['state'][] = "Invalid State.";
    }elseif(in_array(ucwords($address->state), $states)){
        $address->state = array_search(ucwords($address->state), $states);
    }
    
    $address->zip = filter_input(INPUT_POST, 'zip', FILTER_SANITIZE_STRING);
    if(is_null($address->zip) || empty($address->zip)){
        $errors['zip'][] = "Your zip code is required.";
    }elseif(!$address->zip){
        $errors['zip'][] = "Zip code is in an invalide format.";
    }
    
    $person->addresses[] = $address;
    
    $co = filter_input(INPUT_POST, 'company', FILTER_SANITIZE_STRING);
    if(is_null($co) || empty($co) || $co === 0){
        $errors['company'][] = "Company is required";
    }else{
        if($co === 'Create'){
            $company = new Company();
            $company->name = filter_input(INPUT_POST, 'companyName');
            if(is_null($company->name) || empty($company->name)){
                $errors['companyName'][] = "Your company name is required.";
            }elseif(!$company->name){
                $errors['companyName'][] = "Company name is in an invalid format.";
            }

            $companyAddress->lineOne = filter_input(INPUT_POST, 'companyAddress1', FILTER_SANITIZE_STRING);
            if(is_null($companyAddress->lineOne) || empty($companyAddress->lineOne)){
                $errors['companyAddress1'][] = "Your companies address is required.";
            }elseif(!$companyAddress->lineOne){
                $errors['companyAddress1'][] = "Company address is in an invalid format.";
            }

            $companyAddress->lineTwo = filter_input(INPUT_POST, 'companyAddress2', FILTER_SANITIZE_STRING);
            if(!$companyAddress->lineTwo){
                $companyAddress->lineTwo = "";
            }

            $companyAddress->city = filter_input(INPUT_POST, 'companyCity', FILTER_SANITIZE_STRING);
            if(is_null($companyAddress->city) || empty($companyAddress->city)){
                $errors['companyCity'][] = "Your companies city is required.";
            }elseif(!$companyAddress->city){
                $errors['companyCity'][] = "Company city is in an invalid format.";
            }

            $companyAddress->state = filter_input(INPUT_POST, 'companyState', FILTER_SANITIZE_STRING);
            if(is_null($companyAddress->state) || empty($companyAddress->state)){
                $errors['companyState'][] = "Your company's state is required.";
            }elseif(!$companyAddress->state){
                $errors['companyState'][] = "Company state is in an invalid format.";
            }elseif(!in_array(ucwords($companyAddress->state), $states) && !array_key_exists(strtoupper ($companyAddress->state), $states)){
                $errors['companyState'][] = "Invalid State.";
            }elseif(in_array(ucwords($companyAddress->state), $states)){
                $companyAddress->state = array_search(ucwords($address->state), $states);
            }

            $companyAddress->zip = filter_input(INPUT_POST, 'zip', FILTER_SANITIZE_STRING);
            if(is_null($companyAddress->zip) || empty($companyAddress->zip)){
                $errors['companyZip'][] = "Your company's zip code is required.";
            }elseif(!$companyAddress->zip){
                $errors['companyZip'][] = "Company zip code is in an invalid format.";
            }

            $company->addresses[] = $companyAddress;
        }else{
            $company = $companies[$co];
        }
    }
    
    $person->company = $company;
    $user->person = $person;
    
    if(count($errors) === 0){
        $dbContext['Users']->addOrEdit($user);
        $dbContext['Users']->save();

        $mail = new PHPMailer;

        $mail->isSMTP();
        $mail->Host = 'mail.ajinomotowindsorprepschool.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'info@ajinomotowindsorprepschool.com';
        $mail->Password = 'letM3know';
        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;

        $mail->From = 'info@ajinomotowindsorprepschool.com';
        $mail->FromName = 'Ajinomoto Windsor Prep School';
        $admins = $dbContext['Users']->find(array('isAdmin' => true));

        foreach($admins as $admin){
            $mail->addAddress($admin->email);
        }

        $mail->Subject = "New Registration for Ajinomoto Windsor Prep School";
        $mail->Body = "You've had a new person register for Ajinomoto Windsor Prep School.  Visit http://ajinomotowindsorprepschool.com to approve them so they can start their training.";

        $mail->send();

        $mail2 = new PHPMailer;

        $mail2->isSMTP();
        $mail2->Host = 'mail.ajinomotowindsorprepschool.com';
        $mail2->SMTPAuth = true;
        $mail2->Username = 'info@ajinomotowindsorprepschool.com';
        $mail2->Password = 'letM3know';
        $mail2->SMTPSecure = 'tls';
        $mail2->Port = 587;

        $mail2->From = 'info@ajinomotowindsorprepschool.com';
        $mail2->FromName = 'Ajinomoto Windsor Prep School';

        $mail2->addAddress($user->email);

        $mail2->Subject = "New Registration for Ajinomoto Windsor Prep School";
        $mail2->Body = "Thanks for registering for Ajinomoto Windsor Prep School. An administrator will need to review your account and approve you before you can begin your training.  You'll receive a follow-up email once you've been approved.";
        
        $_SESSION['Message'] = "Thanks for registering.  You will receive an email confirming your registration shortly and once your account is approved you'll be able to access the site.";
        header('Location:index.php');
        exit();
    }
    
}

?>
<body class="registration">
<section id='main'>
    <div id='registration'>
        <div class='image-wrapper'>
            <img src='images/logo.png' alt='Ajinomoto Windsor Prep School Logo' />
        </div>
        <?php if(count($errors) > 0):?>
        <div class="message error">
            <h3>Unfortunately, there were some errors in your submission.</h3>
            <ul>
            <?php foreach($errors as $key => $error):?>
                <?php foreach($error as $e):?>
                <li><?=$e?></li>
                <?php endforeach;?>
            <?php endforeach;?>
            </ul>
        </div>
        <?php endif;?>
        <form method="post">
            <div class='group space-bottom'>
                <input type='email' name='email' class="textbox-style-1" placeholder='Email' value="<?=$user->email?>" required>
                <input type='password' name='password' class='textbox-style-1' placeholder='Password' required>
                <input type='password' name='passwordConfirm' class='textbox-style-1' placeholder='Confirm Password' required>
                <input type='text' name='firstName' class='textbox-style-1' placeholder='First Name' value="<?=$person->firstName?>" required>
                <input type='text' name='lastName' class='textbox-style-1' placeholder='Last Name' value="<?=$person->lastName?>" required>
                <input type='text' name='phone' class='textbox-style-1' placeholder='Phone' value="<?=$person->phone?>" required>
                <input type='text' name='dsm' class='textbox-style-1' placeholder='District Sales Manager' value="<?=$person->dsm?>">
                <input type='text' name='brokerNumber' class='textbox-style-1' placeholder='Broker Number' value="<?=$person->brokerNumber?>">
            </div>
            <div class='group space-bottom'>
                <input type='text' name='address1' class='textbox-style-1' placeholder='Address Line One' value="<?=$address->lineOne?>" required>
                <input type='text' name='address2' class='textbox-style-1' placeholder='Address Line Two' value="<?=$address->lineTwo?>">
                <input type='text' name='city' class='textbox-style-1' placeholder='City' value="<?=$address->city?>" required>
                <input type='text' name='state' class='textbox-style-1' placeholder='State' value="<?=$address->state?>" required>
                <input type='text' name='zip' class='textbox-style-1' placeholder='Zip Code' value="<?=$address->zip?>" required>
            </div>
            <div class='group space-bottom'>
                <select name="company" id="company">
                    <option value="0">Select Your Company...</option>
                    <?php foreach($companies as $co):?>
                    <option value="<?=$co->guid?>" <?=isset($company->guid)?($company->guid == $co->guid?"selected":""):""?>><?=$co->name?></option>
                    <?php endforeach;?>
                    <option value="Create">Create New Company</option>
                </select>
                <input type='hidden' name='companyName' class='company textbox-style-1' placeholder='Company Name' value="<?=$company->name?>" required>
                <input type='hidden' name='companyAddress1' class='company textbox-style-1' placeholder='Company Address Line One' value="<?=$companyAddress->lineOne?>" required>
                <input type='hidden' name='companyAddress2' class='company textbox-style-1' placeholder='Company Address Line Two' value="<?=$companyAddress->lineTwo?>">
                <input type='hidden' name='companyCity' class='company textbox-style-1' placeholder='Company City' value="<?=$companyAddress->city?>" required>
                <input type='hidden' name='companyState' class='company textbox-style-1' placeholder='Company State' value="<?=$companyAddress->state?>" required>
                <input type='hidden' name='companyZip' class='company textbox-style-1' placeholder='Company Zip Code' value="<?=$companyAddress->zip?>" required>
                <input type='submit' value='Register' class='blue submit'><br />
                <a href="login.php" class="left">Return to Login</a>
            </div>
        </form>
    </div>
</section>
<script type="text/javascript">
    var companies = {};
    <?php foreach($companies as $company):?>
        companies['<?=$company->guid?>'] = {'name': '<?=$company->name?>', 'address1': '<?=array_values($company->addresses)[0]->lineOne?>', 'address2': '<?=isset(array_values($company->addresses)[0]->lineTwo)?array_values($company->addresses)[0]->lineTwo:''?>', 'city': '<?=array_values($company->addresses)[0]->city?>', 'state': '<?=array_values($company->addresses)[0]->state?>', 'zip': '<?=array_values($company->addresses)[0]->zip?>'};
    <?php endforeach;?>
</script>
<?php include_once 'footer.php';?>
</body>

