<?php
//Start the system
include_once('init.php');

include 'loginCheck.php';

$title = "Course Overview";
$pageName = 'courseLanding';
$pageTitle = 'Sales Coach';
$pageIcon = 'images/sales-coach.png';

//Check for login Session
$person = $user->person;

$courses = $dbContext['Courses']->getAllApproved();

$companies = $dbContext['Companies']->getAll();

$passedCourses = 0;
$passedCourseGUIDs = array();
foreach($person->courseHistory as $ch){
    if($ch->grade > 70 && !in_array($ch->courseGUID, $passedCourseGUIDs)){
        $passedCourses++;
        $passedCourseGUIDs[] = $ch->courseGUID;
    }
}

$companyRankings = json_decode(file_get_contents('./cache/companyRankings.json'));
$personRankings = json_decode(file_get_contents('./cache/personRankings.json'));

$scripts = '<script type="text/javascript" src="scripts/lib/jquery.knob.js"></script>';
include_once('head.php');
?>
<section id="main" class="courseLanding">
    <div id="site-wrapper">
        <?php             
            //Include page header
            include_once('header.php');
        ?>
        <div id="nav-dropdown">
            <?php include_once 'nav.php';?>
        </div>
        <div class="header-image">
	        <div id="progress">
                <h2>Your Progress</h2>
                <div style="display: inline; width: 160px; height: 160px;"><input class="knob" data-fgcolor='#fff' data-bgcolor='rgba(255,255,255,0.2)' value="<?=(($passedCourses/count($courses)) * 100)?>"></div>
            </div>
            <img src="images/header-slide.jpg">
            <div class="header-caption">
                <span class="header-stats"><?=$passedCourses?> of <?=count($courses)?> Passed</span>
                <span class="header-link"><a href="courseOverview.php">Start a New Course</a></span>
            </div>
        </div>
        <div class="box intro-videos">
            <div class="box-contents">
                <p class="intro-links"><a href="https://youtu.be/jOdk2Mycvhw">Golden Tiger Intro</a><br>
                <a href="https://youtu.be/xnGBgfTRw48">Bernardi Intro</a><br>
                <a href="https://youtu.be/gG0W1Q1lc-4">Posada Intro</a><br>
                <a href="https://youtu.be/MBYi8ssUO7M">Fred's For Starters Intro</a><br>
                <a href="https://youtu.be/NObUqRfHIaU">Whitey's/OCB Intro</a><br>
                <a href="https://youtu.be/sRhOGXILbfc">Ajinomoto Windsor Ethnic Fusion Intro</a><br>
                <a href="https://youtu.be/HVsC3dZierM">Child Nutrition Intro</a></p>
            </div>
            <div class="box-footer">
                <div class="large-number">Introduction</div>
                Videos
            </div>
        </div>
        <div class="box margin">
            <div class="box-contents">
                <h2>Your Rank Among Users</h2>
                <?php for($i = 0; $i <= 4 && $i < count($personRankings); $i++):?>
                <div class="person-stats-row clearfix opacity-<?=100 - ($i * 10)?>">
                    <div class="user-rank"><span class="rank"><?=$i +1?></span> <?=$personRankings[$i]->name?></div>
                </div>
                <?php endfor;?>
            </div>
            <div class="box-footer">
                <?php 
                $rank = count($personRankings);
                for($i = 0; $i < count($personRankings); $i++){
                    if($personRankings[$i]->guid === $user->person->guid){
                        $rank = $i + 1;
                        break;
                    }
                }
                ?>
                <div class="large-number"><?=$rank?> out of <?=count($personRankings);?></div>
                Users
            </div>
        </div>
        <div class="box">
            <div class="box-contents">
                <h2>Top 5 Companies</h2>
                <?php for($i = 0; $i <= 4 && $i < count($companyRankings); $i++):?>
                <div class="company-stats-row clearfix opacity-<?=100 - ($i * 10)?>">
                    <div class="rank"><?=$i +1?></div>
                    <div class="company-name"><?=$companyRankings[$i]->name?></div>
                    <div class="company-stats-average progress-bar">
                        <div class="bar" style="width: <?=$companyRankings[$i]->average?>%">&nbsp;</div>
                    </div>
                </div>
                <?php endfor;?>
            </div>
            <div class="box-footer">
                <div class="large-number"><?=count($companies)?></div>
                Companies Competing
            </div>
        </div>
    </div>
</section>

<?php include_once 'footer.php';?>
