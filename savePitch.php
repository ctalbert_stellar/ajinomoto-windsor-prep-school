<?php
//Start the system
include_once('init.php');

if(!isset($_POST['slideshow'])){
    die();
}

$slideshow = array_values($dbContext['Slideshows']->find($_POST['slideshow']))[0];
$slides = json_decode($_POST['slides']);

$slideshow->slides = array();

for($i= 0; $i < count($slides); $i++){
    $cats = array();
    foreach($slides[$i]->categories as $category){
        if(!empty($category)){
            $cats[] = array_values($dbContext['Categories']->find(array('name'=>$category)))[0];
        }
    }
    
    $slideshow->slides[] = new Slide(
            null,
            $slides[$i]->type,
            $slides[$i]->mimeType,
            $slides[$i]->location,
            $cats,
            $slideshow->guid,
            $slides[$i]->order,
            $slides[$i]->previewImage,
            $slides[$i]->resourceGUID
            );
}
$dbContext['Slideshows']->addOrEdit($slideshow);
$dbContext['Slideshows']->save();
echo 'Success';
