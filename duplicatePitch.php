<?php
//Start the system
include_once('init.php');

$title = "Pitch Builder Overview";
$pageName = "pitchBuilderOverview";
//Include HTML head
include_once('head.php');
//Include page header
include_once('header.php');

//Check for login Session
include 'loginCheck.php';

$person = $user->person;

$origSlideshow = array_values($dbContext['Slideshows']->find(urldecode($_GET['slideshow'])))[0];

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    $pitchTitle = filter_input(INPUT_POST, 'pitchName', FILTER_SANITIZE_STRING);
    
    if(isset($pitchTitle) && !empty($pitchTitle) && $pitchTitle !== FALSE){
        $slideshow = new Slideshow();
        $slideshow->title = $pitchTitle;
        $slideshow->person = $person->guid;
        foreach($origSlideshow->slides as $slide){
            $slideshow->slides[] = new Slide(
                    null,
                    $slide->type,
                    $slide->mimeType,
                    $slide->location,
                    $slide->categories,
                    $slideshow->guid,
                    $slide->order,
                    $slide->previewImage,
                    $slide->resource
                    );
        }
        $dbContext['Slideshows']->addOrEdit($slideshow);
        $dbContext['Slideshows']->save();
        $locationString = "Location: pitchBuilder.php?" . http_build_query(array('slideshow' => $slideshow->guid));
        header($locationString);
        exit();
    }else{
        $message = "Your pitch title is invalid.  Please make sure you have entered a valid pitch title and try again";
    }
}
?>
<section id="main">
    <?php include 'nav.php';?>
    <div class='content-padding'>
    <?php if(count($person->resources) > 0): ?>
        <h2>Duplicate Pitch</h2>
        <p>Decided to save some time and reuse part of an old slideshow? Great! Enter in the name of the new slideshow you want to duplicate to and we'll send you to the editor so you can make the changes needed.</p>
        <?=isset($message)?"<div class='message'>$message</div>":""?>
        <form method="post">
            <p><input type="text" name="pitchName" placeholder="Pitch Name" value='<?=$origSlideshow->title?>'></p>
            <input class="button blue" type='submit' value="Get Started">        
        </form>
    <?php else:?>
        <h2>No Resources Unlocked :(</h2>
        <p>You'll have to unlock some resources before you can start building a pitch. <a href="courseOverview.php">Go pass some courses</a> to unlock resources and come back when you've completed at least one.</p>
    <?php endif;?>
    </div>
</section>
