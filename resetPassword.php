<?php
if(!isset($_GET['token'])){
    header('HTTP/1.0 403 Forbidden');
    die();
}else{
    require_once 'init.php';

    $title = 'Reset Password';
    require_once 'head.php';
    $message = "";
    $query = "SELECT * FROM `passwordResets` WHERE `token` = :token";
    $statement = $db->prepare($query);
    $statement->execute(array('token' => $_GET['token']));
    $passwordResetInfo = $statement->fetchAll(PDO::FETCH_OBJ);
    if(count($passwordResetInfo) > 0){
        $expiry = $passwordResetInfo[0]->expiry;
        if(microtime() > $expiry){
            header('HTTP/1.0 403 Forbidden');
            die();
        }
        
        if($_SERVER['REQUEST_METHOD'] === 'POST'){
            $users = $dbContext['Users']->find(array('email' => $passwordResetInfo[0]->user));
            if($_POST['password'] !== $_POST['password2']){
                $message = "Passwords do not match, please try again";
            }else{
                $user = array_values($users)[0];
                $user->createPassword($_POST['password']);
                $dbContext['Users']->addOrEdit($user);
                $dbContext['Users']->save();
                $message = "Your password was successfully changed";
            }
            
        }
    }
}

?>
<section id="main">
    <div id='login'>
        <div id='login-image-wrapper'>
            <img src='images/logo.png' alt='Ajinomoto Windsor Prep School Logo'/>
        </div>
        <?php if(isset($message)):?>
        <div class="message"><?=$message?></div>
        <?php endif;?>
        <p>Enter your new password below</p>
        <form method='post' class='group'>
            <input type='password' name='password' class='textbox-style-1' placeholder='Password'>
            <input type='password' name='password2' class='textbox-style-1' placeholder='Confirm Password'>
            <input type='submit' value='Submit' class='submit blue'>
        </form>
        <div id='registration-link' class='link-style-1'>
            <p><a href="login.php">Return to Login</a></p>
        </div>
    </div>
</section>
<?php include_once 'footer.php'?>



