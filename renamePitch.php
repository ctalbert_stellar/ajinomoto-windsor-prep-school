<?php
//Start the system
include_once('init.php');

$title = "Rename Pitch";
$pageName = "pitchBuilderOverview";
//Include HTML head
include_once('head.php');
//Include page header
include_once('header.php');

//Check for login Session
include 'loginCheck.php';

$person = $user->person;

$slideshow = array_values($dbContext['Slideshows']->find(urldecode($_GET['slideshow'])))[0];

if($_SERVER['REQUEST_METHOD'] === 'POST'){
    $pitchTitle = filter_input(INPUT_POST, 'pitchName', FILTER_SANITIZE_STRING);
    
    if(isset($pitchTitle) && !empty($pitchTitle) && $pitchTitle !== FALSE){
        $slideshow->title = $pitchTitle;
        $dbContext['Slideshows']->addOrEdit($slideshow);
        $dbContext['Slideshows']->save();
        header("Location: pitchBuilderOverview.php");
        exit();
    }else{
        $message = "Your pitch title is invalid.  Please make sure you have entered a valid pitch title and try again";
    }
}
?>
<section id="main">
    <?php include 'nav.php';?>
    <div class='content-padding'>
    <?php if(count($person->resources) > 0): ?>
        <h2>Rename Pitch</h2>
        <p>Decided you didn't like the name of your pitch? Enter your new name below and click save to change it.</p>
        <?=isset($message)?"<div class='message'>$message</div>":""?>
        <form method="post">
            <p><input type="text" name="pitchName" placeholder="Pitch Name" value="<?=$slideshow->title?>"></p>
            <input class="button blue" type='submit' value="Save">        
        </form>
    <?php else:?>
        <h2>No Resources Unlocked :(</h2>
        <p>You'll have to unlock some resources before you can start building a pitch. <a href="courseOverview.php">Go pass some courses</a> to unlock resources and come back when you've completed at least one.</p>
    <?php endif;?>
    </div>
</section>
