//jQuery won't conflict with other libraries that might use $
jQuery.noConflict();

jQuery(function($){
    //jQuery Document Ready
    $('html').removeClass('no-js');

    $('#filter select').change(function(){
        var cat = $(this).val();
        $('.box').each(function(){
           var categories = $(this).data('category');
           categories = categories.split(',');
           console.log(cat + ' ' + categories);

           if(($.inArray(cat, categories)) === -1 && cat !== 'all'){
               $(this).hide();
           }else{
               $(this).show();
           }
        });
        var i = 2;
        $('.box:visible').each(function(){
            if(i%3 === 0){
                $(this).toggleClass('margin', true);
            }else{
                $(this).toggleClass('margin', false);
            }
            i++;
        });
    });
    
    var hamburger = false;
    $('#hamburger').click(function(){
        if(!hamburger){
            $('#nav-dropdown').animate({'top': 0});
            $('#hamburger').animate({'top': 315});
            hamburger = true;
        }else{
            $('#nav-dropdown').animate({'top': -315});
            $('#hamburger').animate({'top': 0});
            hamburger = false;s
            
        }
    });
    
    $('#create-link').click(function(e){
        e.preventDefault();
        $('#modal-wrapper').show();
        $('#create').show();
        return false;
    });
    
    $('.cancel').click(function(e){
        e.preventDefault();
        $('#modal-wrapper').hide();
        $('#create').hide();
        $('#copy').hide();
        $('#rename').hide();
        $('#delete').hide();
        return false;
    });
    
    $('.rename').click(function(){
       $('#modal-wrapper').show();
       $('#rename').show();
       $('#rename input[name="guid"]').val($(this).data('slideshow'));
       $('#rename input[name="title"').val($(this).data('title'));
    });
    $('.copy').click(function(){
       $('#modal-wrapper').show();
       $('#copy').show();
       $('#copy input[name="guid"]').val($(this).data('slideshow'));
       $('#copy input[name="title"]').val($(this).data('title'));
    });
    $('.delete').click(function(){
       $('#modal-wrapper').show();
       $('#delete').show();
       $('#delete input[name="guid"]').val($(this).data('slideshow'));
       $('#delete ul').empty();
       $('#delete ul').append("<li>Title: " + $(this).data('title') + "</li>");
       $('#delete ul').append("<li>Last Updated: " + $(this).data('updated') + "</li>");
       $('#delete ul').append("<li>Number of Slides: " + $(this).data('slides') + "</li>");
    });
    $('#form-create').submit(function(){
        $('#modal-wrapper').hide();
        $('#create').hide();
        $('#copy').hide();
        $('#rename').hide();
        $('#delete').hide();
    });
    $('#form-copy').submit(function(){
        $('#modal-wrapper').hide();
        $('#create').hide();
        $('#copy').hide();
        $('#rename').hide();
        $('#delete').hide();
    });

    $(".courseQuestions .modal-wrapper").show();
    $('.large-modal img.close-button').click(function(){
       $('.courseQuestions .modal-wrapper').fadeOut(500);
    });
    
    $('#company').change(function(){
        if(this.value === "Create"){
            $('.company').attr('type', 'text');
            $('.company').attr('disabled', false);
            $('input[name="companyName"]').val('');
            $('input[name="companyAddress1"]').val('');
            $('input[name="companyAddress2"]').val('');
            $('input[name="companyCity"]').val('');
            $('input[name="companyState"]').val('');
            $('input[name="companyZip"]').val('');
        }else if(this.value == 0){
            $('.company').attr('type', 'hidden');
            $('input[name="companyName"]').val('');
            $('input[name="companyAddress1"]').val('');
            $('input[name="companyAddress2"]').val('');
            $('input[name="companyCity"]').val('');
            $('input[name="companyState"]').val('');
            $('input[name="companyZip"]').val('');
        }else{
            var guid = this.value;
            $('.company').attr('type', 'text');
            $('.company').attr('disabled', true);
            $('input[name="companyName"]').val(companies[guid]['name']);
            $('input[name="companyAddress1"]').val(companies[guid]['address1']);
            $('input[name="companyAddress2"]').val(companies[guid]['address2']);
            $('input[name="companyCity"]').val(companies[guid]['city']);
            $('input[name="companyState"]').val(companies[guid]['state']);
            $('input[name="companyZip"]').val(companies[guid]['zip']);
        }
    });
    
    $('input').placeholder();
    if($.fn.knob) {
        $('.knob').knob({
            'readOnly': true,
            'thickness': 0.15,
            'font': 'Neuton',
            'fontWeight': '300',
        });
        $('.knob').val(parseInt($('.knob').val(), 10) + '%');
    }
});

//Stub Console Methods if they don't exist.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

