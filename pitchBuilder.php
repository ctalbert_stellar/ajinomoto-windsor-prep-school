<?php
if(!isset($_GET['slideshow'])){
    header("Location: index.php");
    exit();
}
//Start the system
include_once('init.php');

//Check for login Session
include 'loginCheck.php';
$title = "Pitch Builder";
$pageName = "pitchBuilder";
//Include HTML head
include_once('head.php');
$person = $user->person;

$slideshow = array_values($dbContext['Slideshows']->find($_GET['slideshow']))[0];
?>
<body class='pitchBuilder'>
<?php if(count($person->resources) < 1): ?>
<div class="preview-pane">
    <h2>You haven't unlocked any resources!</h2>
    <p><a href='courseOverview.php'>Go take some courses</a> to unlock resources and trophies for use in your pitch slide show.</p>
</div>
<?php else:?>
<div class="controls">
    <div class="control-button save-button blue"><img src="images/stiffy.png" alt="Save" class="save-button"><span class="control-text">SAVE</span></div>
    <div class="control-button close-button"><img src="images/delete.png" alt="Close" class="close-button"><span class="control-text">EXIT</span></div>
</div>
    <span class="help" id="slides-help"><img src="images/help.png"><span class="help-bubble"><span class="help-text" id="slides-help-text">Drag to reorder or click the trash can to remove the resource</span>[X]</span></span>
<div class="branding">
    <a href="http://ajinomotowindsorprepschool.com/"><img src="images/logo.png"></a> <span id="pitch-title">This Pitch: <?=$slideshow->title?></span>
</div>
    <div class="slideshow-filmstrip">
        <h2>Slides in this pitch</h2>
        <p>Drag resources from the bottom area to this area for use in this pitch.</p>
    </div>

<div class="preview-pane"></div>

<div class="resource-filter">
    <span class="filter-label">Filter</span>
    <span class="filter-popup-trigger">None <span class="right">▲</span></span>
    <div class="filter-popup">
        <?php foreach($dbContext['Categories']->getAll() as $category):?>
        <label><?=$category->name?>&nbsp;<span class="right"><input type="checkbox" value="<?=$category->name?>" checked></span></label>
        <?php endforeach;?>
    </div>
</div>
<div class="resources-filter-list">Available Resources <span class="help" id="resources-help"><img src="images/help.png"><span class="help-bubble"><span class="help-text" id="resource-help-text">Drag available resources to  the Slides in this pitch area to add the resource to your pitch</span>[X]</span></span></div>
<div class="resources-filmstrip">

</div>
<?php endif;?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?=Config::$siteRoot?>\/scripts\/lib\/jquery.min.js"><\/script>')</script>
<!-- Scripts at the bottom for speed -->
<script src="<?=Config::$siteRoot?>/scripts/lib/jquery.cycle2.min.js"></script>
<script src="<?=Config::$siteRoot?>/scripts/lib/jquery-ui-1.10.4.custom.js"></script>
<script>
/*!
 * jQuery UI Touch Punch 0.2.3
 *
 * Copyright 2011–2014, Dave Furfero
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * Depends:
 *  jquery.ui.widget.js
 *  jquery.ui.mouse.js
 */
!function(a){function f(a,b){if(!(a.originalEvent.touches.length>1)){a.preventDefault();var c=a.originalEvent.changedTouches[0],d=document.createEvent("MouseEvents");d.initMouseEvent(b,!0,!0,window,1,c.screenX,c.screenY,c.clientX,c.clientY,!1,!1,!1,!1,0,null),a.target.dispatchEvent(d)}}if(a.support.touch="ontouchend"in document,a.support.touch){var e,b=a.ui.mouse.prototype,c=b._mouseInit,d=b._mouseDestroy;b._touchStart=function(a){var b=this;!e&&b._mouseCapture(a.originalEvent.changedTouches[0])&&(e=!0,b._touchMoved=!1,f(a,"mouseover"),f(a,"mousemove"),f(a,"mousedown"))},b._touchMove=function(a){e&&(this._touchMoved=!0,f(a,"mousemove"))},b._touchEnd=function(a){e&&(f(a,"mouseup"),f(a,"mouseout"),this._touchMoved||f(a,"click"),e=!1)},b._mouseInit=function(){var b=this;b.element.bind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),c.call(b)},b._mouseDestroy=function(){var b=this;b.element.unbind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),d.call(b)}}}(jQuery);

//Navigation Confirmation
window.onbeforeunload = confirmExit;
function confirmExit(){
    return "Are you sure you want to leave the Pitch Builder? Any unsaved changes will be lost.";
}
</script>
<script>
window.onunload = refreshParent;
function refreshParent() {
    window.opener.location = window.opener.location.href;
}

function generateUUID(){
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x7|0x8)).toString(16);
    });
    return uuid;
}

function isEmpty(value){
    return (typeof value === "undefined" || value === null);
}

/**
 * Simple Model of a Slideshow
 * @param {String} guid
 * @param {String} title
 * @param {Slide[]} slides
 */
var Slideshow = function(guid, title, slides) {
    this.guid = !isEmpty(guid) ? guid : generateUUID();
    this.title = !isEmpty(title) ? title : 'TITLE GOES HERE';
    this.slides = !isEmpty(slides) ? slides : [];

    //Store a reference so we can get class properties in functions.
    var self = this;

    /**
     * Add a slide to this slideshow
     * @param {Slide} slide
     */
    this.addSlide = function(slide) {
        self.slides.push(slide);
    }

    /**
     * Remove a slide from this slideshow
     * @param  {Slide} slide
     */
    this.removeSlide = function(slide) {
        for(var i in self.slides){
            //test if the current slide is the one we're removing.
            if(self.slides[i].guid === slide.guid){
                //remove that slide from the array
                self.slides.splice(i, 1);
            }
        }
    }
}

/**
 * Simple Model of a Resource
 * @param {String} guid
 * @param {ResourceType} type
 * @param {String} mimeType
 * @param {String} location
 * @param {String[]} categories
 */
var Resource = function(guid, type, mimeType, location, categories, previewImage) {
    this.guid = !isEmpty(guid) ? guid : generateUUID();
    this.type = !isEmpty(type) ? type : 'Image';
    this.mimeType = !isEmpty(mimeType) ? mimeType : 'image/png';
    this.location = !isEmpty(location) ? location : 'http://ajinomotowindsorprepschool.com/testResources/404.png';
    this.categories = !isEmpty(categories) ? categories : [];
    this.previewImage = !isEmpty(previewImage) ? previewImage : 'http://ajinomotowindsorprepschool.com/testResources/404.png';

    //Store a reference so we can get class properties in functions.
    var self = this;

    /**
     * Create an HTMLElement Representation of this resource.
     * @return {HTMLElement}
     */
    this.toHTMLElement = function(){
        var categories = self.categories.join('|');
        var resourceElement = $('<div data-self="' + self.guid + '" data-categories="' + categories + '" />');
        resourceElement.addClass('resource');

        resourceElement.css({
            backgroundImage: 'url("'+self.previewImage+'")',
        });

        resourceElement.append($('<div class="slide-overlay"><img src="images/drag-indicator.png"><br />Drag to Reorder<div class="delete"><img src="images/delete-slide.png"></div></div>'));

        return resourceElement.get(0);
    };
};

/**
 * Simple Model of a Slide
 * @param {String} guid
 * @param {ResourceType} type
 * @param {String} mimeType
 * @param {String} location
 * @param {String[]} categories
 * @param {String} previewImage
 * @param {String} resourceGUID
 * @param {number} order
 */
var Slide = function(guid, type, mimeType, location, categories, previewImage, resourceGUID, order) {
    this.guid = !isEmpty(guid) ? guid : generateUUID();
    this.type = !isEmpty(type) ? type : 'Image';
    this.mimeType = !isEmpty(mimeType) ? mimeType : 'image/png';
    this.location = !isEmpty(location) ? location : 'http://ajinomotowindsorprepschool.com/testResources/resources/404.png';
    this.categories = !isEmpty(categories) ? categories : [];
    this.previewImage = !isEmpty(previewImage) ? previewImage : 'http://ajinomotowindsorprepschool.com/testResources/404.png';
    this.resourceGUID = !isEmpty(resourceGUID) ? resourceGUID : '';
    this.order = !isEmpty(order) ? order : 0;

    //Store a reference so we can get class properties in functions.
    var self = this;

    /**
     * Set this slide's properties from a Resource
     * @param {Resource} resource
     */
    this.loadValuesFromResource = function(resource) {
        self.resourceGUID = resource.guid;
        self.type = resource.type;
        self.mimeType = resource.mimeType;
        self.location = resource.location;
        self.categories = resource.categories;
        self.previewImage = resource.previewImage;
    }

    /**
     * Create an HTMLElement Representation of this slide.
     * @return {HTMLElement}
     */
this.toHTMLElement = function(){
        var slideElement = $('<div data-self="'+self.guid+'" />');
        slideElement.addClass('slide');

        slideElement.css({
            backgroundImage: 'url("'+self.previewImage+'")',
        });
        var categories = self.categories.join('|');
        var slideElement = $('<div data-self="' + self.guid + '" data-categories="' + categories + '" />');
        slideElement.addClass('slide');

        slideElement.css({
            backgroundImage: 'url("'+self.previewImage+'")',
        });

        slideElement.append($('<div class="slide-overlay"><img src="images/drag-indicator.png"><br />Drag to Reorder<div class="delete"><img src="images/delete-slide.png"></div></div>'));
        return slideElement.get(0);
    }
}

var slideshow = new Slideshow('<?=$slideshow->guid?>', '<?=$slideshow->title?>', null);

var resources = [
    <?php foreach($person->resources as $resource):
            $categories = array();
            foreach($resource->categories as $category):
                $categories[] = $category->name;
            endforeach;?>
        new Resource('<?=$resource->guid?>', '<?=$resource->type?>', '<?=$resource->mimeType?>', '<?=$resource->location?>', ['<?=addslashes(implode("','", $categories))?>'], '<?=$resource->preview?>'),
    <?php endforeach;?>
];

var slides = [];
<?php if(is_array($slideshow->slides)):
    foreach($slideshow->slides as $slide):
        $categories = array();
        foreach($slide->categories as $category):
            $categories[] = $category->name;
        endforeach;?>
        slides.push(new Slide('<?=$slide->guid?>', '<?=$slide->type?>', '<?=$slide->mimeType?>', '<?=$slide->location?>',['<?=addslashes(implode("','", $categories))?>'],'<?=$slide->previewImage?>', '<?=$slide->resource?>', '<?=$slide->order?>')),
<?php endforeach;
endif;?>

jQuery(function($){
    //Grab the strip elements
    var resourcesStrip = $('.resources-filmstrip');
    var slideshowStrip = $('.slideshow-filmstrip');

    //Load them with content
    for(var i in resources){
        resourcesStrip.append(resources[i].toHTMLElement());
    }
    for(var i in slides){
        slideshowStrip.append(slides[i].toHTMLElement());
    }
    //Setup the jQuery UI Interactions

    slideshowStrip.sortable({
        revert: true,
        revertDuration: 200,
    });
    resourcesStrip.find('.resource').draggable({
        connectToSortable: slideshowStrip,
        helper: 'clone',
        revert: 'invalid',
        revertDuration: 200,
        containment: 'html',
        appendTo: 'body'
    });

    /*resourcesStrip.on('mousewheel', function (event) {
        var delta = Math.max(-1, Math.min(1, (event.originalEvent.wheelDelta || -event.originalEvent.detail)));
        console.log(event, delta);
        this.scrollLeft -= (delta * 100);
    });*/

    $('.help-button').on('click', function(){
        $('.help-slides').stop(true, true).fadeIn(500);
    });

    $('.help-close-button').on('click', function(){
        $('.help-slides').stop(true, true).fadeOut(500);
    });

    $('.close-button').on('click', function(){
        window.close();
    });

    $('.save-button').on('click', function(){
        var _slides = [];
        $('.slideshow-filmstrip').find('.resource, .slide').each(function(index){
            var slide = null;
            if($(this).is('.slide')){
                for(var i in slides){
                    if($(this).data('self') == slides[i].guid){
                        slide = slides[i];
                    }
                }
            }else{
                for(var i in resources){
                    if($(this).data('self') == resources[i].guid){
                        slide = new Slide();
                        slide.loadValuesFromResource(resources[i]);
                    }
                }
            }
            if(slide !== null){
                slide.order = index;
                _slides.push(slide);
            }
        });

        var parameters = {"slideshow": slideshow.guid, "slides": JSON.stringify(_slides)};

        $.ajax({
            type: "POST",
            url: "savePitch.php",
            data: parameters
        }).done(function(data, textStatus, jqXHR){
            alert("Slideshow saved");
            refreshParent();
        }).fail(function(jqXHR, textStatus, errorThrown){
        });
    });

    $(document).on('touchmove', '.preview-pane', function(event){ event.preventDefault() });

    $(document).on('mouseenter touchstart', '.resources-filmstrip .resource', function(){
        var resource = null;
        console.log(this);
        for(var i in resources){
            if(resources[i].guid == $(this).data('self')){
                resource = resources[i];
                break;
            }
        }
        if(resource !== null){
            if(resource.type === 'Image'){
                $('.preview-pane').html('');
                $('.preview-pane').css('background-image', 'url("'+resource.location+'")');
            }else if(resource.type === 'Video'){
                $('.preview-pane').css('background-image', 'none');
                $('.preview-pane').html('<video src="'+resource.location+'" style="width:100%;height:100%;" controls autoplay muted></video>');
            }
        }
    });

    $(document).on('click', '.slideshow-filmstrip .resource, .slideshow-filmstrip .slide', function(){
        var resource = null;

        if($(this).is('.resource')){
            for(var i in resources){
                if(resources[i].guid == $(this).data('self')){
                    resource = resources[i];
                    break;
                }
            }
        }else if($(this).is('.slide')){
            for(var i in slides){
                if(slides[i].guid == $(this).data('self')){
                    resource = slides[i];
                    break;
                }
            }
        }

        if(resource !== null){
            if(resource.type === 'Image'){
                $('.preview-pane').html('');
                $('.preview-pane').css('background-image', 'url("'+resource.location+'")');
            }else if(resource.type === 'Video'){
                $('.preview-pane').css('background-image', 'none');
                $('.preview-pane').html('<video src="'+resource.location+'" style="width:100%;height:100%;" controls autoplay muted></video>');
            }
        }
    });

    $(document).on('click', '.slideshow-filmstrip .delete', function(){
        $(this).parent().parent().hide(400, function(){
            $(this).remove();
        });
    });

    $('.filter-popup-trigger').on('click', function(){
        $('.filter-popup').slideToggle();
    });
    $('.resource-filter input[type=checkbox]').on('change', function(){
        var checkbox = this;
        $('.resources-filmstrip .resource').each(function(){
            var categories = $(this).data('categories').split('|');
            if(categories.indexOf($(checkbox).val()) !== -1){
                if(checkbox.checked){
                    $(this).fadeIn(400);
                }else{
                    $(this).fadeOut(400);
                }
            }
        });
        
        var categoryString = '';
        if($('.resource-filter input[type=checkbox]:checked').length === 0 ||
            $('.resource-filter input[type=checkbox]:checked').length === $('.resource-filter input[type=checkbox]').length){
            categoryString += " None";
        }else if($('.resource-filter input[type=checkbox]:checked').length === 1){
            $('.resource-filter input[type=checkbox]').each(function(){
                if($(this).is(':checked')){
                    categoryString += " " + $(this).val()
                }
            });
        }else{
            categoryString += "Multiple"
        }
        categoryString = $.trim(categoryString);
        $('.filter-popup-trigger').html(categoryString + ' <span class="right">▲</span>');
       
    });
    
    $('.help').click(function(){
        $(this).children('.help-bubble').toggle();
    });
});
</script>
</body>
</html>